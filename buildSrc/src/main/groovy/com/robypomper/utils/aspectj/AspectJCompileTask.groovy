package com.robypomper.utils.aspectj

import org.gradle.api.InvalidUserDataException
import org.gradle.api.Task
import org.gradle.api.file.FileCollection
import org.gradle.api.tasks.Classpath
import org.gradle.api.tasks.Input
import org.gradle.api.tasks.InputDirectory
import org.gradle.api.tasks.InputFile
import org.gradle.api.tasks.InputFiles
import org.gradle.api.tasks.JavaExec
import org.gradle.api.tasks.Nested
import org.gradle.api.tasks.OutputDirectory
import org.gradle.api.tasks.SourceSet
import org.gradle.api.tasks.compile.JavaCompile

/**
 * Execute AspectJCompile on specified sourceSet.
 *
 * Create the 'ajc' configuration and add dependencies for AspectJCompiler
 * execution.
 *
 * Execution order:
 * - This task must be run after sourceSet.compileTask
 * - Task classes depends on this task
 *
 * This task change the compileXYJava destination source to "build/classes_preaspect/xy"
 * instead of "build/classes/xy". This is a workaround to force classes task to
 * find dir "build/classes/xy" empty then it looking for class files in "build/aspect/xy"
 * dir; previously added as sourceset output.
 */
@SuppressWarnings("unused")
class AspectJCompileTask extends JavaExec {

    // ------
    // Fields
    // ------

    File destDir = project.file(".")
    File inPath = project.file(".")
    FileCollection ajcPath = project.files([])
    String srcComp = project.sourceCompatibility
    String targComp = project.targetCompatibility

    // ----------
    // Properties
    // ----------

    void setSourceSet(SourceSet source) {
        JavaCompile javaCompile = (JavaCompile)project.tasks.findByName(source.compileJavaTaskName)
        Task classTask = project.tasks.findByName(source.classesTaskName)

        def javaDest = javaCompile.destinationDir.path.replace("/classes/" , "/classes_preaspect/")
        def aspectDest = javaCompile.destinationDir.path.replace("/classes/" , "/aspect/")


        // AspectJ Compiler args
        inPath = project.file(javaDest)
        destDir = project.file(aspectDest)
        srcComp = javaCompile.sourceCompatibility
        targComp = javaCompile.targetCompatibility

        // Java args
        classpath += source.compileClasspath

        // SourceSet and Task integration
        mustRunAfter javaCompile
        classTask.dependsOn this
        // add build/aspect/xy to sourceset outputs
        source.output.dir destDir
        // make build/classes/xy/ empty, classes will found in build/aspect/xy
        javaCompile.destinationDir = project.file(javaDest)


        inputs.dir inPath
        outputs.dir destDir
        //classTask.inputs outputs.files
        //javaCompile.source outputs.files, source.java
        //destDir = ajc.java.outputDir

        updateArgs()
    }

    void setAspectJCompiler(FileCollection aspectJClasspath) {
        classpath += aspectJClasspath
        ajcPath = aspectJClasspath

        updateArgs()
    }

    @OutputDirectory
    File getDestinationDir() {
        return this.destDir
    }

    @InputDirectory
    File getSource() {
        return this.inPath
    }

    @Classpath
    FileCollection getAjcPath() {
        return ajcPath!=null ? ajcPath : classpath
    }

    @Input
    String getSourceCompatibility() {
        return srcComp
    }

    @Input
    String getTargetCompatibility() {
        return targComp
    }


    // -----------
    // Constructor
    // -----------

    AspectJCompileTask() {
        group 'build'

        // Set Java default
        getProject().configurations { ajc }
        try {
            getProject().dependencies {
                ajc group: 'org.aspectj', name: 'aspectjrt', version: '1.9.+'
                ajc group: 'org.aspectj', name: 'aspectjtools', version: '1.9.+'
                ajc group: 'org.springframework', name: 'spring-tx', version: '5.1.+'
                ajc group: 'org.springframework', name: 'spring-context', version: '5.1.+'
            }
        } catch (InvalidUserDataException ignore) {}
        setAspectJCompiler getProject().configurations['ajc']


        main = 'org.aspectj.tools.ajc.Main'
        jvmArgs = ['-Xmx1024m']

//        doFirst {
//            println "AspectJCompileTask:"
//            println main
//            print "classpath:"
//            println classpath.join("\n\t")
//            println "args:"
//            println args.join("\n\t")
//        }
    }


    // -----
    // Utils
    // -----

    private updateArgs() {
        List<String> argsStr = new ArrayList<>()

        argsStr.add '-inpath'
        //argsStr.add "\"$getSource().as\""
        argsStr.add inPath.absolutePath

        argsStr.add '-d'
        //argsStr.add "\"$getDestinationDir()\""
        argsStr.add destDir.absolutePath

        argsStr.add '-aspectpath'
        //argsStr.add "\"$getAjPath()\""
        argsStr.add ajcPath.asPath

        argsStr.add '-source'
        //argsStr.add "\"$getSourceCompatibility()\""
        argsStr.add srcComp

        argsStr.add '-target'
        //argsStr.add "\"$getTargetCompatibility()\""
        argsStr.add targComp


        argsStr.add '-preserveAllLocals'
//            if (verbose) {
//                argsStr.add '--verbose'
//                argsStr.add "\"$verbose\""
//
//                argsStr.add '-verbose'
//                argsStr.add '-g'
//                argsStr.add '-showWeaveInfo'
//                argsStr.add '-Xlint:warning'
//            } else {
        argsStr.add '-Xlint:ignore'
//            }
        args = argsStr
    }

}
