package com.robypomper.utils.performance

class PubChecks {

    static boolean checkPerformanceOnMavenLocal() {
        def home = System.getProperty("user.home")
        def mavenDir = new File(home + '/.m2/repository/com/robypomper/utils/performance/performance')
        return mavenDir.exists()
    }

    static boolean checkPerformancePluginOnMavenLocal() {
        def home = System.getProperty("user.home")
        def mavenDir = new File(home + '/.m2/repository/com/robypomper/utils/performance/performance-gradle-plugin')
        return mavenDir.exists()
    }

}
