package com.robypomper.utils;


import java.io.*;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.*;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * ToDo: aggiornare implementazioni di resourceIdAsFile e resourceAsDir, guardare l'implementazione di resourceAsFile
 * ToDo: testare sezione monitoring
 * ToDo: riprogettare classe IOResources
 * ToDo: documentare classe IOResources
 */
@SuppressWarnings({"WeakerAccess", "unused"})
public class IOResources {

    // -------
    // Loaders
    // -------

    /**
     * @param resource reference to a existing file (full file).
     * @return readable Path of given resource.
     * @throws FileNotFoundException if resource refer to a not existing file.
     * @throws IllegalArgumentException if resource exist and is a directory,
     *                                  or it's empty, invalid or not supported.
     * @throws NullPointerException if resource is null.
     */
    public static Path resourceToInPath(Object resource) throws FileNotFoundException {
        File file = resourceAsFile(resource);

        if (!file.exists())
            throw new FileNotFoundException(String.format("File '%s' not found.", file.getPath()));

        return file.toPath();
    }

    /**
     * @param resource reference to an existing directory (dir path).
     * @param id reference to an existing file (file name).
     * @return readable Path of given resource/id.
     * @throws FileNotFoundException if resource refer to a not existing file.
     * @throws IllegalArgumentException if resource is a file, or exist and is not a directory;
     *                                  if id is a directory, or exist and is not a file
     *                                  or it's empty, invalid or not supported.
     * @throws NullPointerException if resource is null.
     */
    public static Path resourceToInPath(Object resource, String id) throws FileNotFoundException {
        File file = resourceIdAsFile(resource,id);

        if (!file.exists())
            throw new FileNotFoundException(String.format("File '%s' not found.", file.getPath()));

        return file.toPath();
    }

    /**
     * @param resource reference to file (full file) or directory (dir path).
     * @return list of readable Paths of given resource.
     * @throws NullPointerException if resource is null.
     */
    public static List<Path> resourceToInPaths(Object resource) {
        List<Path> paths = new ArrayList<>();
        try {
            Path path = resourceToInPath(resource);
            paths.add(path);

        } catch (IOException|IllegalArgumentException e) {
            try {
                List<File> files = resourceAsDir(resource,true);
                paths = files.stream().filter(File::exists).map(File::toPath).collect(Collectors.toList());

            } catch(IllegalArgumentException ignored) {}
        }

        return paths;
    }

    /**
     * @param resource reference to file (full file) or directory (dir path).
     * @param filter filter to apply on files contained in reference.
     * @return list of readable Paths of given resource.
     * @throws NullPointerException if resource is null.
     */
    public static List<Path> resourceToInPaths(Object resource, FilenameFilter filter) {
        List<Path> paths = new ArrayList<>();
        try {
            Path path = resourceToInPath(resource);
            if (filter.accept(path.toFile(),path.getFileName().toString()))
                paths.add(path);

        } catch (IOException|IllegalArgumentException e) {
            try {
                List<File> files = resourceAsDir(resource,true,filter);
                paths = files.stream().filter(File::exists).map(File::toPath).collect(Collectors.toList());

            } catch(IllegalArgumentException ignored) {}
        }

        return paths;
    }


    /**
     * @param resource reference to a existing file (full file).
     * @return readable ObjectInputStream of given resource.
     * @throws FileNotFoundException if resource refer to a not existing file.
     * @throws IllegalArgumentException if resource exist and is a directory,
     *                                  or it's empty, invalid or not supported.
     * @throws NullPointerException if resource is null.
     */
    public static ObjectInputStream resourceToInObjStream(Object resource) throws IOException {
        File file = resourceAsFile(resource);

        if (!file.exists())
            throw new FileNotFoundException(String.format("File '%s' not found.", file.getPath()));

        InputStream is = new FileInputStream(file);
        return new ObjectInputStream(is);
    }

    /**
     * @param resource reference to an existing directory (dir path).
     * @param id reference to an existing file (file name).
     * @return readable ObjectInputStream of given resource/id.
     * @throws FileNotFoundException if resource refer to a not existing file.
     * @throws IllegalArgumentException if resource is a file, or exist and is not a directory;
     *                                  if id is a directory, or exist and is not a file
     *                                  or it's empty, invalid or not supported.
     * @throws NullPointerException if resource is null.
     */
    public static ObjectInputStream resourceToInObjStream(Object resource, String id) throws IOException {
        File file = resourceIdAsFile(resource,id);

        if (!file.exists())
            throw new FileNotFoundException(String.format("File '%s' not found.", file.getPath()));

        InputStream is = new FileInputStream(file);
        return new ObjectInputStream(is);
    }

    /**
     * @param resource reference to file (full file) or directory (dir path).
     * @return list of readable ObjectInputStream of given resource.
     * @throws NullPointerException if resource is null.
     */
    public static List<ObjectInputStream> resourceToInObjStreams(Object resource) {
        List<ObjectInputStream> oss = new ArrayList<>();
        try {
            ObjectInputStream objStream = resourceToInObjStream(resource);
            oss.add(objStream);

        } catch (IOException|IllegalArgumentException e) {
            try {
                List<File> files = resourceAsDir(resource,true);
                oss = files.stream().filter(File::exists).map(file -> {
                    try { return new ObjectInputStream(new FileInputStream(file)); }
                    catch (IOException e1) { return null; }
                }).filter(Objects::nonNull).collect(Collectors.toList());

            } catch(IllegalArgumentException ignored) {}
        }

        return oss;
    }

    /**
     * @param resource reference to file (full file) or directory (dir path).
     * @param filter filter to apply on files contained in reference.
     * @return list of readable ObjectInputStream of given resource.
     * @throws NullPointerException if resource is null.
     */
    public static List<ObjectInputStream> resourceToInObjStreams(Object resource, FilenameFilter filter) {
        List<ObjectInputStream> oss = new ArrayList<>();
        try {
            ObjectInputStream objStream = resourceToInObjStream(resource);
            //if (filter.accept(objStream.toFile(),path.getFileName().toString()))
            oss.add(objStream);

        } catch (IOException|IllegalArgumentException e) {
            try {
                List<File> files = resourceAsDir(resource,true,filter);
                oss = files.stream().filter(File::exists).map(file -> {
                    try { return new ObjectInputStream(new FileInputStream(file)); }
                    catch (IOException e1) { return null; }
                }).filter(Objects::nonNull).collect(Collectors.toList());

            } catch(IllegalArgumentException ignored) {}
        }

        return oss;
    }


    // -------
    // Storers
    // -------

    /**
     * @param resource reference to a file (existing or not) (full file).
     * @param mustNew if true resource file must not exist.
     * @return writable Path of given resource.
     * @throws FileAlreadyExistsException if mustNew is true and reference exists.
     * @throws IllegalArgumentException if resource exist and is a directory,
     *                                  or it's empty, invalid or not supported.
     * @throws NullPointerException if resource is null.
     */
    public static Path resourceToOutPath(Object resource, boolean mustNew) throws FileAlreadyExistsException {
        File file = resourceAsFile(resource);

        if (mustNew && file.exists())
            throw new FileAlreadyExistsException(String.format("File '%s' already exist.", file.getPath()));

        return file.toPath();
    }

    /**
     * @param resource reference to a directory (existing or not) (dir path).
     * @param id reference to a file (existing or not) (file name).
     * @param mustNew if true resource file must not exist.
     * @param createPath if true and resource not exist, then create it.
     * @return writable Path of given resource/id.
     * @throws FileAlreadyExistsException if mustNew is true and reference/id exists.
     * @throws NotDirectoryException if createPath is false and reference not exists.
     * @throws IllegalArgumentException if resource is a file, or exist and is not a directory;
     *      *                                  if id is a directory, or exist and is not a file
     *      *                                  or it's empty, invalid or not supported.
     *      * @throws NullPointerException if resource is null.
     */
    public static Path resourceToOutPath(Object resource, String id, boolean mustNew, boolean createPath) throws FileAlreadyExistsException, NotDirectoryException {
        File file = resourceIdAsFile(resource,id);

        if (mustNew && file.exists())
            throw new FileAlreadyExistsException(String.format("File '%s' already exist.", file.getPath()));

        if (!createPath && !resourceExist(resource))
            throw new NotDirectoryException(String.format("File '%s'`s directory not exist.", file.getPath()));

        if (createPath && !resourceExist(resource))
            resourceCreateAsDir(resource);

        return file.toPath();
    }

    /**
     * @param resource reference to a file (existing or not) (full file).
     * @param mustNew if true resource file must not exist.
     * @return writable ObjectOutputStream of given resource.
     * @throws FileAlreadyExistsException if mustNew is true and reference exists.
     * @throws IllegalArgumentException if resource exist and is a directory,
     *                                  or it's empty, invalid or not supported.
     * @throws NullPointerException if resource is null.
     */
    public static ObjectOutputStream resourceToOutObjStream(Object resource, boolean mustNew) throws IOException {
        File file = resourceAsFile(resource);

        if (mustNew && file.exists())
            throw new FileAlreadyExistsException(String.format("File '%s' already exist.", file.getPath()));

        OutputStream os = new FileOutputStream(file);
        return new ObjectOutputStream(os);
    }

    /**
     * @param resource reference to a directory (existing or not) (dir path).
     * @param id reference to a file (existing or not) (file name).
     * @param mustNew if true resource file must not exist.
     * @param createPath if true and resource not exist, then create it.
     * @return writable ObjectOutputStream of given resource/id.
     * @throws FileAlreadyExistsException if mustNew is true and reference/id exists.
     * @throws NotDirectoryException if createPath is false and reference not exists.
     * @throws IllegalArgumentException if resource is a file, or exist and is not a directory;
     *      *                                  if id is a directory, or exist and is not a file
     *      *                                  or it's empty, invalid or not supported.
     *      * @throws NullPointerException if resource is null.
     */
    public static ObjectOutputStream resourceToOutObjStream(Object resource, String id, boolean mustNew, boolean createPath) throws IOException {
        File file = resourceIdAsFile(resource,id);

        if (mustNew && file.exists())
            throw new FileAlreadyExistsException(String.format("File '%s' already exist.", file.getPath()));

        if (createPath && !resourceExist(resource))
            resourceCreateAsDir(resource);

        OutputStream os = new FileOutputStream(file);
        return new ObjectOutputStream(os);
    }


    // --------
    // Monitors
    // --------

    /** Listener object for resource's monitor events. */
    public interface ResourceObserver {
        void resourceChanged(Object resource, Object event);

    }

    private static final Map<Object,Integer> monitorTimes = new HashMap<>();
    private static final Map<Object,Object> monitorResources = new HashMap<>();
    private static final Map<Object,ResourceObserver> monitorObservers = new HashMap<>();

    /** Min seconds between one check and other. */
    public static int MonitorMinExecutionPeriod = 1;
    private static Timer monitorTimer;
    private static final TimerTask monitorTask = new TimerTask() {
        @Override
        public void run() {
            for (Map.Entry<Object,Integer> entry : monitorTimes.entrySet()) {
                if ( System.currentTimeMillis()/1000 % entry.getValue() != 0) continue;

                Object resource = entry.getKey();
                Object resourceMonitor = monitorResources.get(resource);
                if (resourceMonitor==null) {

                    Path path = Paths.get((String)resource);
                    if (!path.toFile().exists())
                        continue;

                    WatchService watchService;
                    try {
                        watchService = FileSystems.getDefault().newWatchService();
                        path.register(watchService,
                                StandardWatchEventKinds.ENTRY_CREATE,
                                StandardWatchEventKinds.ENTRY_DELETE,
                                StandardWatchEventKinds.ENTRY_MODIFY);

                        ResourceObserver observer = monitorObservers.get(resource);
                        WatchEvent event = new WatchEvent() {
                            @Override
                            public Kind kind() {
                                return StandardWatchEventKinds.ENTRY_CREATE;
                            }

                            @Override
                            public int count() {
                                return 0;
                            }

                            @Override
                            public Object context() {
                                return resource;
                            }
                        };
                        observer.resourceChanged(resource,event);
                    } catch (IOException e) {
                        continue;
                    }

                    monitorResources.put(resource,watchService);
                    resourceMonitor = monitorResources.get(resource);
                }

                if (resourceMonitor instanceof WatchService) {
                    WatchService watchService = (WatchService) resourceMonitor;
                    WatchKey key;
                    while ((key = watchService.poll()) != null) {
                        for (WatchEvent<?> event : key.pollEvents()) {
                            //System.out.println(
                            //        SampleRate.toFormattedDate(Performance.getMillisSince()) + ": " +
                            //        "Event kind:" + event.kind() + ". File affected: " + event.context() + ".");
                            ResourceObserver observer = monitorObservers.get(resource);
                            observer.resourceChanged(resource,event);
                        }
                        key.reset();
                    }
                }
            }
        }
    };

    @SuppressWarnings("UnusedReturnValue")
    private static boolean tryStartMonitoring() {
        if (monitorTimer!=null) return true;

        monitorTimer = new Timer(true);
        monitorTimer.schedule(monitorTask,0,MonitorMinExecutionPeriod*1000);
        return true;
    }

    private static boolean tryStopMonitoring() {
        if (monitorTimer==null) return true;
        if (monitorTimes.size()>0) return false;

        monitorTimer.cancel();
        monitorTimer = null;
        return true;
    }

    public static void resourceMonitor(Object resource, int interval, ResourceObserver observer) throws IOException {
        // if not already started, start monitorTimer
        tryStartMonitoring();

        // Register resource monitor
        monitorTimes.put(resource,interval);
        monitorObservers.put(resource,observer);

        // If resource don't exist, exit
        Path path = Paths.get((String)resource);
        if (!path.toFile().exists())
            return;

        // Register WatchService to resource
        WatchService watchService = FileSystems.getDefault().newWatchService();
        path.register(watchService,
                StandardWatchEventKinds.ENTRY_CREATE,
                StandardWatchEventKinds.ENTRY_DELETE,
                StandardWatchEventKinds.ENTRY_MODIFY);
        monitorResources.put(resource,watchService);
    }

    public static boolean resourceDemonitor(Object resource) throws IOException {
        // Deregister resource monitor
        monitorTimes.remove(resource);
        Object resourceMonitor = monitorResources.remove(resource);
        monitorObservers.remove(resource);

        //noinspection ConditionCoveredByFurtherCondition
        if (resourceMonitor!=null && resourceMonitor instanceof WatchService) {
            WatchService watchService = (WatchService)resourceMonitor;
            watchService.close();
        }

        // if this is last resource monitor, stop monitorTimer
        return tryStopMonitoring();
    }

    // -----------
    // Resource As
    // -----------

    /**
     * @param resource reference to a file (existing or not) (full file).
     * @return file representation of given resource.
     * @throws IllegalArgumentException if resource exist and is a directory,
     *                                  or it's empty, invalid or not supported.
     * @throws NullPointerException if resource is null.
     */
    private static File resourceAsFile(Object resource) {
        if (resource==null) throw new NullPointerException("Resource for resourceAsFile method not setted (null).");

        File file = null;
        if (resource instanceof String) {
            URL res = IOResources.class.getResource((String) resource);
            if (res != null && res.toString().startsWith("jar:")) {
                try {
                    InputStream input = IOResources.class.getResourceAsStream((String) resource);
                    file = File.createTempFile("tempfile", ".tmp");
                    OutputStream out = new FileOutputStream(file);
                    int read;
                    byte[] bytes = new byte[1024];
                    while ((read = input.read(bytes)) != -1) {
                        out.write(bytes, 0, read);
                    }
                    file.deleteOnExit();
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            } else if (res != null) {
                file = new File(res.getFile());
            } else {
                //this will probably work in your IDE, but not from a JAR
                file = new File((String) resource);
            }
        }

        // Verify File
        if (file==null) throw new IllegalArgumentException(String.format("Resource '%s' of type '%s', not supported in resourceAsFile method.", resource, resource.getClass().getName()));
        if (file.isDirectory()) throw new IllegalArgumentException(String.format("Resource File '%s' is an existing directory, file was required.", file.getPath()));
        if (file.exists() && !file.isFile()) throw new IllegalArgumentException(String.format("Resource File '%s' already exist and is not a valid file.", file.getPath()));
        if (file.getParentFile()==null) {
            URL dir_url = ClassLoader.getSystemResource((String)resource);
            try {
                return new File(dir_url.toURI());
            } catch (URISyntaxException ignore) {}
            throw new IllegalArgumentException(String.format("Resource File '%s' is not a valid file nor java resource.", file.getPath()));
        }

        return file;
    }

    /**
     * @param resource reference to a directory (existing or not) (dir path).
     * @param id reference to a file (existing or not) (file name).
     * @return file representation of given resource/id.
     * @throws IllegalArgumentException if resource is a file, or exist and is not a directory;
     *                                  if id is a directory, or exist and is not a file
     *                                  or it's empty, invalid or not supported.
     * @throws NullPointerException if resource is null.
     */
    private static File resourceIdAsFile(Object resource, String id) {
        if (resource instanceof String) {
            File dir = new File((String)resource);

            if (dir.isFile())
                throw new IllegalArgumentException(String.format("Resource Dir '%s' is an existing file, directory was required.", dir.getPath()));

            if (dir.exists() && !dir.isDirectory())
                throw new IllegalArgumentException(String.format("Resource Dir '%s' is not a valid directory.", dir.getPath()));


            File file = new File(dir,id);

            if (id.isEmpty())
                throw new IllegalArgumentException(String.format("Resource File '%s' is empty, file name required.", id));

            if (file.isDirectory())
                throw new IllegalArgumentException(String.format("Resource File '%s' is an existing directory, file was required.", file.getPath()));

            if (file.exists() && !file.isFile())
                throw new IllegalArgumentException(String.format("Resource File '%s' is not a valid file.", file.getPath()));

            //if (file.getParentFile()==null)
            //    throw new IllegalArgumentException(String.format("Resource File '%s' is not a valid file.", file.getPath()));

            @SuppressWarnings("RegExpRedundantEscape") Pattern pattern = Pattern.compile("[~#*%{}<>\\[\\]|\"\\^]");
            Matcher matcher = pattern.matcher(file.toString());
            if (matcher.find())
                throw new IllegalArgumentException(String.format("Resource File '%s' contains invalid chars.", file.getPath()));

            return file;
        }

        if (resource==null) throw new NullPointerException("Resource Dir for resourceIdAsFile method not setted (null).");

        throw new IllegalArgumentException(String.format("Resource Dir '%s' of type '%s', not supported in resourceIdAsFile method.", resource, resource.getClass().getName()));
    }

    /**
     * @param resource reference to a directory (existing or not) (dir path).
     * @param onlyFiles if true exclude directories.
     * @return list of file representations of given resource.
     * @throws IllegalArgumentException if resource is a file, or exist and is not a directory
     *                                  or it's empty, invalid or not supported.
     * @throws NullPointerException if resource is null.
     */
    private static List<File> resourceAsDir(Object resource, boolean onlyFiles) {
        if (resource instanceof String) {
            return resourceAsDir(resource, onlyFiles, (dir, name) -> true);
        }

        if (resource==null) throw new NullPointerException("Resource for resourceIdAsFile method not setted (null).");

        throw new IllegalArgumentException(String.format("Resource '%s' of type '%s', not supported in resourceAsDir method.", resource, resource.getClass().getName()));
    }

    /**
     * @param resource reference to a directory (existing or not) (dir path).
     * @param onlyFiles if true exclude directories.
     * @param filenameFilter filter to apply on files contained in reference.
     * @return list of file representations of given resource.
     * @throws IllegalArgumentException if resource is a file, or exist and is not a directory
     *                                  or it's empty, invalid or not supported.
     * @throws NullPointerException if resource is null.
     */
    private static List<File> resourceAsDir(Object resource, boolean onlyFiles, FilenameFilter filenameFilter) {
        if (resource instanceof String) {
            File dir = new File((String)resource);

            if (dir.isFile())
                throw new IllegalArgumentException(String.format("Resource '%s' is an existing file, directory was required.", dir.getPath()));

            if (dir.exists() && !dir.isDirectory())
                throw new IllegalArgumentException(String.format("Resource '%s' is not a valid directory.", dir.getPath()));

            Pattern pattern = Pattern.compile("[~#@*+%{}<>\\[\\]|\"_^]");
            Matcher matcher = pattern.matcher(dir.toString());
            if (matcher.find())
                throw new IllegalArgumentException(String.format("Resource '%s' contains invalid chars.", dir.getPath()));

            File[] files = dir.listFiles(filenameFilter);
            if (files==null)
                return new ArrayList<>();

            return Arrays.stream(files).filter(f -> !(onlyFiles && f.exists() && !f.isFile())).collect(Collectors.toList());
        }

        if (resource==null) throw new IllegalArgumentException("Resource for resourceIdAsFile method not setted (null).");

        throw new NullPointerException(String.format("Resource '%s' of type '%s', not supported in resourceAsDir method.", resource, resource.getClass().getName()));
    }

    private static void resourceCreateAsDir(Object resource) {
        if (resource instanceof String) {
            File file = new File((String)resource);

            if (file.exists())
                return;

            if (!file.mkdirs())
                throw new IllegalArgumentException(String.format("Can't create directory for resource '%s'.", resource));
            return;
        }

        throw new IllegalArgumentException(String.format("Resource '%s' of type '%s', not supported in resourceCreateAsDir method.", resource, resource.getClass().getName()));
    }

    @SuppressWarnings("BooleanMethodIsAlwaysInverted")
    private static boolean resourceExist(Object resource) {
        if (resource instanceof String) {
            File file = new File((String)resource);

            return file.exists();
        }

        throw new IllegalArgumentException(String.format("Resource '%s' of type '%s', not supported in resourceAsFile method.", resource, resource.getClass().getName()));
    }


    // -------------------------------------------------------------------------
    // -------------------------------------------------------------------------

    // -------
    // Testing
    // -------

    private final static String test_resDir = "/tmp/IOResource/";

    public static void main(String[] args) throws IOException {
        test_deleteRecursive(test_resDir);
        test_createDir(test_resDir);

        // Private transformation tests

        System.out.println("\n\n##################");
        System.out.println("resourceAsFileTest");
        System.out.println("##################\n");
        test_deleteRecursive(test_resDir);
        test_createDir(test_resDir);
        resourceAsFileTest();

        System.out.println("\n\n####################");
        System.out.println("resourceIdAsFileTest");
        System.out.println("####################\n");
        test_deleteRecursive(test_resDir);
        test_createDir(test_resDir);
        resourceIdAsFileTest();

        System.out.println("\n\n#################");
        System.out.println("resourceAsDirTest");
        System.out.println("#################\n");
        test_deleteRecursive(test_resDir);
        test_createDir(test_resDir);
        resourceAsDirTest();

        // Resource Loader test

        System.out.println("\n\n####################");
        System.out.println("resourceToInPathTest");
        System.out.println("####################\n");
        test_deleteRecursive(test_resDir);
        test_createDir(test_resDir);
        resourceToInPathTest();

        System.out.println("\n\n######################");
        System.out.println("resourceIdToInPathTest");
        System.out.println("######################\n");
        test_deleteRecursive(test_resDir);
        test_createDir(test_resDir);
        resourceIdToInPathTest();

        System.out.println("\n\n#######################");
        System.out.println("resourceToInPathsTest[]");
        System.out.println("#######################\n");
        test_deleteRecursive(test_resDir);
        test_createDir(test_resDir);
        resourceToInPathsTest();


        // Resource Loader test

        System.out.println("\n\n#####################");
        System.out.println("resourceToOutPathTest");
        System.out.println("#####################\n");
        test_deleteRecursive(test_resDir);
        test_createDir(test_resDir);
        resourceToOutPathTest();

        System.out.println("\n\n#######################");
        System.out.println("resourceIdToOutPathTest");
        System.out.println("#######################\n");
        test_deleteRecursive(test_resDir);
        test_createDir(test_resDir);
        resourceIdToOutPathTest();

        System.exit(0);
    }

    // Test functions

    // Tested functions
    //resourceAsFile(Object resource)
    private static void resourceAsFileTest() throws IOException {
        String existingFullFileName = test_resDir + "existingFile.txt";
        String notExistingFullFileName = test_resDir + "notExistingFile.txt";
        String existingFullDirName = test_resDir + "ExistingDir";
        String existingFullDirNameB = "/tmp/IOResource";
        String notExistingFullDirName = test_resDir + "notExistingDir/";
        String notExistingFullDirNameB = test_resDir + "notExistingDir";
        String invalidPath = "# bla bla ? bla bla #";   // if it contains \ (slash char) it can considered valid
        String emptyString = "";
        Integer notStringObject = 0;
        Object nullObject = null;

        test_createFile(existingFullFileName);
        test_createDir(existingFullDirName);

        File f;
        String testStr;


        System.out.println("Full file path exist: ok");
        testStr = existingFullFileName;
        f = resourceAsFile(testStr);
        System.out.println("-" + f.toString());
        assert testStr.compareTo(f.toString())==0;
        System.out.println();


        System.out.println("full file path not exist: ok, it can be created");
        testStr = notExistingFullFileName;
        f = resourceAsFile(testStr);
        System.out.println("-" + f.toString());
        assert testStr.compareTo(f.toString())==0;
        System.out.println();


        System.out.println("full dir path exist: IllegalArgumentException resource is an existing directory, file was required.");
        try {
            resourceAsFile(existingFullDirName);
            assert false;
        } catch (IllegalArgumentException e) { System.out.println(String.format("- Exception: %s", e.getMessage())); }
        try {
            resourceAsFile(existingFullDirNameB);
            assert false;
        } catch (IllegalArgumentException e) { System.out.println(String.format("- Exception: %s", e.getMessage())); }
        System.out.println();


        System.out.println("full dir path not exist: ok, it can be created as file");
        testStr = notExistingFullDirName;
        f = resourceAsFile(testStr);
        System.out.println("-" + f.toString());
        assert testStr.compareTo(f.toString() + "/")==0;
        testStr = notExistingFullDirNameB;
        f = resourceAsFile(testStr);
        System.out.println("-" + f.toString());
        assert testStr.compareTo(f.toString())==0;
        System.out.println();


        System.out.println("Fullpath invalid path: IllegalArgumentException Resource is not a valid file.");
        try {
            resourceAsFile(invalidPath);
            assert false;
        } catch (IllegalArgumentException e) { System.out.println(String.format("- Exception: %s", e.getMessage())); }
        System.out.println();


        System.out.println("Fullpath empty string: IllegalArgumentException Resource is not a valid file.");
        try {
            resourceAsFile(emptyString);
            assert false;
        } catch (IllegalArgumentException e) { System.out.println(String.format("- Exception: %s", e.getMessage())); }
        System.out.println();


        System.out.println("Fullpath Integer: IllegalArgumentException Resource of type 'java.lang.Integer', not supported in resourceAsFile method.");
        try {
            resourceAsFile(notStringObject);
            assert false;
        } catch (IllegalArgumentException e) { System.out.println(String.format("- Exception: %s", e.getMessage())); }
        System.out.println();


        System.out.println("Fullpath null: NullPointerException Resource for resourceAsFile method not setted (null).");
        try {
            //noinspection ConstantConditions
            resourceAsFile(nullObject);
            assert false;
        } catch (NullPointerException e) { System.out.println(String.format("- Exception: %s", e.getMessage())); }
        System.out.println();

    }

    // Tested functions
    //resourceIdAsFile(Object resource, String id)
    @SuppressWarnings({"UnnecessaryLocalVariable", "ConstantConditions"})
    private static void resourceIdAsFileTest() throws IOException {
        String existingResDir = test_resDir + "";
        String existingFilePath = existingResDir;
        String notExistingFilePath = existingResDir + "NotExistingDirectory/";
        String invalidFilePath = existingResDir + "# bla bla ? bla bla #";
        String emptyFilePath = "";
        Integer notStringFilePath = 1;
        Object nullObjectFilePath = null;
        String existingFileName = "existingFile.txt";
        String notExistingFileName = "notExistingFile.txt";
        String existingSunDirName = "ExistingDirectory";
        String notExistingSunDirName = "NotExistingDirectory";
        String invalidFileName = "# bla bla ? bla bla #";
        String emptyStringFileName = "";

        test_createDir(existingFilePath);
        test_createFile(existingFilePath + existingFileName);
        test_createDir(existingFilePath + existingSunDirName);


        File f;
        Object testStr;
        String testIdStr;

        // Resourced Id cases

        System.out.println("Dir path exist + File path exist: ok");
        testStr = existingFilePath;
        testIdStr = existingFileName;
        f = resourceIdAsFile(testStr,testIdStr);
        System.out.println("-" + f.toString());
        assert (testStr + testIdStr).compareTo(f.toString())==0;
        System.out.println();

        System.out.println("Dir path exist + File path not exist: ok, it can be created");
        testStr = existingFilePath;
        testIdStr = notExistingFileName;
        f = resourceIdAsFile(testStr,testIdStr);
        System.out.println("-" + f.toString());
        assert (testStr + testIdStr).compareTo(f.toString())==0;
        System.out.println();

        System.out.println("Dir path exist + Dir path exist: IllegalArgumentException Resource File is an existing directory, file was required.");
        try {
            resourceIdAsFile(existingFilePath,existingSunDirName);
            assert false;
        } catch (IllegalArgumentException e) { System.out.println(String.format("- Exception: %s", e.getMessage())); }
        System.out.println();

        System.out.println("Dir path exist + Dir path not exist: ok, it can be created as file");
        testStr = existingFilePath;
        testIdStr = notExistingSunDirName;
        f = resourceIdAsFile(testStr,testIdStr);
        System.out.println("-" + f.toString());
        assert (testStr + testIdStr).compareTo(f.toString())==0;
        System.out.println();

        System.out.println("Dir path exist + File path invalid: IllegalArgumentException Resource File is not a valid file.");
        try {
            resourceIdAsFile(existingFilePath,invalidFileName);
            assert false;
        } catch (IllegalArgumentException e) { System.out.println(String.format("- Exception: %s", e.getMessage())); }
        System.out.println();

        System.out.println("Dir path exist + File path empty string: IllegalArgumentException Resource File is empty, file name required.");
        try {
            resourceIdAsFile(existingFilePath,emptyStringFileName);
            assert false;
        } catch (IllegalArgumentException e) { System.out.println(String.format("- Exception: %s", e.getMessage())); }
        System.out.println();


        // Resource Dir cases

        System.out.println("Dir path not exist + File not path exist: ok");
        testStr = notExistingFilePath;
        testIdStr = notExistingFileName;
        f = resourceIdAsFile(testStr,testIdStr);
        System.out.println("-" + f.toString());
        assert (testStr + testIdStr).compareTo(f.toString())==0;
        System.out.println();

        System.out.println("Dir path path invalid + File path exist: IllegalArgumentException: Resource File contains invalid chars.");
        try {
            resourceIdAsFile(invalidFilePath,existingFileName);
            assert false;
        } catch (IllegalArgumentException e) { System.out.println(String.format("- Exception: %s", e.getMessage())); }
        System.out.println();

        System.out.println("Dir path empty string + File path exist: ok");
        testStr = emptyFilePath;
        testIdStr = existingFileName;
        f = resourceIdAsFile(testStr,testIdStr);
        System.out.println("-" + f.toString());
        System.out.println();

        System.out.println("Dir path not String + File path exist: IllegalArgumentException: Resource Dir, not supported in resourceIdAsFile method.");
        try {
            resourceIdAsFile(notStringFilePath,existingFileName);
            assert false;
        } catch (IllegalArgumentException e) { System.out.println(String.format("- Exception: %s", e.getMessage())); }
        System.out.println();

        System.out.println("Dir path null + File path exist: NullPointerException: Resource Dir for resourceIdAsFile method not setted (null)");
        try {
            resourceIdAsFile(nullObjectFilePath,existingFileName);
            assert false;
        } catch (NullPointerException e) { System.out.println(String.format("- Exception: %s", e.getMessage())); }
        System.out.println();
    }

    // Tested functions
    //resourceAsDir(Object resource, boolean onlyFiles)
    //resourceAsDir(Object resource, boolean onlyFiles, FilenameFilter filenameFilter)
    @SuppressWarnings("ConstantConditions")
    private static void resourceAsDirTest() throws IOException {
        String existingResDir = test_resDir + "";
        String invalidResDirFiles = existingResDir + "# bla bla ? bla bla #";

        String notExistingResDirFiles = existingResDir + "NotExistingDirectory/";
        String emptyStringResDirFiles = "";
        Integer notStringResDirFiles = 1;
        Object nullObjectResDirFiles = null;

        String existingResDirFiles = existingResDir + "ExistingDirectory/";
        String existingResDirFiles_Txt1 = existingResDir + "ExistingDirectory/file1.txt";
        String existingResDirFiles_Txt2 = existingResDir + "ExistingDirectory/file2.txt";
        String existingResDirFiles_Txt3 = existingResDir + "ExistingDirectory/file3.txt";
        String existingResDirFiles_Other1 = existingResDir + "ExistingDirectory/file.other1";
        String existingResDirFiles_Other2 = existingResDir + "ExistingDirectory/file.other2";
        String existingResDirSubDir_Other2 = existingResDir + "ExistingDirectory/SubDirectory";
        FilenameFilter txtFilter = (dir, name) -> name.endsWith(".txt");

        test_createDir(existingResDirFiles);
        test_createFile(existingResDirFiles_Txt1);
        test_createFile(existingResDirFiles_Txt2);
        test_createFile(existingResDirFiles_Txt3);
        test_createFile(existingResDirFiles_Other1);
        test_createFile(existingResDirFiles_Other2);
        test_createDir(existingResDirSubDir_Other2);


        List<File> f;
        Object testStr;
        FilenameFilter filter;

        // onlyFiles

        System.out.println("Dir path exist + onlyFiles = true: ok");
        testStr = existingResDirFiles;
        f = resourceAsDir(testStr,true);
        System.out.println(String.format("- found %d results", f.size()));
        assert 5 == f.size();
        System.out.println();

        System.out.println("Dir path exist + onlyFiles = false: ok");
        testStr = existingResDirFiles;
        f = resourceAsDir(testStr,false);
        System.out.println(String.format("- found %d results", f.size()));
        assert 6 == f.size();
        System.out.println();

        // filter

        System.out.println("Dir path exist + filter = txt: ok");
        testStr = existingResDirFiles;
        filter = txtFilter;
        f = resourceAsDir(testStr,true,filter);
        System.out.println(String.format("- found %d results", f.size()));
        assert 3 == f.size();
        System.out.println();

        System.out.println("Dir path exist + filter = txt: ok");
        testStr = existingResDirFiles;
        filter = txtFilter;
        f = resourceAsDir(testStr,false,filter);
        System.out.println(String.format("- found %d results", f.size()));
        assert 3 == f.size();
        System.out.println();



        // Resource Dir cases

        System.out.println("Dir path not exist: ok");
        testStr = notExistingResDirFiles;
        f = resourceAsDir(testStr,false);
        System.out.println(String.format("- found %d results", f.size()));
        assert 0 == f.size();
        System.out.println();

        System.out.println("Dir path path invalid: IllegalArgumentException: Resource File contains invalid chars.");
        try {
            resourceAsDir(invalidResDirFiles,false);
            assert false;
        } catch (IllegalArgumentException e) { System.out.println(String.format("- Exception: %s", e.getMessage())); }
        System.out.println();

        System.out.println("Dir path empty string: ok");
        testStr = emptyStringResDirFiles;
        f = resourceAsDir(testStr,false);
        System.out.println(String.format("- found %d results", f.size()));
        assert 0 == f.size();
        System.out.println();

        System.out.println("Dir path not String: IllegalArgumentException: Resource Dir, not supported in resourceIdAsFile method.");
        try {
            resourceAsDir(notStringResDirFiles,false);
            assert false;
        } catch (IllegalArgumentException e) { System.out.println(String.format("- Exception: %s", e.getMessage())); }
        System.out.println();

        System.out.println("Dir path null: NullPointerException: Resource Dir for resourceIdAsFile method not setted (null)");
        try {
            resourceAsDir(nullObjectResDirFiles,false);
            assert false;
        } catch (NullPointerException e) { System.out.println(String.format("- Exception: %s", e.getMessage())); }
        System.out.println();

    }


    // Tested functions
    //resourceToInPath(Object resource)
    @SuppressWarnings({"CastCanBeRemovedNarrowingVariableType", "ConstantConditions"})
    private static void resourceToInPathTest() throws IOException {
        String existingFullFileName = test_resDir + "existingFile.txt";
        String notExistingFullFileName = test_resDir + "notExistingFile.txt";
        Integer notStringObject = 0;
        Object nullObject = null;

        test_createFile(existingFullFileName);


        Path p;
        Object testStr;


        System.out.println("Full file path exist: ok");
        testStr = existingFullFileName;
        p = resourceToInPath(testStr);
        System.out.println("-" + p.toString());
        assert ((String)testStr).compareTo(p.toString())==0;
        System.out.println();

        System.out.println("Full file path not exist: FileNotFoundException: File '/tmp/IOResource/notExistingFile.txt' not found.");
        try {
            resourceToInPath(notExistingFullFileName);
            assert false;
        } catch (FileNotFoundException e) { System.out.println(String.format("- Exception: %s", e.getMessage())); }
        System.out.println();

        System.out.println("Full file path invalid: IllegalArgumentException: Resource '0' of type 'java.lang.Integer', not supported in resourceAsFile method.");
        try {
            resourceToInPath(notStringObject);
            assert false;
        } catch (IllegalArgumentException e) { System.out.println(String.format("- Exception: %s", e.getMessage())); }
        System.out.println();

        System.out.println("Full file path null: NullPointerException: Resource for resourceAsFile method not setted (null).");
        try {
            resourceToInPath(nullObject);
            assert false;
        } catch (NullPointerException e) { System.out.println(String.format("- Exception: %s", e.getMessage())); }
        System.out.println();
    }

    // Tested functions
    //resourceToInPath(Object resource, String id)
    @SuppressWarnings({"UnnecessaryLocalVariable", "ConstantConditions"})
    private static void resourceIdToInPathTest() throws IOException {
        String existingResDir = test_resDir + "";
        String existingFilePath = existingResDir;
        String notExistingFilePath = existingResDir + "NotExistingDirectory/";
        Integer notStringFilePath = 1;
        Object nullObjectFilePath = null;
        String existingFileName = "existingFile.txt";
        String notExistingFileName = "notExistingFile.txt";
        String emptyStringFileName = "";

        test_createDir(existingFilePath);
        test_createFile(existingFilePath + existingFileName);


        Path p;
        Object testStr;
        String testIdStr;

        System.out.println("Dir path exist + File path exist: ok");
        testStr = existingFilePath;
        testIdStr = existingFileName;
        p = resourceToInPath(testStr,testIdStr);
        System.out.println("-" + p.toString());
        assert (testStr + testIdStr).compareTo(p.toString())==0;
        System.out.println();

        System.out.println("Dir path exist + File path not exist: FileNotFoundException File '/tmp/IOResource/notExistingFile.txt' not found.");
        try {
            resourceToInPath(existingFilePath,notExistingFileName);
            assert false;
        } catch (FileNotFoundException e) { System.out.println(String.format("- Exception: %s", e.getMessage())); }
        System.out.println();

        System.out.println("Dir path not exist + File path not exist: FileNotFoundException File '/tmp/IOResource/NotExistingDirectory/notExistingFile.txt' not found.");
        try {
            resourceToInPath(notExistingFilePath,notExistingFileName);
            assert false;
        } catch (FileNotFoundException e) { System.out.println(String.format("- Exception: %s", e.getMessage())); }
        System.out.println();

        System.out.println("Dir path exist + File path invalid: IllegalArgumentException: any on resource file (resource file empty)");
        try {
            resourceToInPath(existingFilePath,emptyStringFileName);
            assert false;
        } catch (IllegalArgumentException e) { System.out.println(String.format("- Exception: %s", e.getMessage())); }
        System.out.println();

        System.out.println("Dir path invalid + File path not exist: IllegalArgumentException: any on resource dir (resource dir wrong type).");
        try {
            resourceToInPath(notStringFilePath,emptyStringFileName);
            assert false;
        } catch (IllegalArgumentException e) { System.out.println(String.format("- Exception: %s", e.getMessage())); }
        System.out.println();

        System.out.println("Dir path null + File path not exist: NullPointerException: Resource Dir for resourceIdAsFile method not setted (null).");
        try {
            resourceToInPath(nullObjectFilePath,emptyStringFileName);
            assert false;
        } catch (NullPointerException e) { System.out.println(String.format("- Exception: %s", e.getMessage())); }
        System.out.println();
    }

    // Tested functions
    //resourceToInPaths(Object resource)
    //resourceToInPaths(Object resource, FilenameFilter filter)
    @SuppressWarnings("ConstantConditions")
    private static void resourceToInPathsTest() throws IOException {
        String existingFullFileName = test_resDir + "existingFile.txt";
        String notExistingFullFileName = test_resDir + "notExistingFile.txt";
        String existingFullDirName = test_resDir + "ExistingDir/";
        String notExistingFullDirName = test_resDir + "notExistingDir/";
        String invalidResDirOrFiles = test_resDir + "# bla bla ? bla bla #";
        Object nullObjectResDirOrFiles = null;

        String existingResDirFiles_Txt1 = existingFullDirName + "file1.txt";
        String existingResDirFiles_Txt2 = existingFullDirName + "file2.txt";
        String existingResDirFiles_Txt3 = existingFullDirName + "file3.txt";
        String existingResDirFiles_Other1 = existingFullDirName + "file.other1";
        String existingResDirFiles_Other2 = existingFullDirName + "file.other2";
        FilenameFilter txtFilter = (dir, name) -> name.endsWith(".txt");

        test_createFile(existingFullFileName);
        test_createDir(existingFullDirName);
        test_createFile(existingResDirFiles_Txt1);
        test_createFile(existingResDirFiles_Txt2);
        test_createFile(existingResDirFiles_Txt3);
        test_createFile(existingResDirFiles_Other1);
        test_createFile(existingResDirFiles_Other2);


        List<Path> paths;
        Object testStr;


        System.out.println("Full file path exist: ok");
        testStr = existingFullFileName;
        paths = resourceToInPaths(testStr);
        System.out.println(String.format("- found %d results", paths.size()));
        assert 1 == paths.size();
        System.out.println();

        System.out.println("Full file path not exist: ok, empty list.");
        testStr = notExistingFullFileName;
        paths = resourceToInPaths(testStr);
        System.out.println(String.format("- found %d results", paths.size()));
        assert 0 == paths.size();
        System.out.println();

        System.out.println("Dir path exist: ok");
        testStr = existingFullDirName;
        paths = resourceToInPaths(testStr);
        System.out.println(String.format("- found %d results", paths.size()));
        assert 5 == paths.size();
        System.out.println();

        System.out.println("Dir path exist + txtFilter: ok");
        testStr = existingFullDirName;
        paths = resourceToInPaths(testStr,txtFilter);
        System.out.println(String.format("- found %d results", paths.size()));
        assert 3 == paths.size();
        System.out.println();

        System.out.println("Dir path not exist: ok, empty list.");
        testStr = notExistingFullDirName;
        paths = resourceToInPaths(testStr);
        System.out.println(String.format("- found %d results", paths.size()));
        assert 0 == paths.size();
        System.out.println();

        System.out.println("Resource invalid: ok, empty list.");
        testStr = invalidResDirOrFiles;
        paths = resourceToInPaths(testStr);
        System.out.println(String.format("- found %d results", paths.size()));
        assert 0 == paths.size();
        System.out.println();

        System.out.println("Resource null: NullPointerException: Resource for resourceAsFile method not setted (null).");
        try {
            resourceToInPaths(nullObjectResDirOrFiles);
            assert false;
        } catch (NullPointerException e) { System.out.println(String.format("- Exception: %s", e.getMessage())); }
        System.out.println();
    }


    // Tested functions
    //resourceToOutPath(Object resource, boolean mustNew)
    @SuppressWarnings({"CastCanBeRemovedNarrowingVariableType", "ConstantConditions"})
    private static void resourceToOutPathTest() throws IOException {
        String existingFullFileName = test_resDir + "existingFile.txt";
        String notExistingFullFileName = test_resDir + "notExistingFile.txt";
        Integer notStringObject = 0;
        Object nullObject = null;

        test_createFile(existingFullFileName);


        Path p;
        Object testStr;


        System.out.println("Full file path exist + mustNew=true: FileAlreadyExistsException: File '/tmp/IOResource/existingFile.txt' already exist.");
        try {
            resourceToOutPath(existingFullFileName,true);
            assert false;
        } catch (FileAlreadyExistsException e) { System.out.println(String.format("- Exception: %s", e.getMessage())); }
        System.out.println();

        System.out.println("Full file path exist + mustNew=false: ok");
        testStr = existingFullFileName;
        p = resourceToOutPath(testStr,false);
        System.out.println("-" + p.toString());
        assert ((String)testStr).compareTo(p.toString())==0;
        System.out.println();

        System.out.println("Full file path not exist + mustNew=true: ok");
        testStr = notExistingFullFileName;
        p = resourceToOutPath(testStr,false);
        System.out.println("-" + p.toString());
        assert ((String)testStr).compareTo(p.toString())==0;
        System.out.println();

        System.out.println("Full file path not exist + mustNew=false: ok");
        testStr = notExistingFullFileName;
        p = resourceToOutPath(testStr,false);
        System.out.println("-" + p.toString());
        assert ((String)testStr).compareTo(p.toString())==0;
        System.out.println();

        System.out.println("Full file path invalid: IllegalArgumentException: Resource '0' of type 'java.lang.Integer', not supported in resourceAsFile method.");
        try {
            resourceToOutPath(notStringObject,false);
            assert false;
        } catch (IllegalArgumentException e) { System.out.println(String.format("- Exception: %s", e.getMessage())); }
        System.out.println();

        System.out.println("Full file path null: NullPointerException: Resource for resourceAsFile method not setted (null).");
        try {
            resourceToOutPath(nullObject,false);
            assert false;
        } catch (NullPointerException e) { System.out.println(String.format("- Exception: %s", e.getMessage())); }
        System.out.println();
    }

    // Tested functions
    //resourceToOutPath(Object resource, String id, boolean mustNew, boolean createPath)
    @SuppressWarnings({"UnnecessaryLocalVariable", "ConstantConditions"})
    private static void resourceIdToOutPathTest() throws IOException {
        String existingResDir = test_resDir + "";
        String existingFilePath = existingResDir;
        String notExistingFilePath = existingResDir + "NotExistingDirectory/";
        Integer notStringFilePath = 1;
        Object nullObjectFilePath = null;
        String existingFileName = "existingFile.txt";
        String notExistingFileName = "notExistingFile.txt";
        String emptyStringFileName = "";

        test_createDir(existingFilePath);
        test_createFile(existingFilePath + existingFileName);


        Path p;
        Object testStr;
        String testIdStr;

        System.out.println("Dir path exist + File path exist + mustNew=true: FileAlreadyExistsException File '/tmp/IOResource/existingFile.txt' already exist.");
        try {
            resourceToOutPath(existingFilePath,existingFileName,true,true);
            assert false;
        } catch (FileAlreadyExistsException e) { System.out.println(String.format("- Exception: %s", e.getMessage())); }
        System.out.println();

        System.out.println("Dir path exist + File path exist + mustNew=false: Ok");
        testStr = existingFilePath;
        testIdStr = existingFileName;
        p = resourceToOutPath(testStr,testIdStr,false,true);
        System.out.println("-" + p.toString());
        assert (testStr + testIdStr).compareTo(p.toString())==0;
        System.out.println();

        System.out.println("Dir path exist + File path not exist: Ok");
        testStr = existingFilePath;
        testIdStr = notExistingFileName;
        p = resourceToOutPath(testStr,testIdStr,true,true);
        System.out.println("-" + p.toString());
        assert (testStr + testIdStr).compareTo(p.toString())==0;
        System.out.println();

        System.out.println("Dir path not exist + File path not exist + createPath=true: Ok");
        testStr = notExistingFilePath;
        testIdStr = notExistingFileName;
        p = resourceToOutPath(testStr,testIdStr,true,true);
        System.out.println("-" + p.toString());
        assert (testStr + testIdStr).compareTo(p.toString())==0;
        test_deleteRecursive(notExistingFilePath);
        System.out.println();

        System.out.println("Dir path not exist + File path not exist + createPath=false: NotDirectoryException: File '/tmp/IOResource/NotExistingDirectory/notExistingFile.txt'`s directory not exist.");
        try {
            resourceToOutPath(notExistingFilePath,notExistingFileName,true,false);
            assert false;
        } catch (NotDirectoryException e) { System.out.println(String.format("- Exception: %s", e.getMessage())); }
        System.out.println();

        System.out.println("Dir path exist + File path invalid: IllegalArgumentException: any on resource file (resource file empty)");
        try {
            resourceToInPath(existingFilePath,emptyStringFileName);
            assert false;
        } catch (IllegalArgumentException e) { System.out.println(String.format("- Exception: %s", e.getMessage())); }
        System.out.println();

        System.out.println("Dir path invalid + File path not exist: IllegalArgumentException: any on resource dir (resource dir wrong type).");
        try {
            resourceToInPath(notStringFilePath,emptyStringFileName);
            assert false;
        } catch (IllegalArgumentException e) { System.out.println(String.format("- Exception: %s", e.getMessage())); }
        System.out.println();

        System.out.println("Dir path null + File path not exist: NullPointerException: Resource Dir for resourceIdAsFile method not setted (null).");
        try {
            resourceToInPath(nullObjectFilePath,emptyStringFileName);
            assert false;
        } catch (NullPointerException e) { System.out.println(String.format("- Exception: %s", e.getMessage())); }
        System.out.println();
    }

    // Test utils

    private static void test_deleteRecursive(String dirOrFileStr) {
        File dirOrFile = new File(dirOrFileStr);
        if (dirOrFile.isDirectory()) {
            File[] files = dirOrFile.listFiles();
            if (files!=null) Arrays.asList(files).forEach( f-> test_deleteRecursive(f.getAbsolutePath()));
        }
        //noinspection ResultOfMethodCallIgnored
        dirOrFile.delete();
    }

    private static void test_createFile(String fileStr) throws IOException {
        File file = new File(fileStr);
        if (!file.exists())
            Files.write(file.toPath(),"".getBytes(), StandardOpenOption.CREATE);
    }

    private static void test_createDir(String dirStr) {
        File dir = new File(dirStr);
        if (!dir.exists())
            //noinspection ResultOfMethodCallIgnored
            dir.mkdirs();
    }

}
