package com.robypomper.utils;

import java.awt.*;
import javax.swing.*;

class ColorDisplay extends JFrame {

    // ------
    // Fields
    // ------

    private static final String[] defaultColors = new String[]
            {"#f20d0d", "#ff0000", "#0df2f2", "#00ffff", "#0df20d", "#00ff00", "#7ff20d", "#7fff00", "#c4f20d", "#ccff00"};
    //{"#f20d0d", "#f53d3d", "#ff0000", "#ff3333", "#0df2f2", "#3df5f5", "#00ffff", "#33ffff", "#0df20d", "#3df53d", "#00ff00", "#33ff33", "#7ff20d", "#99f53d", "#7fff00", "#99ff33", "#c4f20d", "#d0f53d", "#ccff00", "#d6ff33", "#f2f20d", "#f5f53d", "#ffff00", "#ffff33"};


    // -----------
    // Constructor
    // -----------

    @SuppressWarnings("WeakerAccess")
    ColorDisplay() {
        this(defaultColors);
    }

    ColorDisplay(String[] colors) {
        createPalette(colors);
        showWindow();
    }


    // -----------
    // Window mngm
    // -----------

    private void createPalette(String[] colors) {
        Container content = this.getContentPane();

        content.setLayout(new GridLayout(colors.length/2,2));
        for (String color : colors)
            content.add(new GraphicsPanel(color));

        this.setTitle("ColorDisplay");
        this.pack();
    }

    void showWindow() {
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setVisible(true);
    }


    class GraphicsPanel extends JPanel {

        private final String hexColor;

        GraphicsPanel(String hexColor) {
            this.hexColor = hexColor;
            this.setPreferredSize(new Dimension(300, 100));
            this.setBackground(Color.white);
            this.setForeground(Color.black);
            this.add(new Label(hexColor));
        }

        public void paintComponent(Graphics g) {
            super.paintComponent(g);           // paint background, border
            Color c = new Color(
                    Integer.valueOf( hexColor.substring( 1, 3 ), 16 ),
                    Integer.valueOf( hexColor.substring( 3, 5 ), 16 ),
                    Integer.valueOf( hexColor.substring( 5, 7 ), 16 ) );
            g.setColor(c);
            g.fillRect(0, 0, this.getWidth(), this.getHeight()); // Color everything
        }
    }


    // -------------------------------------------------------------------------
    // -------------------------------------------------------------------------

    // -------
    // Testing
    // -------

    public static void main(String[] args) {
        new ColorDisplay().showWindow();
    }

}