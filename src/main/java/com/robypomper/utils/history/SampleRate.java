package com.robypomper.utils.history;

import com.robypomper.utils.performance.Performance;

import java.text.SimpleDateFormat;
import java.util.*;
import java.util.function.BiFunction;
import java.util.function.Function;

/**
 * SampleRate representation and management methods.
 *
 * SampleRate are used by History class to define time ranges.
 *
 * @since 0.1
 */
@SuppressWarnings({"WeakerAccess", "unused"})
public enum SampleRate {

    // ------
    // Values
    // ------

    Minute(1000*60),
    Hour(1000*60*60),
    HalfDay(1000*60*60*12),
    Day(1000*60*60*24);


    // ------
    // Fields
    // ------

    private final long length;
    private final long HALF_DAY_LENGTH = 1000 * 60 * 60 * 12;


    // -----------
    // Constructor
    // -----------

    SampleRate(long length) {
        this.length = length;
    }


    // -------
    // Getters
    // -------

    public long getLength() {
        return length;
    }

    public long getCurrentStart() {
        return getStart(Performance.getMillisSince());
    }

    public long getStart(long millisSince) {
        int timeZoneOffsetStart = length < HALF_DAY_LENGTH ? 0 : TimeZone.getDefault().getOffset(millisSince);          //to UTC
        long Days = ( millisSince + timeZoneOffsetStart ) / length;

        long Start = Days * length;
        int timeZoneOffsetEnd = length < HALF_DAY_LENGTH ? 0 : TimeZone.getDefault().getOffset(Start);                  //from UTC

        return Start - timeZoneOffsetEnd;
    }

    public long getPrevStart(int count) {
        return getPrevStart(count,Performance.getMillisSince());
    }

    public long getPrevStart(int count,long millisSince) {          //to UTC
        long currentStart = getStart(millisSince);
        int timeZoneOffsetStart = length < HALF_DAY_LENGTH ? 0 : TimeZone.getDefault().getOffset(currentStart);          //to UTC

        long Prev = currentStart + timeZoneOffsetStart - (length*count);
        int timeZoneOffsetEnd = length < HALF_DAY_LENGTH ? 0 : TimeZone.getDefault().getOffset(Prev);                   //from UTC

        return Prev - timeZoneOffsetEnd;
    }

    public long getCurrentEnd() {
        return getEnd(Performance.getMillisSince());
    }

    public long getEnd(long millisSince) {
        int timeZoneOffsetStart = length < HALF_DAY_LENGTH ? 0 : TimeZone.getDefault().getOffset(millisSince);
        long Days = ( millisSince + timeZoneOffsetStart ) / length;
        long Start = Days * length;

        long End = Start + length;
        int timeZoneOffsetEnd = length < HALF_DAY_LENGTH ? 0 : TimeZone.getDefault().getOffset(End);
        return End - timeZoneOffsetEnd;
    }

    public long getNextEnd(int count) {
        return getNextEnd(count,Performance.getMillisSince());
    }

    public long getNextEnd(int count, long millisSince) {
        long currentStart = getEnd(millisSince);
        int timeZoneOffsetStart = length < HALF_DAY_LENGTH ? 0 : TimeZone.getDefault().getOffset(currentStart);          //to UTC

        long Prev = currentStart + timeZoneOffsetStart + (length*count);
        int timeZoneOffsetEnd = length < HALF_DAY_LENGTH ? 0 : TimeZone.getDefault().getOffset(Prev);                   //from UTC

        return Prev - timeZoneOffsetEnd;
    }


    // ---------------
    // Timestamp utils
    // ---------------

    /**
     * Transform passed long in formatted string in "yyyy-MM-dd HH:mm:ss.S" format,
     * ready to be displayed.
     *
     * @param msSince number of milliseconds since January 1, 1970, 00:00:00 GMT
     * @return formatted string in "yyyy-MM-dd HH:mm:ss.S" representing msSince
     */
    public static String toFormattedDate(long msSince) {
        Date date=new Date(msSince);
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S");
        return df.format(date);
    }

    /**
     * Transform passed long in formatted string in "yyyyMMdd_HHmmss" format,
     * ready to be used as filename.
     *
     * @param msSince number of milliseconds since January 1, 1970, 00:00:00 GMT
     * @return formatted string in "yyyyMMdd_HHmmss" representing msSince
     */
    public static String toFilenameDate(long msSince) {
        Date date=new Date(msSince);
        SimpleDateFormat df = new SimpleDateFormat("yyyyMMdd_HHmmss");
        return df.format(date);
    }

    /**
     * Transform passed string in format "yyyy-MM-dd HH:mm:ss.S" to the
     * number of milliseconds since January 1, 1970, 00:00:00 GMT.
     *
     * @param timestamp date string in format "yyyy-MM-dd HH:mm:ss.S"
     * @return the number of milliseconds since January 1, 1970, 00:00:00 GMT
     *         represented by timestamp
     */
    public static long toMSSince(String timestamp) {
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S");
        Date date;
        try {
            date = df.parse(timestamp);
        } catch (Exception e) {
            return -1;
        }
        return date.getTime();
    }

    // -------------------------------------------------------------------------
    // -------------------------------------------------------------------------

    // -------
    // Testing
    // -------

    public static void main(String[] args) {

        // Simple SampleRate Viewer (for test and example purpose)
//        String ms = "2018-01-01 00:00:00.0";                // reference date
//        int count = 1;                                      // +/- integer
//        SampleRate sr = Minute;                             // Any SampleRate
//        test_simpleGetPrint(ms,sr::getCurrentStart);              // getCurrentStart, getCurrentEnd
//        test_simpleGetCountPrint(count,ms,sr::getPrevStart);      // getPrevStart, getNextEnd
//        System.exit(0);


        Map<SampleRate,List<String>> testSet = new HashMap<>();


        // this test pass a start date to getCurrentStart, getCurrentEnd, getPrevStart, getNextEnd functions for each SampleRate
        // then check the result with the passed string containing the expected result
        // this test is executed with UTC and Europe/Rome timezones
        System.out.println("\n\n##########");
        System.out.println("startEndPrevNextTest");
        System.out.println("##########\n");

        //                                                      getCurrentStart         getCurrentEnd           getPrevStart(1)         getPrevStart(2)         getPrevStart(3)         getPrevStart(-1)        getNextEnd(1)           getNextEnd(2)           getNextEnd(3)           getNextEnd(-1)
        List<String> minutes = new ArrayList<>(Arrays.asList("2018-01-01 00:00:00.0","2018-01-01 00:01:00.0","2017-12-31 23:59:00.0","2017-12-31 23:58:00.0","2017-12-31 23:57:00.0","2018-01-01 00:01:00.0","2018-01-01 00:02:00.0","2018-01-01 00:03:00.0","2018-01-01 00:04:00.0","2018-01-01 00:00:00.0"));
        List<String> hours   = new ArrayList<>(Arrays.asList("2018-01-01 00:00:00.0","2018-01-01 01:00:00.0","2017-12-31 23:00:00.0","2017-12-31 22:00:00.0","2017-12-31 21:00:00.0","2018-01-01 01:00:00.0","2018-01-01 02:00:00.0","2018-01-01 03:00:00.0","2018-01-01 04:00:00.0","2018-01-01 00:00:00.0"));
        List<String> halfday = new ArrayList<>(Arrays.asList("2018-01-01 00:00:00.0","2018-01-01 12:00:00.0","2017-12-31 12:00:00.0","2017-12-31 00:00:00.0","2017-12-30 12:00:00.0","2018-01-01 12:00:00.0","2018-01-02 00:00:00.0","2018-01-02 12:00:00.0","2018-01-03 00:00:00.0","2018-01-01 00:00:00.0"));
        List<String> day     = new ArrayList<>(Arrays.asList("2018-01-01 00:00:00.0","2018-01-02 00:00:00.0","2017-12-31 00:00:00.0","2017-12-30 00:00:00.0","2017-12-29 00:00:00.0","2018-01-02 00:00:00.0","2018-01-03 00:00:00.0","2018-01-04 00:00:00.0","2018-01-05 00:00:00.0","2018-01-01 00:00:00.0"));

        // Imposta inizio sample
        testSet.clear();
        testSet.put(Minute , addFirst("2018-01-01 00:00:00.0",minutes));
        testSet.put(Hour   , addFirst("2018-01-01 00:00:00.0",hours  ));
        testSet.put(HalfDay, addFirst("2018-01-01 00:00:00.0",halfday));
        testSet.put(Day    , addFirst("2018-01-01 00:00:00.0",day    ));
        startEndPrevNextTest(TimeZone.getTimeZone("UTC"),testSet);
        startEndPrevNextTest(TimeZone.getTimeZone("Europe/Rome"),testSet);

        // Imposta fine sample (-1ms)
        testSet.clear();
        testSet.put(Minute , addFirst("2018-01-01 00:00:59.9",minutes));
        testSet.put(Hour   , addFirst("2018-01-01 00:59:59.9",hours  ));
        testSet.put(HalfDay, addFirst("2018-01-01 11:59:59.9",halfday));
        testSet.put(Day    , addFirst("2018-01-01 23:59:59.9",day    ));
        startEndPrevNextTest(TimeZone.getTimeZone("UTC"),testSet);
        startEndPrevNextTest(TimeZone.getTimeZone("Europe/Rome"),testSet);

        // this test pass a start date to getCurrentStart, getCurrentEnd, getPrevStart, getNextEnd functions for each SampleRate
        // then check the result with the passed string containing the expected result
        // this test is executed with UTC and Europe/Rome timezones
        System.out.println("\n\n##########");
        System.out.println("Solar and legal hours");
        System.out.println("##########\n");
        //                                                              getCurrentStart         getCurrentEnd           getPrevStart(1)         getPrevStart(2)         getPrevStart(3)         getPrevStart(-1)        getNextEnd(1)           getNextEnd(2)           getNextEnd(3)           getNextEnd(-1)
        List<String> minutesSolarUTC  = new ArrayList<>(Arrays.asList("2018-03-25 01:59:00.0","2018-03-25 02:00:00.0","2018-03-25 01:58:00.0","2018-03-25 01:57:00.0","2018-03-25 01:56:00.0","2018-03-25 02:00:00.0","2018-03-25 02:01:00.0","2018-03-25 02:02:00.0","2018-03-25 02:03:00.0","2018-03-25 01:59:00.0"));
        List<String> minutesSolarRome = new ArrayList<>(Arrays.asList("2018-03-25 01:59:00.0","2018-03-25 03:00:00.0","2018-03-25 01:58:00.0","2018-03-25 01:57:00.0","2018-03-25 01:56:00.0","2018-03-25 03:00:00.0","2018-03-25 03:01:00.0","2018-03-25 03:02:00.0","2018-03-25 03:03:00.0","2018-03-25 01:59:00.0"));
        List<String> hoursSolarUTC    = new ArrayList<>(Arrays.asList("2018-03-25 01:00:00.0","2018-03-25 02:00:00.0","2018-03-25 00:00:00.0","2018-03-24 23:00:00.0","2018-03-24 22:00:00.0","2018-03-25 02:00:00.0","2018-03-25 03:00:00.0","2018-03-25 04:00:00.0","2018-03-25 05:00:00.0","2018-03-25 01:00:00.0"));
        List<String> hoursSolarRome   = new ArrayList<>(Arrays.asList("2018-03-25 01:00:00.0","2018-03-25 03:00:00.0","2018-03-25 00:00:00.0","2018-03-24 23:00:00.0","2018-03-24 22:00:00.0","2018-03-25 03:00:00.0","2018-03-25 04:00:00.0","2018-03-25 05:00:00.0","2018-03-25 06:00:00.0","2018-03-25 01:00:00.0"));
        List<String> halfdaySolar     = new ArrayList<>(Arrays.asList("2018-03-25 00:00:00.0","2018-03-25 12:00:00.0","2018-03-24 12:00:00.0","2018-03-24 00:00:00.0","2018-03-23 12:00:00.0","2018-03-25 12:00:00.0","2018-03-26 00:00:00.0","2018-03-26 12:00:00.0","2018-03-27 00:00:00.0","2018-03-25 00:00:00.0"));
        List<String> daySolar         = new ArrayList<>(Arrays.asList("2018-03-25 00:00:00.0","2018-03-26 00:00:00.0","2018-03-24 00:00:00.0","2018-03-23 00:00:00.0","2018-03-22 00:00:00.0","2018-03-26 00:00:00.0","2018-03-27 00:00:00.0","2018-03-28 00:00:00.0","2018-03-29 00:00:00.0","2018-03-25 00:00:00.0"));

        // Cambio ora legale/solare
        testSet.clear();
        testSet.put(Minute , addFirst("2018-03-25 01:59:59.0",minutesSolarUTC));
        testSet.put(Hour   , addFirst("2018-03-25 01:59:59.0",hoursSolarUTC  ));
        testSet.put(HalfDay, addFirst("2018-03-25 01:59:59.0",halfdaySolar   ));    // no diff to Rome
        testSet.put(Day    , addFirst("2018-03-25 01:59:59.0",daySolar       ));    // no diff to Rome
        startEndPrevNextTest(TimeZone.getTimeZone("UTC"),testSet);

        testSet.clear();
        testSet.put(Minute , addFirst("2018-03-25 01:59:59.0",minutesSolarRome));
        testSet.put(Hour   , addFirst("2018-03-25 01:59:59.0",hoursSolarRome  ));
        testSet.put(HalfDay, addFirst("2018-03-25 01:59:59.0",halfdaySolar    ));    // no diff to UTC
        testSet.put(Day    , addFirst("2018-03-25 01:59:59.0",daySolar        ));    // no diff to UTC
        startEndPrevNextTest(TimeZone.getTimeZone("Europe/Rome"),testSet);

        // this test pass a start date to getCurrentStart, getCurrentEnd, getPrevStart, getNextEnd functions for each SampleRate
        // then check the result with the passed string containing the expected result
        // this test is executed with UTC and Europe/Rome timezones
        System.out.println("\n\n##########");
        System.out.println("Leap year");
        System.out.println("##########\n");
        //                                                      getCurrentStart         getCurrentEnd           getPrevStart(1)         getPrevStart(2)         getPrevStart(3)         getPrevStart(-1)        getNextEnd(1)           getNextEnd(2)           getNextEnd(3)           getNextEnd(-1)
        List<String> minutesLeap = new ArrayList<>(Arrays.asList("2000-02-28 23:59:00.0","2000-02-29 00:00:00.0","2000-02-28 23:58:00.0","2000-02-28 23:57:00.0","2000-02-28 23:56:00.0","2000-02-29 00:00:00.0","2000-02-29 00:01:00.0","2000-02-29 00:02:00.0","2000-02-29 00:03:00.0","2000-02-28 23:59:00.0"));
        List<String> hoursLeap   = new ArrayList<>(Arrays.asList("2000-02-28 23:00:00.0","2000-02-29 00:00:00.0","2000-02-28 22:00:00.0","2000-02-28 21:00:00.0","2000-02-28 20:00:00.0","2000-02-29 00:00:00.0","2000-02-29 01:00:00.0","2000-02-29 02:00:00.0","2000-02-29 03:00:00.0","2000-02-28 23:00:00.0"));
        List<String> halfdayLeap = new ArrayList<>(Arrays.asList("2000-02-28 12:00:00.0","2000-02-29 00:00:00.0","2000-02-28 00:00:00.0","2000-02-27 12:00:00.0","2000-02-27 00:00:00.0","2000-02-29 00:00:00.0","2000-02-29 12:00:00.0","2000-03-01 00:00:00.0","2000-03-01 12:00:00.0","2000-02-28 12:00:00.0"));
        List<String> dayLeap     = new ArrayList<>(Arrays.asList("2000-02-28 00:00:00.0","2000-02-29 00:00:00.0","2000-02-27 00:00:00.0","2000-02-26 00:00:00.0","2000-02-25 00:00:00.0","2000-02-29 00:00:00.0","2000-03-01 00:00:00.0","2000-03-02 00:00:00.0","2000-03-03 00:00:00.0","2000-02-28 00:00:00.0"));

        // anno bisestile
        testSet.clear();
        testSet.put(Minute , addFirst("2000-02-28 23:59:59.0",minutesLeap));
        testSet.put(Hour   , addFirst("2000-02-28 23:59:59.0",hoursLeap  ));
        testSet.put(HalfDay, addFirst("2000-02-28 23:59:59.0",halfdayLeap));
        testSet.put(Day    , addFirst("2000-02-28 23:59:59.0",dayLeap    ));
        startEndPrevNextTest(TimeZone.getTimeZone("UTC"),testSet);
        startEndPrevNextTest(TimeZone.getTimeZone("Europe/Rome"),testSet);

        System.exit(0);
    }

    private static List<String> addFirst(String str, List<String> l) {
        List<String> res = new ArrayList<>();
        res.add(str);
        res.addAll(l);
        return res;
    }

    // Test functions

    private static void startEndPrevNextTest(TimeZone tz, Map<SampleRate,List<String>> testSet) {
        TimeZone.setDefault(tz);

        for (Map.Entry<SampleRate, List<String>> entry : testSet.entrySet()) {
            SampleRate sr = entry.getKey();
            List<String> tests = entry.getValue();

            String testStr = tests.get(0);
            long testMs = toMSSince(testStr);

            assert toFormattedDate(sr.getStart(testMs)).compareTo(tests.get(1))==0:
                    String.format("Input '%s' generated %s.CurrentStart at %d (as %s) that not equal to expected %d (as %s).",
                            testStr, sr, sr.getStart(testMs), toFormattedDate(sr.getStart(testMs)), toMSSince(tests.get(1)), tests.get(1));
            assert toFormattedDate(sr.getEnd(testMs)).compareTo(tests.get(2))==0:
                    String.format("Input '%s' generated %s.getCurrentEnd at %d (as %s) that not equal to expected %d (as %s).",
                            testStr, sr, sr.getEnd(testMs), toFormattedDate(sr.getEnd(testMs)), toMSSince(tests.get(2)), tests.get(2));

            assert toFormattedDate(sr.getPrevStart(1, testMs)).compareTo(tests.get(3))==0:
                    String.format("Input '%s' generated %s.getPrevStart(1) at %d (as %s) that not equal to expected %d (as %s).",
                            testStr, sr, sr.getPrevStart(1, testMs), toFormattedDate(sr.getPrevStart(1, testMs)), toMSSince(tests.get(3)), tests.get(3));
            assert toFormattedDate(sr.getPrevStart(2, testMs)).compareTo(tests.get(4))==0:
                    String.format("Input '%s' generated %s.getPrevStart(2) at %d (as %s) that not equal to expected %d (as %s).",
                            testStr, sr, sr.getPrevStart(2, testMs), toFormattedDate(sr.getPrevStart(2, testMs)), toMSSince(tests.get(4)), tests.get(4));
            assert toFormattedDate(sr.getPrevStart(3, testMs)).compareTo(tests.get(5))==0:
                    String.format("Input '%s' generated %s.getPrevStart(3) at %d (as %s) that not equal to expected %d (as %s).",
                            testStr, sr, sr.getPrevStart(3, testMs), toFormattedDate(sr.getPrevStart(3, testMs)), toMSSince(tests.get(5)), tests.get(5));
            assert toFormattedDate(sr.getPrevStart(-1, testMs)).compareTo(tests.get(6))==0:
                    String.format("Input '%s' generated %s.getPrevStart(-1) at %d (as %s) that not equal to expected %d (as %s).",
                            testStr, sr, sr.getPrevStart(-1, testMs), toFormattedDate(sr.getPrevStart(-1, testMs)), toMSSince(tests.get(6)), tests.get(6));

            assert toFormattedDate(sr.getNextEnd(1, testMs)).compareTo(tests.get(7))==0:
                    String.format("Input '%s' generated %s.getNextEnd(1) at %d (as %s) that not equal to expected %d (as %s).",
                            testStr, sr, sr.getNextEnd(1, testMs), toFormattedDate(sr.getNextEnd(1, testMs)), toMSSince(tests.get(7)), tests.get(7));
            assert toFormattedDate(sr.getNextEnd(2, testMs)).compareTo(tests.get(8))==0:
                    String.format("Input '%s' generated %s.getNextEnd(2) at %d (as %s) that not equal to expected %d (as %s).",
                            testStr, sr, sr.getNextEnd(2, testMs), toFormattedDate(sr.getNextEnd(2, testMs)), toMSSince(tests.get(8)), tests.get(8));
            assert toFormattedDate(sr.getNextEnd(3, testMs)).compareTo(tests.get(9))==0:
                    String.format("Input '%s' generated %s.getNextEnd(3) at %d (as %s) that not equal to expected %d (as %s).",
                            testStr, sr, sr.getNextEnd(3, testMs), toFormattedDate(sr.getNextEnd(3, testMs)), toMSSince(tests.get(9)), tests.get(9));
            assert toFormattedDate(sr.getNextEnd(-1, testMs)).compareTo(tests.get(10))==0:
                    String.format("Input '%s' generated %s.getNextEnd(-1) at %d (as %s) that not equal to expected %d (as %s).",
                            testStr, sr, sr.getNextEnd(-1, testMs), toFormattedDate(sr.getNextEnd(-1, testMs)), toMSSince(tests.get(10)), tests.get(10));

            assert tests.get(1).compareTo(tests.get(10)) == 0 : String.format("CurrentStart ['%s'] must be equal to getNextEnd(-1) ['%s]", tests.get(1),tests.get(10));
            assert tests.get(2).compareTo(tests.get(6)) == 0 : String.format("CurrentEnd ['%s'] must be equal to getPrevStart(-1) ['%s]", tests.get(2),tests.get(6));

            System.out.println(String.format("%s tested successfully on %d dates.", sr, tests.size()));
        }
    }

    // Test utils

    protected static long test_simpleGetCall(String timestamp, Function<Long,Long> f) {
        return test_simpleGetCall(toMSSince(timestamp),f);
    }

    protected static long test_simpleGetCall(long ms, Function<Long,Long> f) {
        return f.apply(ms);
    }

    protected static long test_simpleGetCountCall(int count, String timestamp, BiFunction<Integer,Long,Long> f) {
        return test_simpleGetCountCall(count,toMSSince(timestamp),f);
    }

    protected static long test_simpleGetCountCall(int count, long ms, BiFunction<Integer,Long,Long> f) {
        return f.apply(count,ms);
    }

    protected static void test_simpleGetPrint(String timestamp, Function<Long,Long> f) {
        test_simpleGetPrint(toMSSince(timestamp),f);
    }

    protected static void test_simpleGetPrint(long ms, Function<Long,Long> f) {
        System.out.println(String.format("Input timestamp %d (as %s)", ms, toFormattedDate(ms)));
        long res = f.apply(ms);
        System.out.println(String.format("Output timestamp %d (as %s)", res, toFormattedDate(res)));
    }

    protected static void test_simpleGetCountPrint(int count, String timestamp, BiFunction<Integer,Long,Long> f) {
        test_simpleGetCountPrint(count,toMSSince(timestamp),f);
    }

    protected static void test_simpleGetCountPrint(int count, long ms, BiFunction<Integer,Long,Long> f) {
        String msg = String.format("Input %d (as %s)", ms, toFormattedDate(ms));
        //System.out.println(String.format("Input timestamp %d (as %s)", ms, toFormattedDate(ms)));
        long res = f.apply(count,ms);

        msg += " => ";
        msg += String.format("Output %d (as %s)", res, toFormattedDate(res));
        //System.out.println(String.format("Output timestamp %d (as %s)", res, toFormattedDate(res)));
        System.out.println(msg);
    }

}
