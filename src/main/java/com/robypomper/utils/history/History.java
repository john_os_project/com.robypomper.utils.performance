package com.robypomper.utils.history;

import com.robypomper.utils.performance.Performance;

import java.sql.Timestamp;
import java.util.*;
import java.util.stream.Collectors;

/**
 * History storage class.
 *
 * This Data Class allow to store T objects (that implements Timed interface)
 * by Timestamp and SampleRate. The this objects can be accessed by SampleRate,
 * time range, specific timestamp...
 *
 * ToDo rewrite tests documentation
 *
 * @param <T> objects to be stored in the History object, it must implement Timed
 *           interface.
 * @since 0.1
 */
@SuppressWarnings({"WeakerAccess", "unused"})
public class History <T extends Timed> {

    // ------
    // Fields
    // ------

    private Map<SampleRate, Map<Long,T>> history;


    // -----------
    // Constructor
    // -----------

    public History() {
        clean();
    }

    public History(Map<SampleRate, Map<Long,T>> historyData) {
        this.history = historyData;
    }

    public void clean() {
        this.history = new TreeMap<>();
    }


    // ------
    // Adders
    // ------

    @SuppressWarnings("Java8MapApi")
    public void add(SampleRate sampleRate, T object) {
        if (history.get(sampleRate)==null) history.put(sampleRate,new TreeMap<>());
        history.get(sampleRate).put(sampleRate.getStart(object.getTime()),object);
    }

    public void add(SampleRate sampleRate, List<T> objects) {
        for (T obj : objects)
            add(sampleRate, obj);
    }

    public void remove(SampleRate sampleRate, T object) {
        history.get(sampleRate).remove(object.getTime(),object);
    }


    // -------
    // Getters
    // -------

    public T get(SampleRate sampleRate, long at) {
        Collection<T> res = getBySampleRate(sampleRate,at,true).values();
        return res.iterator().next();
    }

    public Map<SampleRate,Map<Long,T>> get(long at, boolean includeNull) {
        Map<SampleRate,Map<Long,T>> results = new TreeMap<>();

        for (SampleRate sr : SampleRate.values()) {
            if (history.get(sr)==null)
                if (includeNull) {
                    results.put(sr, new TreeMap<>());
                    results.get(sr).put(sr.getStart(at),null);
                    continue;
                }
            T obj = history.get(sr).get(sr.getStart(at));

            if (obj!=null || includeNull) {
                results.put(sr, new TreeMap<>());
                results.get(sr).put(sr.getStart(at),obj);
            }
        }
        return results;
    }

    @SuppressWarnings("Java8MapApi")
    public Map<SampleRate,Map<Long,T>> get(long from, long to, boolean includeNull, boolean includeBounds) {
        Map<SampleRate,Map<Long,T>> results = new TreeMap<>();

        for (SampleRate sr : SampleRate.values()) {
            long to_sr = to;

            if (from == to_sr)
                to_sr = to_sr+1;
            if (sr.getEnd(to_sr) == to_sr + sr.getLength())
                to_sr = to_sr-1;
            long start = sr.getStart(from);
            long end = sr.getEnd(to_sr);

            long iterator = start;
            while (iterator < end) {
                T obj = history.get(sr).get(iterator);

                if (obj!=null || includeNull) {

                    if (results.get(sr)==null)
                        results.put(sr, new TreeMap<>());

                    //noinspection ConstantConditions
                    if (obj==null && includeNull)                                                   // NotFound + includeNull => Null
                        results.get(sr).put(sr.getStart(iterator),null);
                    else if (includeBounds)                                                         // Found + includeBounds => Obj
                        results.get(sr).put(sr.getStart(iterator),obj);
                    else if (from <= sr.getStart(obj.getTime()) && sr.getEnd(obj.getTime()) <= to)  // Found + NOT includeBounds + Inside = Obj
                        results.get(sr).put(sr.getStart(iterator),obj);
                    else if (includeNull)                                                           // Found + NOT includeBounds + NOT Inside + includeNull = Null
                        results.get(sr).put(sr.getStart(iterator),null);
                    else                                                                            // Found + NOT includeBounds + NOT Inside + NOT includeNull = remove SampleRate added at start of while
                        results.remove(sr);
                }
                iterator = sr.getPrevStart(-1,iterator);
            }
        }

        return results;
    }

    public Map<Long,T> getBySampleRate(SampleRate sampleRate, boolean includeNull, boolean allSampleRate) {
        Map<Long,T> samples = history.get(sampleRate);
        if (!includeNull)
            return samples;

        long start;
        long end;
        if (allSampleRate) {
            start = sampleRate.getStart(getFirst().getTime());
            end = sampleRate.getEnd(getLast().getTime());
        } else {
            start = sampleRate.getStart(getFirst(sampleRate).getTime());
            end = sampleRate.getEnd(getLast(sampleRate).getTime());
        }

        Map<Long,T> samplesResults = new TreeMap<>();
        long iterator = start;
        while (iterator < end) {
            samplesResults.put(iterator,samples.get(iterator));
            iterator = sampleRate.getPrevStart(-1,iterator);
        }

        return samplesResults;
    }

    public Map<Long,T> getBySampleRate(SampleRate sampleRate, long at, boolean includeNull) {       // convenient functions (@see getSingle(SampleRate sampleRate, long at))
        Map<Long,T> samplesFiltred = new TreeMap<>();

        Map<Long,T> samples = history.get(sampleRate);
        if (samples==null && includeNull) {
            samplesFiltred.put(sampleRate.getStart(at),null);
            return samplesFiltred;
        }

        T obj = null;
        if (samples!=null)
            obj = samples.get(sampleRate.getStart(at));
        if (obj!=null || includeNull)
            samplesFiltred.put(sampleRate.getStart(at),obj);

        return samplesFiltred;
    }

    public Map<Long,T> getBySampleRate(SampleRate sampleRate, long from, long to, boolean includeNull, boolean includeBounds) {
        Map<Long,T> results = new TreeMap<>();

        long to_sr = to;

        if (from == to_sr)
            to_sr = to_sr+1;
        if (sampleRate.getEnd(to_sr) == to_sr + sampleRate.getLength())
            to_sr = to_sr-1;
        long start = sampleRate.getStart(from);
        long end = sampleRate.getEnd(to_sr);

        long iterator = start;
        while (iterator < end) {
            T obj = history.get(sampleRate).get(iterator);

            if (obj!=null || includeNull) {


                //noinspection ConstantConditions
                if (obj==null && includeNull)                                                   // NotFound + includeNull => Null
                    results.put(sampleRate.getStart(iterator),null);
                else if (includeBounds)                                                         // Found + includeBounds => Obj
                    results.put(sampleRate.getStart(iterator),obj);
                else if (from <= sampleRate.getStart(obj.getTime())
                        && sampleRate.getEnd(obj.getTime()) <= to)                              // Found + NOT includeBounds + Inside = Obj
                    results.put(sampleRate.getStart(iterator),obj);
                else if (includeNull)                                                           // Found + NOT includeBounds + NOT Inside + includeNull = Null
                    results.put(sampleRate.getStart(iterator),null);

            }
            iterator = sampleRate.getPrevStart(-1,iterator);
        }

        return results;
    }

    public Map<SampleRate,Map<Long,T>> getCurrent(boolean includeNull) { return get(Performance.getMillisSince(),includeNull); }

    public Map<SampleRate,Map<Long,T>> getPrev(boolean includeNull) {
        Map<SampleRate,Map<Long,T>> result = new TreeMap<>();

        for (SampleRate sr : SampleRate.values()) {
            result.put(sr,new TreeMap<>());
            T obj = getPrev(sr);
            if (obj!=null || includeNull)
                result.get(sr).put(sr.getPrevStart(1),obj);
        }
        return result;
    }

    public Map<SampleRate,Map<Long,T>> getNext(boolean includeNull) {
        Map<SampleRate,Map<Long,T>> result = new TreeMap<>();

        for (SampleRate sr : SampleRate.values()) {
            result.put(sr,new TreeMap<>());
            T obj = getNext(sr);
            if (obj!=null || includeNull)
                result.get(sr).put(sr.getPrevStart(-1),obj);
        }
        return result;
    }

    public T getCurrent(SampleRate sampleRate) {
        return history.get(sampleRate).get(sampleRate.getStart(Performance.getMillisSince()));
    }

    public T getPrev(SampleRate sampleRate) {
        return history.get(sampleRate).get(sampleRate.getStart(sampleRate.getPrevStart(1)));
    }

    public T getNext(SampleRate sampleRate) {
        return history.get(sampleRate).get(sampleRate.getPrevStart(-1));
    }

    public T  getFirst() {
        return getFirst(history);
    }

    public T  getLast() {
        return getLast(history);
    }

    public T  getFirst(SampleRate sampleRate) {
        Map<Long,T> samples = history.get(sampleRate);
        return getFirstInSamples(samples);
    }

    public T  getLast(SampleRate sampleRate) {
        Map<Long,T> samples = history.get(sampleRate);
        return getLastInSamples(samples);
    }

    public static <T extends Timed> T getFirst(Map<SampleRate,Map<Long,T>> historyResults) {
        T firstObj = null;
        for (SampleRate sr : SampleRate.values()) {
            T firstSample = getFirstInSamples(historyResults.get(sr));
            if (firstSample==null)
                continue;

            if (firstObj==null) {
                firstObj = firstSample;
                continue;
            }

            if (firstObj.getTime()>firstSample.getTime())
                firstObj = firstSample;
        }

        return firstObj;
    }

    public static <T extends Timed> T getLast(Map<SampleRate,Map<Long,T>> historyResults) {
        T lastObj = null;
        for (SampleRate sr : SampleRate.values()) {
            T lastSample = getLastInSamples(historyResults.get(sr));
            if (lastObj==null) {
                lastObj = lastSample;
                continue;
            }

            if (lastObj.getTime()<lastSample.getTime())
                lastObj = lastSample;
        }

        return lastObj;
    }

    @Deprecated
    public static <T extends Timed> T getFirst(Map<SampleRate,Map<Long,T>> historyResults, SampleRate sampleRate) {
        Map<Long,T> samples = historyResults.get(sampleRate);
        if (samples==null) return null;

        if (!samples.values().iterator().hasNext())
            return null;

        return samples.values().iterator().next();
    }

    public static <T extends Timed> T getFirstInSamples(Map<Long,T> samples) {
        if (!samples.values().iterator().hasNext())
            return null;

        return samples.values().iterator().next();
    }

    public static <T extends Timed> T getLastInSamples(Map<Long,T> samples) {
        Iterator<Map.Entry<Long,T>> itr = samples.entrySet().iterator();
        Map.Entry<Long,T> lastElement = itr.next();
        while(itr.hasNext()) {
            lastElement=itr.next();
        }
        return lastElement.getValue();
    }


    // --------
    // Printers
    // --------

    public String printStatus() {
        String status = "";
        status += printCountsBySampleRates() + "\n";
        status += printCurrentsStatus() + "\n";
        status += printLatestStatus() + "\n";

        return status;
    }
    
    public String printCountsBySampleRates() {
        StringBuilder srCounts = new StringBuilder();
        for (Map.Entry<SampleRate, Map<Long,T>> entry : history.entrySet()) {
            SampleRate sr = entry.getKey();
            int count = entry.getValue().size();
            srCounts.append(String.format("%s: %d;\t", sr, count));
        }
        String msg = srCounts.length() == 0 ? "Empty" : srCounts.substring(0, srCounts.length() - 2);
        return String.format("Counts per SampleRate > %s", srCounts.toString());
//        Collection<Map.Entry<SampleRate,Map<Long, T>>> history = sortHistoryByLength(this.history);
//        StringBuilder srCounts = new StringBuilder();
//        for (Map.Entry<SampleRate, Map<Long,T>> entry : history) {
//            SampleRate sr = entry.getKey();
//            int count = entry.getValue().size();
//            srCounts.append(String.format("%s: %d;\t", sr, count));
//        }
//        String msg = srCounts.length() == 0 ? "Empty" : srCounts.substring(0, srCounts.length() - 2);
//        return String.format("Counts per SampleRate > %s", srCounts.toString());
    }

    public String printCurrentsStatus() {
        Map<SampleRate,Map<Long, T>> currents = getCurrent(true);
        return printHistoryMap(currents);
//        String currAvailability = "";
//        for (Map.Entry<Pair<SampleRate,Long>, T> entry : currents.entrySet()) {
//            SampleRate sr = entry.getKey().getKey();
//            long time = entry.getKey().getValue();
//            String availability = entry.getValue()!=null ? entry.getValue().toString() : "N/A                               ";
//            currAvailability += String.format("%s (%s): %s;\t",sr,new Timestamp(time),availability);
//        }
//        currAvailability = currAvailability.isEmpty() ? "" : currAvailability.substring(0,currAvailability.length()-2);
//        return String.format("Currents Availability > %s", currAvailability);
    }

    public String printLatestStatus() {
        Map<SampleRate,Map<Long, T>> currents = getPrev(true);
        return printHistoryMap(currents);
//        Map<Pair<SampleRate, Long>, T> latest = getLatest(true);
//        String latestAvailability = "";
//        for (Map.Entry<Pair<SampleRate, Long>, T> entry : latest.entrySet()) {
//            SampleRate sr = entry.getKey().getKey();
//            long time = entry.getKey().getValue();
//            String availability = entry.getValue() != null ? entry.getValue().toString() : "N/A                               ";
//            latestAvailability += String.format("%s (%s): %s;\t", sr, new Timestamp(time), availability);
//        }
//        latestAvailability = latestAvailability.isEmpty() ? "" : latestAvailability.substring(0, latestAvailability.length() - 2);
//        return String.format("Latest Availability   > %s", latestAvailability);
    }

    public static <T extends Timed> String printHistoryMap(Map<SampleRate,Map<Long,T>> history) {
        StringBuilder msg = new StringBuilder();
        for (Map.Entry<SampleRate,Map<Long,T>> entry : history.entrySet()) {
            SampleRate sr = entry.getKey();
            int elements = entry.getValue().size();

            if (elements==0)
                msg.append(String.format("%7s@ -- : no items", sr));

            T obj = entry.getValue().entrySet().iterator().next().getValue();
            long time = entry.getValue().entrySet().iterator().next().getKey();

            if (elements==1)
                msg.append(String.format("%7s@%s: %s\n", sr, SampleRate.toFormattedDate(time), obj == null ? "Null" : SampleRate.toFormattedDate(obj.getTime())));

            if (elements>1)
                msg.append(String.format("%7s@%s: %s, %d more...\n", sr, SampleRate.toFormattedDate(time), SampleRate.toFormattedDate(obj.getTime()), elements - 1));

        }
        return msg.toString();
    }

    public static <T extends Timed> String printSampleRateMap(SampleRate sampleRate, Map<Long,T> samples, boolean compat) {
        StringBuilder msg = new StringBuilder();
        int elements = samples.size();

        if (elements==0) {
            msg.append(String.format("- %7s@ -- : no items\n", sampleRate));
            return msg.toString();
        }

        T obj = samples.entrySet().iterator().next().getValue();
        long time = samples.entrySet().iterator().next().getKey();
        String objStr = obj == null ? "Null" : SampleRate.toFormattedDate(obj.getTime());
        String timeStr = SampleRate.toFormattedDate(time);

        if (elements==1)
            msg.append(String.format("- %7s@%s: %s\n", sampleRate, timeStr, objStr));

        T objLast = getLastInSamples(samples);
        long timeLast = objLast == null ? 0 : sampleRate.getStart(objLast.getTime());
        String objLastStr = objLast == null ? "Null" : SampleRate.toFormattedDate(objLast.getTime());
        String timeLastStr = timeLast==0 ? "--" : SampleRate.toFormattedDate(timeLast);

        if (elements>1 && compat)
            msg.append(String.format("- %7s@%s: %s, %d more with latest time %s...\n", sampleRate, timeStr, objStr, elements - 1, timeLastStr));

        if (elements>1 && !compat)
            for (Map.Entry<Long,T> entry : samples.entrySet()) {
                T objElem = entry.getValue();
                long timeElem = entry.getKey();
                String objElemStr = objElem == null ? "Null" : SampleRate.toFormattedDate(objElem.getTime());
                String timeElemStr = SampleRate.toFormattedDate(timeElem);

                msg.append(String.format("- %7s@%s: %s\n", sampleRate, timeElemStr, objElemStr));
            }

        return msg.toString();
    }


    // -------------------------------------------------------------------------
    // -------------------------------------------------------------------------

    // -------
    // Testing
    // -------

    @SuppressWarnings("PointlessArithmeticExpression")
    public static void main(String[] args) {
        SampleRate testSampleRate = SampleRate.Minute;
        History<Test_History_Timed> history = new History<>();
        List<Test_History_Timed> testSet;
        long refTime;


        // add a single timed object and get it back
        System.out.println("\n\n#############");
        System.out.println("simpleAddTest");
        System.out.println("#############\n");
        history.clean();
        simpleAddTest(history,testSampleRate,new Test_History_Timed(Performance.getMillisSince()));

        // add multiple timed object and get it back
        System.out.println("\n\n###########");
        System.out.println("multipleAdd");
        System.out.println("###########\n");
        history.clean();
        testSet = new ArrayList<>();
        testSet.add(new Test_History_Timed(Performance.getMillisSince() - testSampleRate.getLength()*1));
        testSet.add(new Test_History_Timed(Performance.getMillisSince() - testSampleRate.getLength()*2));
        testSet.add(new Test_History_Timed(Performance.getMillisSince() - testSampleRate.getLength()*3));
        multipleAdd(history,testSampleRate,testSet);

        // add multiple timed but all for the same SampleRate.getStart(), only last object added will be stored, others are overwrite
        System.out.println("\n\n###########");
        System.out.println("addAndOverwrite");
        System.out.println("###########\n");
        history.clean();
        testSet = new ArrayList<>();
        testSet.add(new Test_History_Timed(testSampleRate.getCurrentStart() + 5));
        testSet.add(new Test_History_Timed(testSampleRate.getCurrentStart() + 10));
        testSet.add(new Test_History_Timed(testSampleRate.getCurrentStart() + 15));
        addAndOverwrite(history,testSampleRate,testSet);

        // Getter at
        System.out.println("\n\n#############");
        System.out.println("simpleGetAtTest");
        System.out.println("#############\n");
        history.clean();
        refTime = SampleRate.toMSSince("2018-01-01 00:00:00.0");
        test_populate3Days(history, refTime);
        simpleGetAtTest(history,refTime);

        // Getter from to
        System.out.println("\n\n###################");
        System.out.println("simpleGetFromToTest");
        System.out.println("###################\n");
        history.clean();
        refTime = SampleRate.toMSSince("2018-01-01 00:00:00.0");
        test_populate3Days(history, refTime);
        simpleGetFromToTest(history,refTime);

        // Getter by SampleRate
        System.out.println("\n\n#####################");
        System.out.println("simpleGetBySampleRate");
        System.out.println("#####################\n");
        history.clean();
        refTime = SampleRate.toMSSince("2018-01-01 00:00:00.0");
        test_populate3Days(history, refTime);
        simpleGetBySampleRateFromToTest(history,refTime);


        // Getter by SampleRate
        System.out.println("\n\n#####################");
        System.out.println("simpleGetCurrentPrevNext");
        System.out.println("#####################\n");
        history.clean();
        refTime = SampleRate.toMSSince("2018-01-01 00:00:00.0");
        test_populate3Days(history, refTime);
        simpleGetCurrentPrevNextTest(history,refTime);
    }

    // Test functions

    // Tested functions
    //public void add(SampleRate sampleRate, T object)
    private static <T extends Timed> void simpleAddTest(History<T> h, SampleRate sampleRate, T object) {
        h.add(sampleRate,object);
        Map<Long,T> results = h.getBySampleRate(sampleRate,object.getTime(),false);

        assert results.size()==1;
        assert results.get(sampleRate.getStart(object.getTime())) == object;
    }

    // Tested functions
    //public void add(SampleRate sampleRate, List<T> objects)
    private static <T extends Timed> void multipleAdd(History<T> h, SampleRate sampleRate, List<T> objects) {
        h.add(sampleRate,objects);
        Map<Long,T> results = h.getBySampleRate(sampleRate,false,false);

        assert results.size()==3;
        objects = objects.stream().sorted((o1, o2)-> (int)(o1.getTime() - o2.getTime())).collect(Collectors.toList());
        int count = 0;
        for (Map.Entry<Long,T> entry : results.entrySet()) {
            assert entry.getValue() == objects.get(count);
            count++;
        }
    }

    // Tested functions
    //public void add(SampleRate sampleRate, List<T> objects)
    private static <T extends Timed> void addAndOverwrite(History<T> h, SampleRate sampleRate, List<T> objects) {
        h.add(sampleRate,objects);
        Map<Long,T> results = h.getBySampleRate(sampleRate,false,false);

        assert results.size()==1;
        assert results.values().toArray()[0] == objects.get(objects.size()-1);
    }

    // Tested functions
    //public Map<SampleRate,Map<Long,T>> get(long at, boolean includeNull)
    @SuppressWarnings({"UnnecessaryLocalVariable", "ConstantConditions"})
    private static <T extends Timed> void simpleGetAtTest(History<T> history, long refTime) {
        long startTime = refTime;
        long midTime = refTime + (SampleRate.Day.getLength() * 3 / 2);
        long endTime =  refTime + (SampleRate.Day.getLength() * 3) - 1;
        long postTime =  refTime + (SampleRate.Day.getLength() * 3);
        long postPostTime =  refTime + (SampleRate.Day.getLength() * 4);

        Map<SampleRate,Map<Long,T>> getted;

        // at Start
        getted = history.get(startTime,true);
        System.out.println(String.format("Start@%s", new Timestamp(startTime)));
        System.out.println(printHistoryMap(getted));
        assert SampleRate.Minute .getStart(startTime) == SampleRate.Minute .getStart(getFirstInSamples(getted.get(SampleRate.Minute )).getTime());
        assert SampleRate.Hour   .getStart(startTime) == SampleRate.Hour   .getStart(getFirstInSamples(getted.get(SampleRate.Hour   )).getTime());
        assert SampleRate.HalfDay.getStart(startTime) == SampleRate.HalfDay.getStart(getFirstInSamples(getted.get(SampleRate.HalfDay)).getTime());
        assert SampleRate.Day    .getStart(startTime) == SampleRate.Day    .getStart(getFirstInSamples(getted.get(SampleRate.Day    )).getTime());
        assert getted.size()==4;

        // at Mid
        getted = history.get(midTime,true);
        System.out.println(String.format("Mid@%s", new Timestamp(midTime)));
        System.out.println(printHistoryMap(getted));
        assert SampleRate.Minute .getStart(midTime) == SampleRate.Minute .getStart(getFirstInSamples(getted.get(SampleRate.Minute )).getTime());
        assert SampleRate.Hour   .getStart(midTime) == SampleRate.Hour   .getStart(getFirstInSamples(getted.get(SampleRate.Hour   )).getTime());
        assert SampleRate.HalfDay.getStart(midTime) == SampleRate.HalfDay.getStart(getFirstInSamples(getted.get(SampleRate.HalfDay)).getTime());
        assert SampleRate.Day    .getStart(midTime) == SampleRate.Day    .getStart(getFirstInSamples(getted.get(SampleRate.Day    )).getTime());
        assert getted.size()==4;

        // at End
        getted = history.get(endTime,true);
        System.out.println(String.format("End@%s", new Timestamp(endTime)));
        System.out.println(printHistoryMap(getted));
        assert SampleRate.Minute .getStart(endTime) == SampleRate.Minute .getStart(getFirstInSamples(getted.get(SampleRate.Minute )).getTime());
        assert SampleRate.Hour   .getStart(endTime) == SampleRate.Hour   .getStart(getFirstInSamples(getted.get(SampleRate.Hour   )).getTime());
        assert SampleRate.HalfDay.getStart(endTime) == SampleRate.HalfDay.getStart(getFirstInSamples(getted.get(SampleRate.HalfDay)).getTime());
        assert SampleRate.Day    .getStart(endTime) == SampleRate.Day    .getStart(getFirstInSamples(getted.get(SampleRate.Day    )).getTime());
        assert getted.size()==4;

        // at Post
        getted = history.get(postTime,true);
        System.out.println(String.format("Post@%s", new Timestamp(postTime)));
        System.out.println(printHistoryMap(getted));
        assert SampleRate.Minute .getStart(postTime) == SampleRate.Minute .getStart(getFirstInSamples(getted.get(SampleRate.Minute )).getTime());
        assert null == getFirstInSamples(getted.get(SampleRate.Hour   ));
        assert null == getFirstInSamples(getted.get(SampleRate.HalfDay));
        assert null == getFirstInSamples(getted.get(SampleRate.Day    ));
        assert getted.size()==4;

        getted = history.get(postTime,false);
        System.out.println(String.format("Post@%s (incudeNull=false)", new Timestamp(postTime)));
        System.out.println(printHistoryMap(getted));
        assert getted.size()==1;
        // todo check value

        // at PostPost
        getted = history.get(postPostTime,true);
        System.out.println(String.format("PostPost@%s", new Timestamp(postPostTime)));
        System.out.println(printHistoryMap(getted));
        assert null == getFirstInSamples(getted.get(SampleRate.Minute ));
        assert null == getFirstInSamples(getted.get(SampleRate.Hour   ));
        assert null == getFirstInSamples(getted.get(SampleRate.HalfDay));
        assert null == getFirstInSamples(getted.get(SampleRate.Day    ));
        assert getted.size()==4;


        getted = history.get(postPostTime,false);
        System.out.println(String.format("PostPost@%s (includeNull=false)", new Timestamp(postPostTime)));
        System.out.println(printHistoryMap(getted));
        assert getted.size()==0;
    }

    // Tested functions
    //public Map<SampleRate,Map<Long,T>> get(long from, long to, boolean includeNull, boolean includeBounds)
    @SuppressWarnings({"UnnecessaryLocalVariable", "ConstantConditions"})
    private static<T extends Timed> void simpleGetFromToTest(History<T> history, long refTime) {

        long startMinuteStartTime = refTime;
        long startMinuteEndTime = startMinuteStartTime + SampleRate.Minute.getLength();
        long startHourStartTime = refTime;
        long startHourEndTime = startHourStartTime + SampleRate.Hour.getLength();
        long midMinuteStartTime = refTime + ( SampleRate.Day.getLength() / 2 ) + ( SampleRate.HalfDay.getLength() / 2 ) + ( SampleRate.Hour.getLength() / 2 );
        long midMinuteEndTime = midMinuteStartTime + SampleRate.Minute.getLength();
        long midHourStartTime = refTime + ( SampleRate.Day.getLength() / 2 ) + ( SampleRate.HalfDay.getLength() / 2 );
        long midHourEndTime = midHourStartTime + SampleRate.Hour.getLength();
        long endMinuteStartTime = refTime + SampleRate.Day.getLength() - SampleRate.Minute.getLength();
        long endMinuteEndTime = endMinuteStartTime + SampleRate.Minute.getLength();
        long afterMinuteStartTime = refTime+ SampleRate.Day.getLength()*3;
        long afterMinuteEndTime = afterMinuteStartTime + SampleRate.Minute.getLength();
        long sameTime = refTime;

        Map<SampleRate,Map<Long,T>> getted;

        // at Start - Range 1 Minute    (tutti all'inizio)
        System.out.println(String.format("Start, range 1 Minute @ %s > %s",new Timestamp(startMinuteStartTime),new Timestamp(startMinuteEndTime)));
        getted = history.get(startMinuteStartTime,startMinuteEndTime,true,true);
        System.out.println(printHistoryMap(getted));
        assert SampleRate.Minute .getStart(startMinuteStartTime) == SampleRate.Minute .getStart(getFirstInSamples(getted.get(SampleRate.Minute)).getTime());
        assert SampleRate.Hour   .getStart(startMinuteStartTime) == SampleRate.Hour   .getStart(getFirstInSamples(getted.get(SampleRate.Hour   )).getTime());
        assert SampleRate.HalfDay.getStart(startMinuteStartTime) == SampleRate.HalfDay.getStart(getFirstInSamples(getted.get(SampleRate.HalfDay)).getTime());
        assert SampleRate.Day    .getStart(startMinuteStartTime) == SampleRate.Day    .getStart(getFirstInSamples(getted.get(SampleRate.Day    )).getTime());
        assert getted.size()==4;

        // includeBounds = false     at Start - Range 1 Minute    (tutti all'inizio)
        System.out.println(String.format("Start, range 1 Minute @ %s > %s (includeBounds=false)",new Timestamp(startMinuteStartTime),new Timestamp(startMinuteEndTime)));
        getted = history.get(startMinuteStartTime,startMinuteEndTime,true,false);
        System.out.println(printHistoryMap(getted));
        assert SampleRate.Minute .getStart(startMinuteStartTime) == SampleRate.Minute .getStart(getFirstInSamples(getted.get(SampleRate.Minute )).getTime());
        assert null == getFirstInSamples(getted.get(SampleRate.Hour   ));
        assert null == getFirstInSamples(getted.get(SampleRate.HalfDay));
        assert null == getFirstInSamples(getted.get(SampleRate.Day    ));
        assert getted.size()==4;

        // includeBounds = false     at Start - Range 1 Hour    (tutti all'inizio)
        System.out.println(String.format("Start, range 1 Hour @ %s > %s (includeBounds=false)",new Timestamp(startHourStartTime),new Timestamp(startHourEndTime)));
        getted = history.get(startHourStartTime,startHourEndTime,true,false);
        System.out.println(printHistoryMap(getted));
        assert SampleRate.Minute .getStart(startHourStartTime) == SampleRate.Minute .getStart(getFirstInSamples(getted.get(SampleRate.Minute )).getTime());
        assert SampleRate.Hour   .getStart(startHourStartTime) == SampleRate.Hour   .getStart(getFirstInSamples(getted.get(SampleRate.Hour   )).getTime());
        assert null == getFirstInSamples(getted.get(SampleRate.HalfDay));
        assert null == getFirstInSamples(getted.get(SampleRate.Day    ));
        assert getted.size()==4;
        assert getted.get(SampleRate.Minute).size()==60;

        // includeBounds = false, includeNull = false     at Start - Range 1 Minute    (tutti all'inizio)
        System.out.println(String.format("Start, range 1 Minute @ %s > %s (includeBounds=false, includeNull=false)",new Timestamp(startMinuteStartTime),new Timestamp(startMinuteEndTime)));
        getted = history.get(startMinuteStartTime,startMinuteEndTime,false,false);
        System.out.println(printHistoryMap(getted));
        assert SampleRate.Minute .getStart(startMinuteStartTime) == SampleRate.Minute .getStart(getFirstInSamples(getted.get(SampleRate.Minute )).getTime());
        assert null == getted.get(SampleRate.Hour   );
        assert null == getted.get(SampleRate.HalfDay);
        assert null == getted.get(SampleRate.Day    );
        assert getted.size()==1;

        // includeBounds = false, includeNull = true     at Start - Range 1 Minute    (tutti all'inizio) todo trovare un'altro intervallo dove non tutti i SampleRate siano presenti
        System.out.println(String.format("Start, range 1 Minute @ %s > %s (includeBounds=false, includeNull=true)",new Timestamp(startMinuteStartTime),new Timestamp(startMinuteEndTime)));
        getted = history.get(startMinuteStartTime,startMinuteEndTime,false,true);
        System.out.println(printHistoryMap(getted));
        assert SampleRate.Minute .getStart(startMinuteStartTime) == SampleRate.Minute .getStart(getFirstInSamples(getted.get(SampleRate.Minute )).getTime());
        assert SampleRate.Hour   .getStart(startMinuteStartTime) == SampleRate.Hour   .getStart(getFirstInSamples(getted.get(SampleRate.Hour   )).getTime());
        assert SampleRate.HalfDay.getStart(startMinuteStartTime) == SampleRate.HalfDay.getStart(getFirstInSamples(getted.get(SampleRate.HalfDay)).getTime());
        assert SampleRate.Day    .getStart(startMinuteStartTime) == SampleRate.Day    .getStart(getFirstInSamples(getted.get(SampleRate.Day    )).getTime());


        // at Mid - Range 1 Minute    (dall'inizio Day/2 + HalfDay/2 * Hour/2 => 18:30:00)
        System.out.println(String.format("Mid, range 1 Minute @%s > %s",new Timestamp(midMinuteStartTime),new Timestamp(midMinuteEndTime)));
        getted = history.get(midMinuteStartTime,midMinuteEndTime,true,true);
        System.out.println(printHistoryMap(getted));
        assert SampleRate.Minute .getStart(midMinuteStartTime) == SampleRate.Minute .getStart(getFirstInSamples(getted.get(SampleRate.Minute )).getTime());
        assert SampleRate.Hour   .getStart(midMinuteStartTime) == SampleRate.Hour   .getStart(getFirstInSamples(getted.get(SampleRate.Hour   )).getTime());
        assert SampleRate.HalfDay.getStart(midMinuteStartTime) == SampleRate.HalfDay.getStart(getFirstInSamples(getted.get(SampleRate.HalfDay)).getTime());
        assert SampleRate.Day    .getStart(midMinuteStartTime) == SampleRate.Day    .getStart(getFirstInSamples(getted.get(SampleRate.Day    )).getTime());

        // includeBounds = false     at Mid - Range 1 Minute    (dall'inizio Day/2 + HalfDay/2 * Hour/2 => 18:30:00)
        System.out.println(String.format("Mid, range 1 Minute @%s > %s (includeBounds=false)",new Timestamp(midMinuteStartTime),new Timestamp(midMinuteEndTime)));
        getted = history.get(midMinuteStartTime,midMinuteEndTime,true,false);
        System.out.println(printHistoryMap(getted));
        assert SampleRate.Minute .getStart(midMinuteStartTime) == SampleRate.Minute .getStart(getFirstInSamples(getted.get(SampleRate.Minute )).getTime());
        assert null == getFirstInSamples(getted.get(SampleRate.Hour   ));
        assert null == getFirstInSamples(getted.get(SampleRate.HalfDay));
        assert null == getFirstInSamples(getted.get(SampleRate.Day    ));

        // includeBounds = false     at Mid - Range 1 Hour    (dall'inizio Day/2 + HalfDay/2 * Hour/2 => 18:30:00)
        System.out.println(String.format("Mid, range 1 Hour @%s > %s (includeBounds=false)",new Timestamp(midHourStartTime),new Timestamp(midHourEndTime)));
        getted = history.get(midHourStartTime,midHourEndTime,true,false);
        System.out.println(printHistoryMap(getted));
        assert SampleRate.Minute .getStart(midHourStartTime) == SampleRate.Minute .getStart(getFirstInSamples(getted.get(SampleRate.Minute )).getTime());
        assert SampleRate.Hour   .getStart(midHourStartTime) == SampleRate.Hour   .getStart(getFirstInSamples(getted.get(SampleRate.Hour   )).getTime());
        assert null == getFirstInSamples(getted.get(SampleRate.HalfDay));
        assert null == getFirstInSamples(getted.get(SampleRate.Day    ));
        assert getted.get(SampleRate.Minute).size()==60;


        // at End (all)
        System.out.println(String.format("End, range 1 Minute @%s > %s",new Timestamp(endMinuteStartTime),new Timestamp(endMinuteEndTime)));
        getted = history.get(endMinuteStartTime,endMinuteEndTime,true,true);
        System.out.println(printHistoryMap(getted));
        assert SampleRate.Minute .getStart(endMinuteStartTime) == SampleRate.Minute .getStart(getFirstInSamples(getted.get(SampleRate.Minute )).getTime());
        assert SampleRate.Hour   .getStart(endMinuteStartTime) == SampleRate.Hour   .getStart(getFirstInSamples(getted.get(SampleRate.Hour   )).getTime());
        assert SampleRate.HalfDay.getStart(endMinuteStartTime) == SampleRate.HalfDay.getStart(getFirstInSamples(getted.get(SampleRate.HalfDay)).getTime());
        assert SampleRate.Day    .getStart(endMinuteStartTime) == SampleRate.Day    .getStart(getFirstInSamples(getted.get(SampleRate.Day    )).getTime());

        System.out.println(String.format("End, range 1 Minute @%s > %s (includeBounds=false)",new Timestamp(endMinuteStartTime),new Timestamp(endMinuteEndTime)));
        getted = history.get(endMinuteStartTime,endMinuteEndTime,true,false);
        System.out.println(printHistoryMap(getted));
        assert SampleRate.Minute .getStart(endMinuteStartTime) == SampleRate.Minute .getStart(getFirstInSamples(getted.get(SampleRate.Minute )).getTime());
        assert null == getFirstInSamples(getted.get(SampleRate.Hour   ));
        assert null == getFirstInSamples(getted.get(SampleRate.HalfDay));
        assert null == getFirstInSamples(getted.get(SampleRate.Day    ));


        // at After - Range 1 Minute    (tutti all'inizio)
        System.out.println(String.format("After, range 1 Minute @ %s > %s",new Timestamp(afterMinuteStartTime),new Timestamp(afterMinuteEndTime)));
        getted = history.get(afterMinuteStartTime,afterMinuteEndTime,true,true);
        System.out.println(printHistoryMap(getted));
        assert SampleRate.Minute .getStart(afterMinuteStartTime) == SampleRate.Minute .getStart(getFirstInSamples(getted.get(SampleRate.Minute )).getTime());
        assert null == getFirstInSamples(getted.get(SampleRate.Hour   ));
        assert null == getFirstInSamples(getted.get(SampleRate.HalfDay));
        assert null == getFirstInSamples(getted.get(SampleRate.Day    ));
        assert getted.size()==4;

        System.out.println(String.format("After, range 1 Minute @ %s > %s (includeBounds=false)",new Timestamp(afterMinuteStartTime),new Timestamp(afterMinuteEndTime)));
        getted = history.get(afterMinuteStartTime,afterMinuteEndTime,true,false);
        System.out.println(printHistoryMap(getted));
        assert SampleRate.Minute .getStart(afterMinuteStartTime) == SampleRate.Minute .getStart(getFirstInSamples(getted.get(SampleRate.Minute )).getTime());
        assert null == getFirstInSamples(getted.get(SampleRate.Hour   ));
        assert null == getFirstInSamples(getted.get(SampleRate.HalfDay));
        assert null == getFirstInSamples(getted.get(SampleRate.Day    ));
        assert getted.size()==4;

        System.out.println(String.format("After, range 1 Hour @ %s > %s (includeBounds=false)",new Timestamp(startHourStartTime),new Timestamp(startHourEndTime)));
        getted = history.get(startHourStartTime,startHourEndTime,true,false);
        System.out.println(printHistoryMap(getted));
        assert SampleRate.Minute .getStart(startHourStartTime) == SampleRate.Minute .getStart(getFirstInSamples(getted.get(SampleRate.Minute )).getTime());
        assert SampleRate.Hour   .getStart(startHourStartTime) == SampleRate.Hour   .getStart(getFirstInSamples(getted.get(SampleRate.Hour   )).getTime());
        assert null == getFirstInSamples(getted.get(SampleRate.HalfDay));
        assert null == getFirstInSamples(getted.get(SampleRate.Day    ));
        assert getted.size()==4;
        assert getted.get(SampleRate.Minute).size()==60;

        System.out.println(String.format("After, range 1 Minute @ %s > %s (includeBounds=false, includeNull=false)",new Timestamp(afterMinuteStartTime),new Timestamp(afterMinuteEndTime)));
        getted = history.get(afterMinuteStartTime,afterMinuteEndTime,false,false);
        System.out.println(printHistoryMap(getted));
        assert SampleRate.Minute .getStart(afterMinuteStartTime) == SampleRate.Minute .getStart(getFirstInSamples(getted.get(SampleRate.Minute )).getTime());
        assert null == getted.get(SampleRate.Hour   );
        assert null == getted.get(SampleRate.HalfDay);
        assert null == getted.get(SampleRate.Day    );
        assert getted.size()==1;

        // todo nuovo test con un'altro intervallo dove non tutti i SampleRate siano presenti
        System.out.println(String.format("After, range 1 Minute @ %s > %s (includeBounds=false, includeNull=true)",new Timestamp(afterMinuteStartTime),new Timestamp(afterMinuteEndTime)));
        getted = history.get(afterMinuteStartTime,afterMinuteEndTime,false,true);
        System.out.println(printHistoryMap(getted));
        System.out.println("Not passed, it miss the SampleRate Maps when they are empty");
        System.out.println();
//        assert SampleRate.Minute .getStart(afterMinuteStartTime) == SampleRate.Minute .getStart(getFirstInSamples(getted.get(SampleRate.Minute )).getTime());
//        assert null == getted.get(SampleRate.Hour   ).entrySet().iterator().next().getValue();         //null == getFirstInSamples(getted.get(SampleRate.Hour   ));
//        assert null == getted.get(SampleRate.HalfDay).entrySet().iterator().next().getValue();         //null == getFirstInSamples(getted.get(SampleRate.HalfDay));
//        assert null == getted.get(SampleRate.Day    ).entrySet().iterator().next().getValue();         //null == getFirstInSamples(getted.get(SampleRate.Day    ));


        // same time
        System.out.println(String.format("Start, range 0 - same time @ %s",new Timestamp(sameTime)));
        getted = history.get(sameTime,sameTime,true,true);
        System.out.println(printHistoryMap(getted));
        assert SampleRate.Minute .getStart(sameTime) == SampleRate.Minute .getStart(getFirstInSamples(getted.get(SampleRate.Minute )).getTime());
        assert SampleRate.Hour   .getStart(sameTime) == SampleRate.Hour   .getStart(getFirstInSamples(getted.get(SampleRate.Hour   )).getTime());
        assert SampleRate.HalfDay.getStart(sameTime) == SampleRate.HalfDay.getStart(getFirstInSamples(getted.get(SampleRate.HalfDay)).getTime());
        assert SampleRate.Day    .getStart(sameTime) == SampleRate.Day    .getStart(getFirstInSamples(getted.get(SampleRate.Day    )).getTime());

        System.out.println(String.format("Start, range 0 - same time @ %s  (includeBounds=false)",new Timestamp(sameTime)));
        getted = history.get(sameTime,sameTime,true,false);
        System.out.println(printHistoryMap(getted));
        assert null == getFirstInSamples(getted.get(SampleRate.Minute ));
        assert null == getFirstInSamples(getted.get(SampleRate.Hour   ));
        assert null == getFirstInSamples(getted.get(SampleRate.HalfDay));
        assert null == getFirstInSamples(getted.get(SampleRate.Day    ));


        System.out.println(String.format("Start, range 0 - same time @ %s  (includeBounds=false,includeNull=false)",new Timestamp(sameTime)));
        getted = history.get(sameTime,sameTime,false,false);
        System.out.println(printHistoryMap(getted));
        assert null == getted.get(SampleRate.Minute   );
        assert null == getted.get(SampleRate.Hour   );
        assert null == getted.get(SampleRate.HalfDay);
        assert null == getted.get(SampleRate.Day    );

        System.out.println(String.format("Start, range 0 - same time @ %s  (includeBounds=true,includeNull=false)",new Timestamp(sameTime)));
        getted = history.get(sameTime,sameTime,false,true);
        System.out.println(printHistoryMap(getted));
        assert SampleRate.Minute .getStart(sameTime) == SampleRate.Minute .getStart(getFirstInSamples(getted.get(SampleRate.Minute )).getTime());
        assert SampleRate.Hour   .getStart(sameTime) == SampleRate.Hour   .getStart(getFirstInSamples(getted.get(SampleRate.Hour   )).getTime());
        assert SampleRate.HalfDay.getStart(sameTime) == SampleRate.HalfDay.getStart(getFirstInSamples(getted.get(SampleRate.HalfDay)).getTime());
        assert SampleRate.Day    .getStart(sameTime) == SampleRate.Day    .getStart(getFirstInSamples(getted.get(SampleRate.Day    )).getTime());
    }

    // Tested functions
    //public T getSingle(SampleRate sampleRate, long at)
    //public Map<Long,T> getBySampleRate(SampleRate sampleRate, boolean includeNull)
    //public Map<Long,T> getBySampleRate(SampleRate sampleRate, long at, boolean includeNull)
    //public Map<Long,T> getBySampleRate(SampleRate sampleRate, long from, long to, boolean includeNull, boolean includeBounds)
    @SuppressWarnings({"UnnecessaryLocalVariable", "RedundantStringFormatCall", "PointlessArithmeticExpression"})
    private static <T extends Timed> void simpleGetBySampleRateFromToTest(History<T> history, long refTime) {
        long startMinuteAtTime = refTime;
        long emptyMinuteAtTime = refTime + SampleRate.Day.getLength() * 5;
        long startMinuteStartTime = refTime;
        long startMinuteEndTime = startMinuteStartTime + SampleRate.Minute.getLength() * 3;
        long startMinuteMinusStartTime = refTime;
        long startMinuteMinusEndTime = startMinuteMinusStartTime + SampleRate.Minute.getLength() * 3 - 1;
        long endMinutePlusStartTime = refTime + SampleRate.Day.getLength() * 4 - SampleRate.Minute.getLength();
        long endMinutePlusEndTime =   refTime + SampleRate.Day.getLength() * 4 + SampleRate.Minute.getLength();
        
        
        Map<Long,T> getted;
        T resObj;
        int nulls;
        int notNulls;

        // public Map<Long,T> getBySampleRate(SampleRate sampleRate, boolean includeNull)
        System.out.println(String.format("Get Minutes"));
        getted = history.getBySampleRate(SampleRate.Minute,true,true);
        System.out.println(printSampleRateMap(SampleRate.Minute,getted,true));
        nulls = test_countNulls(getted);
        notNulls = test_countNotNulls(getted);
        assert 0 == nulls;
        assert 60 * 24 * 4 == notNulls;
        assert 60 * 24 * 4 == getted.size();

        System.out.println(String.format("Get Minutes (includeNulls=false"));
        getted = history.getBySampleRate(SampleRate.Minute,false,true);
        System.out.println(printSampleRateMap(SampleRate.Minute,getted,true));
        nulls = test_countNulls(getted);
        notNulls = test_countNotNulls(getted);
        assert 0 == nulls;
        assert 60 * 24 * 4 == notNulls;
        assert 60 * 24 * 4 == getted.size();

        System.out.println(String.format("Get Minutes (allSampleRate=false)"));
        getted = history.getBySampleRate(SampleRate.Minute,true,false);
        System.out.println(printSampleRateMap(SampleRate.Minute,getted,true));
        nulls = test_countNulls(getted);
        notNulls = test_countNotNulls(getted);
        assert 0 == nulls;
        assert 60 * 24 * 4 == notNulls;
        assert 60 * 24 * 4 == getted.size();

        System.out.println(String.format("Get Minutes (includeNulls=false,allSampleRate=false)"));
        getted = history.getBySampleRate(SampleRate.Minute,false,false);
        System.out.println(printSampleRateMap(SampleRate.Minute,getted,true));
        nulls = test_countNulls(getted);
        notNulls = test_countNotNulls(getted);
        assert 0 == nulls;
        assert 60 * 24 * 4 == notNulls;
        assert 60 * 24 * 4 == getted.size();

        System.out.println(String.format("Get Hours"));
        System.out.println(String.format("Hours - true - true"));
        getted = history.getBySampleRate(SampleRate.Hour,true,true);
        System.out.println(printSampleRateMap(SampleRate.Hour,getted,true));
        nulls = test_countNulls(getted);
        notNulls = test_countNotNulls(getted);
        assert 24 * 1 == nulls;
        assert 24 * 3 == notNulls;
        assert 24 * 4 == getted.size();

        System.out.println(String.format("Get Hours (includeNulls=false)"));
        getted = history.getBySampleRate(SampleRate.Hour,false,true);
        System.out.println(printSampleRateMap(SampleRate.Hour,getted,true));
        nulls = test_countNulls(getted);
        notNulls = test_countNotNulls(getted);
        assert 0 == nulls;
        assert 24 * 3 == notNulls;
        assert 24 * 3 == getted.size();

        System.out.println(String.format("Get Hours (allSampleRate=false)"));
        getted = history.getBySampleRate(SampleRate.Hour,true,false);
        System.out.println(printSampleRateMap(SampleRate.Hour,getted,true));
        nulls = test_countNulls(getted);
        notNulls = test_countNotNulls(getted);
        assert 0 == nulls;
        assert 24 * 3 == notNulls;
        assert 24 * 3 == getted.size();

        System.out.println(String.format("Get Hours (includeNulls=false,allSampleRate=false)"));
        getted = history.getBySampleRate(SampleRate.Hour,false,false);
        System.out.println(printSampleRateMap(SampleRate.Hour,getted,true));
        nulls = test_countNulls(getted);
        notNulls = test_countNotNulls(getted);
        assert 0 == nulls;
        assert 24 * 3 == notNulls;
        assert 24 * 3 == getted.size();

        // Add penultimate hour
        Test_History_Timed hour = new Test_History_Timed(history.getLast().getTime() - SampleRate.Hour.getLength());
        //noinspection unchecked
        history.add(SampleRate.Hour,(T)hour);


        System.out.println(String.format("Get Hours+"));
        getted = history.getBySampleRate(SampleRate.Hour,true,true);
        System.out.println(printSampleRateMap(SampleRate.Hour,getted,true));
        nulls = test_countNulls(getted);
        notNulls = test_countNotNulls(getted);
        assert 24 * 1 - 1 == nulls;
        assert 24 * 3 + 1 == notNulls;
        assert 24 * 4 == getted.size();
        // elements 95

        System.out.println(String.format("Get Hours+ (includeNulls=false)"));
        getted = history.getBySampleRate(SampleRate.Hour,false,true);
        System.out.println(printSampleRateMap(SampleRate.Hour,getted,true));
        nulls = test_countNulls(getted);
        notNulls = test_countNotNulls(getted);
        assert 0 == nulls;
        assert 24 * 3 + 1 == notNulls;
        assert 24 * 3 + 1 == getted.size();

        System.out.println(String.format("Get Hours+ (allSampleRate=false)"));
        getted = history.getBySampleRate(SampleRate.Hour,true,false);
        System.out.println(printSampleRateMap(SampleRate.Hour,getted,true));
        nulls = test_countNulls(getted);
        notNulls = test_countNotNulls(getted);
        assert 24 * 1 - 1 - 1 == nulls;
        assert 24 * 3 + 1 == notNulls;
        assert 24 * 4 - 1 == getted.size();
        // elements 94

        System.out.println(String.format("Get Hours+ (includeNulls=false,allSampleRate=false)"));
        getted = history.getBySampleRate(SampleRate.Hour,false,false);
        System.out.println(printSampleRateMap(SampleRate.Hour,getted,true));
        nulls = test_countNulls(getted);
        notNulls = test_countNotNulls(getted);
        assert 0 == nulls;
        assert 24 * 3 + 1 == notNulls;
        assert 24 * 3 + 1 == getted.size();

        // public Map<Long,T> getBySampleRate(SampleRate sampleRate, long at, boolean includeNull)
        // Convenient function        public T getSingle(SampleRate sampleRate, long at)
        System.out.println(String.format("Get Minutes @ %s",new Timestamp(startMinuteAtTime)));
        getted = history.getBySampleRate(SampleRate.Minute,startMinuteAtTime,true);
        System.out.println(printSampleRateMap(SampleRate.Minute,getted,true));
        resObj = getted.values().iterator().next();
        assert SampleRate.Minute .getStart(startMinuteAtTime) == SampleRate.Minute .getStart(resObj.getTime());
        assert getted.size()==1;

        System.out.println(String.format("Get Minutes @ %s  (includeNull=false)",new Timestamp(startMinuteAtTime)));
        getted = history.getBySampleRate(SampleRate.Minute,startMinuteAtTime,false);
        System.out.println(printSampleRateMap(SampleRate.Minute,getted,true));
        assert SampleRate.Minute .getStart(startMinuteAtTime) == SampleRate.Minute .getStart(resObj.getTime());
        assert getted.size()==1;

        System.out.println(String.format("Get Minutes (empty) @ %s",new Timestamp(emptyMinuteAtTime)));
        getted = history.getBySampleRate(SampleRate.Minute,emptyMinuteAtTime,true);
        System.out.println(printSampleRateMap(SampleRate.Minute,getted,true));
        resObj = getted.values().iterator().next();
        assert null == resObj;
        assert getted.size()==1;

        System.out.println(String.format("Get Minutes (empty) @ %s  (includeNull=false)",new Timestamp(emptyMinuteAtTime)));
        getted = history.getBySampleRate(SampleRate.Minute,emptyMinuteAtTime,false);
        System.out.println(printSampleRateMap(SampleRate.Minute,getted,true));
        assert getted.size()==0;

        System.out.println(String.format("Get Minutes (single) @ %s",new Timestamp(startMinuteAtTime)));
        resObj = history.get(SampleRate.Minute,startMinuteAtTime);
        System.out.println(resObj + "\n");
        assert SampleRate.Minute .getStart(startMinuteAtTime) == SampleRate.Minute .getStart(resObj.getTime());

        System.out.println(String.format("Get Minutes (single + empty) @ %s",new Timestamp(emptyMinuteAtTime)));
        resObj = history.get(SampleRate.Minute,emptyMinuteAtTime);
        System.out.println(resObj + "\n");
        assert null == resObj;

        // public Map<Long,T> getBySampleRate(SampleRate sampleRate, long from, long to, boolean includeNull, boolean includeBounds)
        System.out.println(String.format("Get Minutes @ %s > %s",new Timestamp(startMinuteStartTime),new Timestamp(startMinuteEndTime)));
        getted = history.getBySampleRate(SampleRate.Minute,startMinuteStartTime,startMinuteEndTime,true,true);
        System.out.println(printSampleRateMap(SampleRate.Minute,getted,false));
        nulls = test_countNulls(getted);
        notNulls = test_countNotNulls(getted);
        assert nulls == 0;
        assert notNulls == 3;
        assert getted.size()==3;

        System.out.println(String.format("Get Minutes @ %s > %s (includeBounds=false)",new Timestamp(startMinuteStartTime),new Timestamp(startMinuteEndTime)));
        getted = history.getBySampleRate(SampleRate.Minute,startMinuteStartTime,startMinuteEndTime,true,false);
        System.out.println(printSampleRateMap(SampleRate.Minute,getted,false));
        nulls = test_countNulls(getted);
        notNulls = test_countNotNulls(getted);
        assert nulls == 0;
        assert notNulls == 3;
        assert getted.size()==3;

        
        System.out.println(String.format("Start: %s;\tEnd: %s;", new Timestamp(startMinuteMinusStartTime), new Timestamp(startMinuteMinusEndTime)));
        getted = history.getBySampleRate(SampleRate.Minute,startMinuteMinusStartTime,startMinuteMinusEndTime,true,true);
        System.out.println(printSampleRateMap(SampleRate.Minute,getted,false));
        nulls = test_countNulls(getted);
        notNulls = test_countNotNulls(getted);
        assert nulls == 0;
        assert notNulls == 3;
        assert getted.size()==3;

        System.out.println(String.format("Get Minutes @ %s > %s (includeBounds=false)",new Timestamp(startMinuteMinusStartTime),new Timestamp(startMinuteMinusEndTime)));
        getted = history.getBySampleRate(SampleRate.Minute,startMinuteMinusStartTime,startMinuteMinusEndTime,true,false);
        System.out.println(printSampleRateMap(SampleRate.Minute,getted,false));
        nulls = test_countNulls(getted);
        notNulls = test_countNotNulls(getted);
        assert nulls == 1;
        assert notNulls == 2;
        assert getted.size()==3;

        System.out.println(String.format("Get Minutes @ %s > %s (includeNull=false)",new Timestamp(startMinuteMinusStartTime),new Timestamp(startMinuteMinusEndTime)));
        getted = history.getBySampleRate(SampleRate.Minute,startMinuteMinusStartTime,startMinuteMinusEndTime,false,true);
        System.out.println(printSampleRateMap(SampleRate.Minute,getted,false));
        nulls = test_countNulls(getted);
        notNulls = test_countNotNulls(getted);
        assert nulls == 0;
        assert notNulls == 3;
        assert getted.size()==3;

        System.out.println(String.format("Get Minutes @ %s > %s (includeBounds=false,includeNull=false)",new Timestamp(startMinuteMinusStartTime),new Timestamp(startMinuteMinusEndTime)));
        getted = history.getBySampleRate(SampleRate.Minute,startMinuteMinusStartTime,startMinuteMinusEndTime,false,false);
        System.out.println(printSampleRateMap(SampleRate.Minute,getted,false));
        nulls = test_countNulls(getted);
        notNulls = test_countNotNulls(getted);
        assert nulls == 0;
        assert notNulls == 2;
        assert getted.size()==2;

        System.out.println(String.format("Get Minutes @ %s > %s",new Timestamp(endMinutePlusStartTime),new Timestamp(endMinutePlusEndTime)));
        getted = history.getBySampleRate(SampleRate.Minute,endMinutePlusStartTime,endMinutePlusEndTime,true,true);
        System.out.println(printSampleRateMap(SampleRate.Minute,getted,false));
        nulls = test_countNulls(getted);
        notNulls = test_countNotNulls(getted);
        assert nulls == 1;
        assert notNulls == 1;
        assert getted.size()==2;

        System.out.println(String.format("Get Minutes @ %s > %s (includeNull=false)",new Timestamp(endMinutePlusStartTime),new Timestamp(endMinutePlusEndTime)));
        getted = history.getBySampleRate(SampleRate.Minute,endMinutePlusStartTime,endMinutePlusEndTime,false,true);
        System.out.println(printSampleRateMap(SampleRate.Minute,getted,false));
        nulls = test_countNulls(getted);
        notNulls = test_countNotNulls(getted);
        assert nulls == 0;
        assert notNulls == 1;
        assert getted.size()==1;
    }

    // Tested functions
    //public Map<SampleRate,Map<Long,T>> getCurrent(boolean includeNull)
    //public Map<SampleRate,Map<Long,T>> getPrev(boolean includeNull)
    //public Map<SampleRate,Map<Long,T>> getNext(boolean includeNull)
    private static <T extends Timed> void simpleGetCurrentPrevNextTest(History<T> history, long refTime) {
        Map<SampleRate,Map<Long,T>> getted;

        getted = history.getCurrent(true);
        System.out.println(printHistoryMap(getted));

        getted = history.getPrev(true);
        System.out.println(printHistoryMap(getted));

        getted = history.getNext(true);
        System.out.println(printHistoryMap(getted));
    }

    // Test utils

    public static class Test_History_Timed implements Timed {

        private final long time;

        public Test_History_Timed(long time) {
            this.time = time;
        }

        @Override
        public long getTime() {
            return time;
        }

        public String toString() { return String.format("%s@%s",this.getClass().getSimpleName(), SampleRate.toFormattedDate(getTime())); }
    }

    private static void test_populate3Days(History<Test_History_Timed> history, long refTime) {
        List<Test_History_Timed> minuteTestSet = new ArrayList<>();
        List<Test_History_Timed> hourTestSet = new ArrayList<>();
        List<Test_History_Timed> halfdayTestSet = new ArrayList<>();
        List<Test_History_Timed> dayTestSet = new ArrayList<>();
        for (int i = 0; i<= ( 60*24*3 ) - 1 + ( 60*24 ); i++)
            minuteTestSet.add(new Test_History_Timed(refTime + ( SampleRate.Minute.getLength() * i ) + 5000));
        for (int i = 0; i<= ( 24*3 ) - 1; i++)
            hourTestSet.add(new Test_History_Timed(refTime + ( SampleRate.Hour.getLength() * i ) + 5000));
        for (int i = 0; i<= ( 2*3 ) - 1; i++)
            halfdayTestSet.add(new Test_History_Timed(refTime + ( SampleRate.HalfDay.getLength() * i ) + 5000));
        for (int i = 0; i<= ( 3 ) - 1; i++)
            dayTestSet.add(new Test_History_Timed(refTime + ( SampleRate.Day.getLength() * i ) + 5000));

        history.add(SampleRate.Minute,minuteTestSet);
        history.add(SampleRate.Hour,hourTestSet);
        history.add(SampleRate.HalfDay,halfdayTestSet);
        history.add(SampleRate.Day,dayTestSet);
    }

    private static <T extends Timed> int test_countNulls(Map<?, T> getted) {
        Collection<T> vals = getted.values();
        int count = 0;
        for (T v : vals)
            count += v==null ? 1 : 0;
        return count;
    }

    private static <T extends Timed> int test_countNotNulls(Map<?, T> getted) {
        if (getted==null) return 0;
        Collection<T> vals = getted.values();
        int count = 0;
        for (T v : vals)
            count += v!=null ? 1 : 0;
        return count;
    }

}
