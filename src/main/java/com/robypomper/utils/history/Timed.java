package com.robypomper.utils.history;

/**
 * The getTime() method is used by History to know where store the Timed
 * implementing Object.
 *
 * @since 0.1
 */
public interface Timed {

    /** @return object associated time in millisecond, see Timestamp.getTime() */
    long getTime();

}
