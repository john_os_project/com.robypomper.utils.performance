package com.robypomper.utils;

import java.awt.*;
import java.util.*;
import java.util.List;

@SuppressWarnings("WeakerAccess")
public class ColorPatterns {

    @SuppressWarnings("FieldCanBeLocal")
    private static boolean HSV = false;
    @SuppressWarnings("FieldCanBeLocal")
    private static boolean RGB = true;


    // ---------------
    // Palettes getter
    // ---------------

    public static String[] getColors(int length) {
        for (List<String> colors : palettes.values())
            if (colors.size()>length)
                return colors.toArray(new String[0]);
        return new String[0];
    }


    // -------------------
    // Palettes generators
    // -------------------

    private static Map<Integer, List<String>> palettes = generatePalettes();

    private static Map<Integer, List<String>> generatePalettes() {
        Map<Integer, List<String>> palettes = new TreeMap<>();
        for (int i=1; i<=10; i++) {
            palettes.put(i*10,generateSinglePalette(i*10));
        }
        return palettes;
    }

    @SuppressWarnings({"UnusedAssignment", "ConstantConditions"})
    private static List<String> generateSinglePalette(int colorCount) {
        RGB = true;
        int hCount = 5;
        int sCount = 1;
        int vCount = 1;

        if (RGB) {
            hCount = 1;
            sCount = 1;
            vCount = 1;
        }
        boolean hIsNext = false;
        boolean sIsNext = true;
        boolean vIsNext = false;

        while (colorCount > hCount*sCount*vCount) {

            if (vIsNext) {
                vCount++;
                vIsNext = false;
                hIsNext = true;
            }

            if (sIsNext) {
                sCount++;
                sIsNext = false;
                vIsNext = true;
            }

            if (hIsNext) {
                hCount++;
                hIsNext = false;
                sIsNext = true;
            }
        }
        List<String> colors = new ArrayList<>();

        for (int h=0; h<hCount; h++)
            for (int s=0; s<sCount; s++)
                for (int v=0; v<vCount; v++) {
                    int hVal = h==0 ? 0 : 360/h;
                    int sVal = s==0 ? 50 : ((100/s) + 50 ) % 100;
                    int vVal = v==0 ? 50 : ((100/v) + 50 ) % 100;

                    Color c = null;
                    if (HSV) {
                        hVal = h == 0 ? 0 : 360 / (h + 1);
                        sVal = 25 + (s == 0 ? 0 : 50 / s);
                        vVal = 25 + (v == 0 ? 0 : 50 / v);
                        c = toRGB(hVal, sVal, vVal, 1.0f);
                    }

                    if (RGB) {
                        int rVal = h == 0 ? 0 : 255 / (h);
                        int gVal = s == 0 ? 0 : 255 / (s);
                        int bVal = v == 0 ? 0 : 255 / (v);
                        c = new Color(rVal, gVal, bVal);
                    }

                    if (c!=null) {
                        String cStr = "#" + Integer.toHexString(c.getRGB()).substring(2);
                        colors.add(cStr);
                    }
                }

        return colors;
    }


    // ----------
    // Converters
    // ----------

    public static Color toRGB(float h, float s, float l, float alpha) {
        if (s <0.0f || s > 100.0f)
        {
            String message = "Color parameter outside of expected range - Saturation";
            throw new IllegalArgumentException( message );
        }

        if (l <0.0f || l > 100.0f)
        {
            String message = "Color parameter outside of expected range - Luminance";
            throw new IllegalArgumentException( message );
        }

        if (alpha <0.0f || alpha > 1.0f)
        {
            String message = "Color parameter outside of expected range - Alpha";
            throw new IllegalArgumentException( message );
        }

        //  Formula needs all values between 0 - 1.

        h = h % 360.0f;
        h /= 360f;
        s /= 100f;
        l /= 100f;

        float q;
        if (l < 0.5)
            q = l * (1 + s);
        else
            q = (l + s) - (s * l);

        float p = 2 * l - q;

        float r = Math.max(0, HueToRGB(p, q, h + (1.0f / 3.0f)));
        float g = Math.max(0, HueToRGB(p, q, h));
        float b = Math.max(0, HueToRGB(p, q, h - (1.0f / 3.0f)));

        r = Math.min(r, 1.0f);
        g = Math.min(g, 1.0f);
        b = Math.min(b, 1.0f);

        return new Color(r, g, b, alpha);
    }

    private static float HueToRGB(float p, float q, float h) {
        if (h < 0) h += 1;

        if (h > 1 ) h -= 1;

        if (6 * h < 1)
        {
            return p + ((q - p) * 6 * h);
        }

        if (2 * h < 1 )
        {
            return  q;
        }

        if (3 * h < 2)
        {
            return p + ( (q - p) * 6 * ((2.0f / 3.0f) - h) );
        }

        return p;
    }


    // -------------------------------------------------------------------------
    // -------------------------------------------------------------------------

    // -------
    // Testing
    // -------

    public static void main(String[] args) {

        for (List<String> colors : palettes.values())
            new ColorDisplay(colors.toArray(new String[0])).showWindow();


        for (Map.Entry<Integer,List<String>> entry : palettes.entrySet()) {
            System.out.println(String.format("Palette with %d colors (%d)", entry.getKey(), entry.getValue().size()));
            for (String color : entry.getValue()) {
                System.out.print(String.format("\"%s\", ", color));
            }
            System.out.println();
        }
    }

}
