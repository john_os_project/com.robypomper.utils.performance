package com.robypomper.utils.performance;

import com.beust.jcommander.IStringConverter;
import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import com.robypomper.utils.IOResources;
import com.robypomper.utils.history.History;
import com.robypomper.utils.history.SampleRate;
import com.robypomper.utils.performance.analyzer.ProceduresCollection;
import com.robypomper.utils.performance.viewer.ProceduresGraph;
import com.robypomper.utils.performance.viewer.ProceduresLayout;
import org.graphstream.ui.geom.Point2;
import org.graphstream.ui.geom.Point3;
import org.graphstream.ui.layout.Layout;
import org.graphstream.ui.swingViewer.ViewPanel;
import org.graphstream.ui.view.Camera;
import org.graphstream.ui.view.View;

import javax.swing.*;
import java.awt.*;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.WatchEvent;
import java.util.List;
import java.util.Map;

/**
 *
 * @since 0.2
 */
@SuppressWarnings({"WeakerAccess", "unused"})
public class PerformanceViewer {

    private static final int DEF_WIDTH = 300;
    private static final int DEF_HEIGHT = 400;


    // ------
    // Fields
    // ------

    /** Procedures representation in GraphStream object. */
    private ProceduresGraph graph = null;
    private JFrame graphFrame = null;
    private History<ProceduresCollection> procsHistory = null;
    private Timer autoUpdatesTimer = null;
    private Object cssLoadResource = null;


    // ----------
    // Properties
    // ----------

    private Class<? extends Layout> layoutClass = ProceduresLayout.class;
    public Class getLayoutClass() { return layoutClass; }
    public void setLayoutClass(Class<? extends Layout> layout) {
        if (graph!=null) throw new IllegalStateException("Layout can be changed only before call setup() or after close() methods.");
        layoutClass = layout;
    }

    private String frameTitle = "PerformanceViewer";
    public String getFrameTitle() { return frameTitle; }
    public void setFrameTitle(String frameTitle) {
        this.frameTitle = frameTitle;
        if (graphFrame!=null)
            graphFrame.setTitle(String.format("%s - %s@%s", frameTitle, selectedSampleRate, SampleRate.toFormattedDate(selectedSampleRate.getStart(selectedTime))));
    }
    private void updateFrameTitle() {
        setFrameTitle(getFrameTitle());
    }


    private Object procsLoadResource = ".perf/procedures";
    public Object getProcsLoadResource() { return procsLoadResource; }
    public void setProcsLoadResource(Object procsLoadResource) {
        if (graph!=null) throw new IllegalStateException("Layout can be changed only before call setup() or after close() methods.");
        this.procsLoadResource = procsLoadResource;
    }

    private long selectedTime = Performance.getMillisSince();
    public long getSelectedTime() { return selectedTime; }
    public void setSelectedTime(long selectedTime) {
        this.selectedTime = selectedTime;
        // ToDo update rendered procedure and autoUpdate
    }

    private SampleRate selectedSampleRate = SampleRate.Minute;
    public SampleRate getSelectedSampleRate() { return selectedSampleRate; }
    public void setSelectedSampleRate(SampleRate selectedSampleRate) {
        this.selectedSampleRate = selectedSampleRate;
        // ToDo update rendered procedure and autoUpdate
    }

    private boolean fullscreen = false;
    public boolean getFullscreen() { return fullscreen; }
    public void setFullscreen(boolean fullscreen) {
        if (graphFrame!=null) throw new IllegalStateException("Fullscreen mode can changed only before call show() or after hide() methods.");
        this.fullscreen = fullscreen;
    }

    private int updateTime = 5;
    public int getUpdateTime() { return updateTime; }
    public void setUpdateTime(int updateTime) {
        if (graph!=null) throw new IllegalStateException("UpdateTime can changed only before call setup() or after close() methods.");
        this.updateTime = updateTime;
    }


    /** If true, this class prints warning messages. */
    private boolean isWarnEnabled = true;
    @SuppressWarnings("WeakerAccess")
    public boolean getEnableWarning() { return isWarnEnabled; }
    @SuppressWarnings("WeakerAccess")
    protected boolean isWarn() { return getEnableWarning(); }
    @SuppressWarnings("WeakerAccess")
    public void setEnableWarning(boolean warnEnabled) {
        this.isWarnEnabled  = warnEnabled;
        if (graph!=null)
            graph.setEnableLog(warnEnabled);
    }
    /** If true, this class prints logs messages. */
    private boolean isLogEnabled = false;
    @SuppressWarnings("WeakerAccess")
    public boolean getEnableLog() { return isLogEnabled; }
    @SuppressWarnings("WeakerAccess")
    protected boolean isLog() { return getEnableLog(); }
    @SuppressWarnings("WeakerAccess")
    public void setEnableLog(boolean logEnabled) {
        this.isLogEnabled  = logEnabled;
        if (graph!=null)
            graph.setEnableLog(logEnabled);
    }
    /** If true, this class prints debug messages. */
    private boolean isDebugEnabled = false;
    @SuppressWarnings("WeakerAccess")
    public boolean getEnableDebug() { return isDebugEnabled; }
    @SuppressWarnings({"WeakerAccess", "RedundantSuppression", "unused"})
    protected boolean isDeb() { return getEnableDebug(); }
    @SuppressWarnings("WeakerAccess")
    public void setEnableDebug(boolean debEnabled) {
        this.isDebugEnabled  = debEnabled;
        if (graph!=null)
            graph.setEnableDebug(debEnabled);
    }

    private boolean updateDataStoreSingleton = false;
    private IOResources.ResourceObserver resourceMonitorObserver = (resource, event) -> {
        if (resource instanceof String)
            if (event instanceof WatchEvent)
                updateDataSource((String)resource, (WatchEvent)event);
            else
                updateDataSource((String)resource, null);
        else
            if (isWarn()) System.out.println(String.format("%-25s: PerformanceViewer received invalid resourceMonitor event (with params: %s %s, %s %s).", "PerformanceViewer", resource.getClass().getSimpleName(), resource, event.getClass().getSimpleName(), event));

    };


    // ----------
    // Graph Mngm
    // ----------

    public void setup() {
        if (graph!=null) throw new IllegalStateException("Can't setup PerformanceViewer already setup.");

        graph = new ProceduresGraph(layoutClass, Performance.getViewerCssLoadPaths());
        graph.setEnableWarning(getEnableWarning());
        graph.setEnableLog(getEnableLog());
        graph.setEnableDebug(getEnableDebug());

        try {
            IOResources.resourceMonitor(procsLoadResource, updateTime, resourceMonitorObserver);
        } catch (IOException e) {
            System.out.println(String.format("%-25s: Can't monitor Procedure's directory, error on '%s' dir: %s.", "PerformanceViewer", procsLoadResource, e));
        }

        updateDataSource((String)procsLoadResource,null);

        String resourceFullPath = Paths.get((String)procsLoadResource).toAbsolutePath().toString();
        if (isLog()) System.out.println(String.format("%-25s: PerformanceViewer setup successfully on dir '%s'.", "PerformanceViewer", resourceFullPath));
        //if (isLog()) System.out.println(String.format("%-25s: PerformanceViewer loaded successfully on dir '%s', with updateTime of '%d' seconds and '%s' layoutClass.", "PerformanceViewer", procsLoadResource, updateTime, layoutClass.getSimpleName()));
    }

    public void show() {
        if (graphFrame!=null) throw new IllegalStateException("Can't show PerformanceViewer, it's already visible.");

        graphFrame = new JFrame(getFrameTitle());
        graphFrame.setSize(new Dimension(DEF_WIDTH,DEF_HEIGHT));
        updateFrameTitle();
        if (fullscreen) graphFrame.setExtendedState(JFrame.MAXIMIZED_BOTH );
        graphFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        ViewPanel graphView = graph.getView();
        enableMouseWheelZoom(graphView);
        graphFrame.add(graphView);

        graphFrame.setVisible(true);

        if (isLog()) System.out.println(String.format("%-25s: PerformanceViewer showed successfully.", "PerformanceViewer"));
    }

    public void close() {
        if (graphFrame!=null) throw new IllegalStateException("Can't close PerformanceViewer, it's not visible.");

        try {
            IOResources.resourceDemonitor(procsLoadResource);
        } catch (IOException e) {
            if (isWarn()) System.out.println(String.format("%-25s: Can't demonitor Procedure's directory, error on '%s' dir: %s.", "PerformanceViewer", procsLoadResource, e));
        }

        if (graphFrame==null) return;
        graphFrame.setVisible(false);
        graphFrame.dispatchEvent(new WindowEvent(graphFrame, WindowEvent.WINDOW_CLOSING));

        if (isLog()) System.out.println(String.format("%-25s: PerformanceViewer closed successfully.", "PerformanceViewer"));
    }

    public void store(Class<? extends Layout> layoutClass, ProceduresCollection procs) {
        String fileName = String.format("%s@%s.png", procs.ProcsSampleRate.name(), SampleRate.toFilenameDate(procs.StartTime().getTime()));
        ProceduresGraph tmpGraph = new ProceduresGraph(layoutClass, Performance.getViewerCssLoadPaths());
        tmpGraph.storeGraph(new File(fileName).getAbsolutePath());

    }


    // ---------
    // Data Mngm
    // ---------

    private void updateDataSource(String resource, WatchEvent event) {
        if (updateDataStoreSingleton) return;

        updateDataStoreSingleton = true;
        _updateDataSource(resource,event);
        updateDataStoreSingleton = false;
    }

    private void _updateDataSource(String resource, WatchEvent event) {
        // 1st load
        if (procsHistory==null) {
            // Load ProceduresCollections from resource
            procsHistory = loadProcedures(IOResources.resourceToInPaths(resource));
            int countColl = countCollections(procsHistory);
            if (countColl==0) {
                if (isWarn()) System.out.println(String.format("%-25s: No procedures loaded from '%s'.", "PerformanceViewer", resource));
                procsHistory = null;
                return;
            }
            int countProcs = countProcedures(procsHistory);
            if (isLog()) System.out.println(String.format("%-25s: Loaded from '%s' %d procedures on %d collection.", "PerformanceViewer", resource, countProcs, countColl));

            // Set selected procedure collection on graph
            updateFrameTitle();
            if (procsHistory.get(selectedSampleRate,selectedTime)==null) {
                if (isWarn()) System.out.println(String.format("%-25s: Selected procedure '%s@%s' not found in loaded procedures.", "PerformanceViewer", selectedSampleRate, SampleRate.toFormattedDate(selectedSampleRate.getStart(selectedTime))));
                return;
            }
            graph.setProcedures(procsHistory.get(selectedSampleRate, selectedTime));
            if (isLog()) System.out.println(String.format("%-25s: Set selected ProcedureCollection '%s@%s' Time successfully.", "PerformanceViewer", selectedSampleRate, SampleRate.toFormattedDate(selectedSampleRate.getStart(selectedTime))));
            return;
        }

        // Load/Update single ProceduresCollection file
        if (event!=null) {
            // Reload/Load single file
            Path resourceFile = event.context() instanceof Path ? (Path) event.context() : Paths.get((String) event.context());
            ProceduresCollection reloadProcs = reloadProcedure(procsHistory, Paths.get(resource, resourceFile.toString()));
            if (reloadProcs == null) {
                if (isLog()) System.out.println(String.format("%-25s: Error on loading procedures from '%s' file.", "PerformanceViewer", resourceFile.toString()));
                return;
            }
            if (isDeb()) System.out.println(String.format("%-25s: Loaded from '%s' files %d procedures.", "PerformanceViewer", resourceFile.toString(), reloadProcs.Procedures().size()));

            // Check if can update graph
            if (reloadProcs.ProcsSampleRate==selectedSampleRate) {
                boolean switchToReloaded = procedure(reloadProcs) > selected();
                boolean updateToReloaded = procedure(reloadProcs) == selected();

                if (switchToReloaded) {
                    selectedTime = procedure(reloadProcs);
                    graph.setProcedures(reloadProcs);
                    updateFrameTitle();

                    if (isLog()) System.out.println(String.format("%-25s: Switched selected ProcedureCollection '%s@%s' successfully.", "PerformanceViewer", reloadProcs.ProcsSampleRate.name(), SampleRate.toFormattedDate(reloadProcs.getTime())));
                }

                if (updateToReloaded) {
                    graph.updatesProcedure(reloadProcs);
                    if (isLog()) System.out.println(String.format("%-25s: Updated selected ProcedureCollection '%s@%s' Time successfully.", "PerformanceViewer", selectedSampleRate, SampleRate.toFormattedDate(selectedSampleRate.getStart(selectedTime))));
                }

            }
        }

    }

    private long selected() {
        return selectedSampleRate.getStart(selectedTime);
    }

    private long procedure(ProceduresCollection proc) {
        return proc.ProcsSampleRate.getStart(proc.StartTime().getTime());
    }

    private static int countCollections(History<ProceduresCollection> procHistory) {
        int countColl = 0;
        for (SampleRate sr : SampleRate.values())
            if (procHistory.getBySampleRate(sr, false, true)!=null)
                countColl += procHistory.getBySampleRate(sr, false, true).entrySet().size();
        return countColl;
    }

    private static int countProcedures(History<ProceduresCollection> procHistory) {
        int countProcs = 0;
        for (SampleRate sr : SampleRate.values())
            if (procHistory.getBySampleRate(sr, false, true)!=null)
                for (Map.Entry<Long, ProceduresCollection> p : procHistory.getBySampleRate(sr, false, false).entrySet())
                    countProcs += p.getValue().Procedures().size();
        return countProcs;
    }

    private static History<ProceduresCollection> loadProcedures(List<Path> proceduresCollectionPaths) {
        History<ProceduresCollection> loadedProcedureHistory = new History<>();

        for (Path p : proceduresCollectionPaths) {
            ProceduresCollection loadedProcs = ProceduresCollection.loadJson(p);
            if (loadedProcs!=null) loadedProcedureHistory.add(loadedProcs.ProcsSampleRate,loadedProcs);
        }

        return loadedProcedureHistory;
    }

    private static ProceduresCollection reloadProcedure(History<ProceduresCollection> historyProcedure, Path proceduresCollectionPath) {
        ProceduresCollection loadedProcs = ProceduresCollection.loadJson(proceduresCollectionPath);
        if (loadedProcs!=null) {
            ProceduresCollection preLoadedProcs = historyProcedure.get(loadedProcs.ProcsSampleRate,loadedProcs.getTime());
            // if already present, remove it
            if (preLoadedProcs!=null)
                historyProcedure.remove(loadedProcs.ProcsSampleRate,preLoadedProcs);
            historyProcedure.add(loadedProcs.ProcsSampleRate,loadedProcs);
        }
        return loadedProcs;
    }


    // ---------
    // View Mngm
    // ---------

    private static void enableMouseWheelZoom(View view) {
        //view.getCamera().setViewPercent(1);
        //view.getCamera().setViewCenter(0, 0, 0);
        ((Component) view).addMouseWheelListener(event -> {
            event.consume();
            int i = event.getWheelRotation();
            double factor = Math.pow(1.25, i);
            Camera cam = view.getCamera();
            double zoom = cam.getViewPercent() * factor;
            Point2 pxCenter  = cam.transformGuToPx(cam.getViewCenter().x, cam.getViewCenter().y, 0);
            Point3 guClicked = cam.transformPxToGu(event.getX(), event.getY());
            double newRatioPx2Gu = cam.getMetrics().ratioPx2Gu/factor;
            double x = guClicked.x + (pxCenter.x - event.getX())/newRatioPx2Gu;
            double y = guClicked.y - (pxCenter.y - event.getY())/newRatioPx2Gu;
            cam.setViewCenter(x, y, 0);
            cam.setViewPercent(zoom);
        });
    }


    // ----
    // Main
    // ----

    @SuppressWarnings("RedundantThrows")
    public static void main(String[] args) throws InterruptedException {
        CmdLineArgs argsParser = new CmdLineArgs();
        JCommander.newBuilder()
                .addObject(argsParser)
                .build()
                .parse(args);

        if (argsParser.Log) {
            System.out.println(String.format("%-25s: LOG mainFunction(args: %s)", "PerformanceViewer", String.join(", ",args)));
        }
        if (argsParser.Verbose) {
            System.out.println(String.format("%-25s: VER  workingDir= %s", "PerformanceViewer", System.getProperty("user.dir")));
        }

        PerformanceViewer viewer = new PerformanceViewer();
        viewer.setProcsLoadResource(argsParser.ProcsLoadResource);
        viewer.setSelectedTime(argsParser.SelectedTime);
        viewer.setSelectedSampleRate(argsParser.SelectedSampleRate);
        viewer.setFullscreen(argsParser.Fullscreen);
        viewer.setUpdateTime(argsParser.UpdateTime);
        viewer.setEnableWarning(argsParser.Warn);
        viewer.setEnableLog(argsParser.Log);
        viewer.setEnableDebug(argsParser.Verbose);

        viewer.setup();
        viewer.show();
    }

    static class CmdLineArgs {
        @Parameter(names = { "--procLoadResource", "-p" }, description = "Path of ProceduresCollection's files.")
        private String ProcsLoadResource = "./.perf/procedures";

        @Parameter(names = {"--selectedTime", "-time"}, converter = timeToMillsSince.class, description = "Time to show in \"yyyy-MM-dd HH:mm:ss.S\" format or \"\" to set now")
        private long SelectedTime = Performance.getMillisSince();

        @Parameter(names = {"--selectedSampleRate", "-samplerate"}, description = "Name of SampleRate to show (Minute, Hour, HalFay, Day), Minute as default.")
        private SampleRate SelectedSampleRate = SampleRate.Minute;

        @Parameter(names = {"--fullscreen"}, arity = 1, description = "If true, show PerformanceViewer in fullscreen mode, default false.")
        private boolean Fullscreen = false;

        @Parameter(names = {"--updateTime"}, description = "Range in seconds between Procedure's files checks for updates, default 5.")
        private int UpdateTime = 5;

        @Parameter(names = {"--warn"}, arity = 1, description = "If true, print warning messages, default true.")
        private boolean Warn = true;

        @Parameter(names = {"--log"}, arity = 1, description = "If true, print log messages, default false.")
        private boolean Log = false;

        @Parameter(names = {"--verbose"}, arity = 1, description = "If true, print debug messages, default false.")
        private boolean Verbose = false;
    }

    static class timeToMillsSince implements IStringConverter<Long> {
        @Override
        public Long convert(String value) {
            if (value.isEmpty()) return (long)0;
            return SampleRate.toMSSince(value);
        }
    }

}

