package com.robypomper.utils.performance.collector;

import com.robypomper.utils.performance.Performance;

import java.sql.Timestamp;
import java.util.List;

/**
 * Performance Block representation.
 *
 * This class can store all info about a single execution block: start time, end
 * time, and duration.
 *
 * @since 0.1
 */
@SuppressWarnings({"WeakerAccess", "unused"})
public class ExecutionBlock implements ExecutionUnit {

    // ------
    // Fields
    // ------

    /**
     * Block Id.
     * It's set by user, and used to identify blocks against executions and
     * generate Procedure Block's lists.
     */
    public final String BlockName;
    private final Timestamp startTime;
    /**
     * Block execution start time.
     *
     * @return the timestamp of ExecutionBlock creation.
     */
    public Timestamp StartTime() { return startTime; }
    private Timestamp endTime;
    /**
     * Block execution end time.
     *
     * @return the timestamp of ExecutionBlock.stop() call.
     */
    public Timestamp EndTime() { return endTime; }
    private long duration;
    /**
     * Execution's duration in milliseconds.
     *
     * @return difference between EndTime and StartTime.
     */
    public long Duration() { return duration; /* ms */ }
    private Throwable error;
    /**
     * Block execution error.
     *
     * @return the error that interrupt the execution, null if no errors occured
     * @since 0.2
     */
    public Throwable Error() { return error; }
    /**
     * @return True only if method stop() was called.
     */
    public boolean isTerminated() { return endTime!=null; }
    /**
     * @return true if #error is false.
     * @see #isTerminated()
     */
    public boolean isTerminatedCorrectly() { return isTerminated() && error==null ; }
    /**
     * ExecutionUnit implementation.
     * {@inheritDoc}
     */
    public String getName() {
        return BlockName;
    }


    // -----------
    // Constructor
    // -----------

    /**
     * Create and start.
     * @param blockName block's name and id.
     */
    public ExecutionBlock(String blockName) {
        BlockName = blockName;
        startTime = new Timestamp(Performance.getMillisSince());
        endTime = null;
        duration = 0;
    }

    private ExecutionBlock(ExecutionBlock other) {
        BlockName = other.BlockName;
        startTime = other.startTime;
        endTime = other.endTime;
        duration = other.duration;
        error = other.error;
    }

    /**
     * Create a copy of passed execution block.
     *
     * This method perform a deep copy of the current ExecutionBlock.
     *
     * @param oldExecutionBlock execution block to clone from.
     * @return a new Execution identical to the original.
     */
    public static ExecutionBlock clone(ExecutionBlock oldExecutionBlock) {
        return new ExecutionBlock(oldExecutionBlock);
    }


    // -------
    // Actions
    // -------

    /**
     * Stop block. Register the last error occured.
     *
     * @param error the error occured
     */
    public void stop(Throwable error) {
        if (endTime != null)
            throw new IllegalStateException(String.format("stop() already called for block %s at %s.", BlockName, EndTime()));

        endTime = new Timestamp(Performance.getMillisSince());
        duration = EndTime().getTime() - StartTime().getTime();
        this.error = error;
    }


    // --------
    // Printing
    // --------

    /**
     * @param blocks ExecutionBlock's list to print.
     * @return string containing the list of passed blocks.
     * @since 0.2
     */
    public static String printBlocks(List<ExecutionBlock> blocks) {
        StringBuilder msg = new StringBuilder();
        String sep, ind, bol;
        sep = " ";
        ind = "";
        bol = "\\";

        int count=0;
        for (ExecutionBlock execution : blocks) {
            msg.append(new String(new char[count]).replace("\0", ind));
            msg.append(bol).append(" ").append(execution.getName()).append(sep);
            count++;
        }
        return msg.toString();
    }
}
