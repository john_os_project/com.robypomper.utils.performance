package com.robypomper.utils.performance.collector;

/**
 * The common interface for Performance's Procedure Stack.
 *
 * All classes that represent an execution (like Execution or ExecutionBlock)
 * must implement this interface to be added on Performance's Procedure Stack.
 *
 * @since 0.2
 */
public interface ExecutionUnit {
    String getName();
}
