package com.robypomper.utils.performance.collector;

import com.robypomper.utils.performance.Performance;
import com.robypomper.utils.performance.analyzer.ExecutionStats;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Stack;
import java.util.stream.Collectors;

/**
 * Performance Execution representation.
 *
 * This class can store all info about a procedure execution: start time, end
 * time, duration and executed blocks list.
 *
 * @since 0.1
 */
@SuppressWarnings({"WeakerAccess", "unused"})
public class Execution implements ExecutionUnit {

    // ------
    // Fields
    // ------

    /**
     * Execution Id.
     */
    public final String ExecutionId;
    /**
     * Name of the procedure to which this Execution belongs.
     */
    public final String ProcName;
    private Timestamp startTime;
    /**
     * Execution start time.
     *
     * @return the first (StartTime ordered) block StartTime, or null if no
     *         blocks has registered.
     */
    public Timestamp StartTime() { return startTime; }
    private Timestamp endTime;
    /**
     * Execution end time.
     *
     * @return the last (EndTime ordered) block EndTime, or null if no blocks
     *         has registered.
     */
    public Timestamp EndTime() { return endTime; }
    private Throwable error;
    /**
     * Execution error, this error was throws outside any execution's block.
     *
     * @return the error that interrupt the execution, null if no errors occurred.
     * @since 0.2
     */
    public Throwable Error() { return error; }
    private final List<Throwable> blockErrors = new ArrayList<>();
    /**
     * Block execution error.
     * Block's errors, this errors were throws by execution's block.
     *
     * @return the error that interrupt some execution's block, empty if no errors occurred
     * @since 0.2
     */
    public List<Throwable> BlockErrors() { return blockErrors; }
    /**
     * Execution executions's stats based on registered Blocks.
     * @since 0.2
     */
    public final ExecutionStats Stats;
    private final List<ExecutionBlock> blocks;
    /**
     * @return list of blocks registered for this Execution.
     */
    public final List<ExecutionBlock> Blocks() { return Collections.unmodifiableList(blocks); }
    /**
     * @return list of blocks registered for this Execution ordered by StartTime.
     */
    public final List<ExecutionBlock> BlocksOrderedByStart() { return Collections.unmodifiableList(blocksOrderByStartTime()); }
    /**
     * @return list of blocks registered for this Execution ordered by EndTime.
     */
    public final List<ExecutionBlock> BlocksOrderedByEnd() { return Collections.unmodifiableList(blocksOrderByEndTime()); }
    /**
     * @param blockName required block's name.
     * @return ExecutionBlock with given name.
     */
    public final ExecutionBlock getBlock(String blockName) { for (ExecutionBlock block : BlocksOrderedByEnd()) if (blockName.compareTo(block.BlockName)==0) return block; return null; }
    /**
     * @return count of registered blocks.
     */
    public int size() { return blocks.size(); }
    /**
     * @return true if no block has registered.
     */
    public boolean isEmpty() { return size()==0; }
    private Stack<ExecutionUnit> stack = null;
    /**
     * @return performance's Procedures stack during the execution start.
     * @see "Execution::registerStack"
     * @since 0.2
     */
    public Stack<ExecutionUnit> Stack() { return stack; }
    /**
     * ExecutionUnit implementation.
     * {@inheritDoc}
     */
    public String getName() {
        return ProcName;
    }


    // -----------
    // Constructor
    // -----------

    /**
     * Create new Execution associated to the procedure procName.
     *
     * @param executionId unique execution identifier.
     * @param executionProcName procedure to associate with.
     */
    public Execution(String executionId, String executionProcName) {
        ExecutionId = executionId;
        ProcName = executionProcName;
        startTime = null;
        endTime = null;
        Stats = new ExecutionStats();
        blocks = new ArrayList<>();
    }

    private Execution(Execution other, String executionId) {
        ExecutionId = executionId;
        ProcName = other.ProcName;
        startTime = other.startTime;
        endTime = other.endTime;
        error = other.error;
        Stats = new ExecutionStats(other.Stats);
        blocks = new ArrayList<>();
        for (ExecutionBlock block : other.blocks)
            blocks.add(ExecutionBlock.clone(block));
    }

    /**
     * Create a copy of passed execution.
     *
     * This method perform a deep copy of the current Execution, including all blocks.
     *
     * @param oldExecution execution to clone from.
     * @param newExecutionId id for new created execution.
     * @return a new Execution identical to the original, except for his Id.
     */
    public static Execution clone(Execution oldExecution, String newExecutionId) {
        return new Execution(oldExecution,newExecutionId);
    }

    public String toString() {
        return String.format("%s@%s > %s", ProcName, StartTime(),EndTime());
    }


    // -------
    // Actions
    // -------

    /**
     * Register and start a new block.
     * @param blockName block Id for the new ExecutionBlock to create.
     * @return the ExecutionBlock started
     */
    public ExecutionBlock startBlock(String blockName) {
        ExecutionBlock block = new ExecutionBlock(blockName);
        if (isEmpty()) {
            startTime = block.StartTime();
            endTime = block.StartTime();
        }

        blocks.add(block);
        if (startTime.getTime()>block.StartTime().getTime())
            startTime = block.StartTime();

        return block;
    }

    /**
     * Stop the blockId block.
     * @param blockName block Id of the ExecutionBlock to stop.
     * @param error the info of the error that interrupt the block execution
     * @return the ExecutionBlock stopped or null if Execution doesn't contains
     *         blockName
     */
    public ExecutionBlock endBlock(String blockName, Throwable error) {
        ExecutionBlock block = getBlock(blockName);
        if (block==null)
            throw new IllegalStateException(String.format("Block '%s' can't be ended because not started.", blockName));

        if (block.isTerminated())
            throw new IllegalStateException(String.format("Block '%s' can be ended only once by PerformanceCollector::endRecording().", blockName));

        block.stop(error);
        if (error!=null)
            blockErrors.add(error);

        if (endTime.getTime()<block.EndTime().getTime())
            endTime = block.EndTime();

        Stats.register(block.Duration());

        return block;
    }

    /**
     * Associate Performance's Procedures stack to current Execution.
     *
     * This function must be called only once by the PerformanceCollector when
     * the execution terminate.
     *
     * @param currentStack Performance's Procedures stack to associate at current
     *                     Execution.
     */
    public void registerStack(Stack<ExecutionUnit> currentStack) {
        if (stack!=null)
            throw new IllegalStateException("Stack can be set only once by PerformanceCollector::terminateExecution().");

        stack = new Stack<>();
        stack.addAll(currentStack);
        stack.pop();
    }

    public void registerError(Throwable executionError){
        if (error!=null)
            throw new IllegalStateException("Error can be set only once by PerformanceCollector::terminateExecution(Throwable).");

        error = executionError;
    }

    // -------
    // Sorting
    // -------

    private List<ExecutionBlock> blocksOrderByStartTime() {
        return blocks.stream().sorted((o1, o2) -> {
                    long o1Time = o1.StartTime().getTime();
                    long o2Time = o2.StartTime().getTime();
                    return (int)(o1Time - o2Time);
                }
        ).collect(Collectors.toList());
    }

    private List<ExecutionBlock> blocksOrderByEndTime() {
        return blocks.stream().sorted((o1, o2) -> {
            long o1Time = o1.EndTime()!=null ? o1.EndTime().getTime() : Performance.getMillisSince();
            long o2Time = o2.EndTime()!=null ? o2.EndTime().getTime() : Performance.getMillisSince();
            return (int)(o2Time - o1Time);
        }
        ).collect(Collectors.toList());
    }

}
