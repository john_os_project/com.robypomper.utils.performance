package com.robypomper.utils.performance.viewer.nodes;


import com.robypomper.utils.performance.analyzer.ExecutionStats;
import com.robypomper.utils.performance.viewer.ProcedureViewersConsts;
import org.graphstream.graph.Edge;
import org.graphstream.graph.Node;

import java.util.ArrayList;
import java.util.List;

@SuppressWarnings({"WeakerAccess", "unused"})
public class NodeBlock extends Base {

    // -----------------------
    // Static instances access
    // -----------------------

    private static List<NodeBlock> nodes = new ArrayList<>();

    public static NodeBlock get(Node nodeBlock) {
        // search
        for (NodeBlock block : nodes)
            if (block.MainNode==nodeBlock)
                return block;

        // or create
        try {
            NodeBlock block = new NodeBlock(nodeBlock);
            nodes.add(block);
            return block;
        } catch (IllegalArgumentException ignore) {
            return null;
        }
    }


    // -----------
    // Constructor
    // -----------

    private NodeBlock(Node block) {
        super(block);
        if (!isBlock(block)) throw new IllegalArgumentException("Node is not a Block nodes.");
    }

    public static boolean isBlock(Node n) {
        return n!=null && n.hasLabel(ProcedureViewersConsts.CLASS) && n.getLabel(ProcedureViewersConsts.CLASS).equals(ProcedureViewersConsts.CLASS_BLOCK);
    }


    // ----------
    // Get errors
    // ----------

    public List<NodeError> getErrors() { // out
        return cast(getOutNode(ProcedureViewersConsts.CLASS, ProcedureViewersConsts.CLASS_ERR_BLOCK),NodeError.class);
    }

    // ------------------------
    // Get neighbors procedures
    // ------------------------

    public Node getMainProcedure() {
        if (getInNode(ProcedureViewersConsts.CLASS,ProcedureViewersConsts.CLASS_PROC).size()==1)
            return getInNode(ProcedureViewersConsts.CLASS,ProcedureViewersConsts.CLASS_PROC).get(0);
        return null;
    }

    public List<NodeProcedure> getProceduresCalled() { // out
        return cast(getOutNode(ProcedureViewersConsts.CLASS,ProcedureViewersConsts.CLASS_PROC),NodeProcedure.class);
    }


    // -------------------
    // Get sequence blocks
    // -------------------

    public boolean isRootSeq() {
        return getPrevBlockSeq().size()==0;
    }

    public List<NodeBlock> getPrevBlockSeq() {
        return getPrevBlockSeq(false);
    }

    public List<NodeBlock> getPrevBlockSeq(boolean avoidRecursion) {
        if (!avoidRecursion) return cast(getInNode(ProcedureViewersConsts.CLASS,ProcedureViewersConsts.CLASS_BLOCK),NodeBlock.class);
        List<NodeBlock> prevs = cast(getInNode(ProcedureViewersConsts.CLASS,ProcedureViewersConsts.CLASS_BLOCK),NodeBlock.class);
        if (prevs != null) prevs.remove(this);
        return prevs;
    }

    public List<Edge> getPrevBlockSeq_Edges() {
        return getEdgesBetween(getPrevBlockSeq(false),this);
    }

    public NodeBlock getPrevBlockSeq_Main() {
        Edge mainPrevEdge = null;
        double mainVal = Double.MIN_VALUE;

        for (Edge edge : getPrevBlockSeq_Edges()) {
            ExecutionStats edgeStats = edge.getAttribute(ProcedureViewersConsts.EXEC_STATS);
            if (edgeStats!=null && edgeStats.ExecCount() > mainVal) {
                mainPrevEdge = edge;
                mainVal = edgeStats.ExecCount();
            }
        }

        if (mainPrevEdge==null) return null;

        Node mainPrevBlockNode = mainPrevEdge.getOpposite(MainNode);
        if (mainPrevBlockNode==null) return null;

        return get(mainPrevBlockNode);
    }

    public List<NodeBlock> getNextBlockSeq() {
        return getNextBlockSeq(true);
    }

    public List<NodeBlock> getNextBlockSeq(boolean avoidRecursion) {
        if (!avoidRecursion) return cast(getOutNode(ProcedureViewersConsts.CLASS,ProcedureViewersConsts.CLASS_BLOCK),NodeBlock.class);
        List<NodeBlock> nexts = cast(getOutNode(ProcedureViewersConsts.CLASS,ProcedureViewersConsts.CLASS_BLOCK),NodeBlock.class);
        if (nexts!=null) nexts.remove(this);
        return nexts;
    }

    public List<Edge> getNextBlockSeq_Edges() {
        return getEdgesBetween(this,getNextBlockSeq(true));
    }

    public List<NodeError> getNextErrorSeq() {
        return cast(getOutNode(ProcedureViewersConsts.CLASS,ProcedureViewersConsts.CLASS_ERR_PROC),NodeError.class);
    }

    public List<Edge> getNextErrorSeq_Edges() {
        return getEdgesBetween(this,getNextErrorSeq());
    }

}

