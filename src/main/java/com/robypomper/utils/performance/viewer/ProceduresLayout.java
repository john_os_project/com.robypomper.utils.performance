package com.robypomper.utils.performance.viewer;

import com.robypomper.utils.performance.viewer.boxes.BoxGhost;
import com.robypomper.utils.performance.viewer.boxes.BoxProc;
import com.robypomper.utils.performance.viewer.nodes.NodeGhost;
import com.robypomper.utils.performance.viewer.nodes.NodeInfo;
import com.robypomper.utils.performance.viewer.nodes.NodeProcedure;
import javafx.util.Pair;
import org.graphstream.graph.Graph;
import org.graphstream.graph.Node;
import org.graphstream.ui.geom.Point3;
import org.graphstream.ui.layout.Layout;


@SuppressWarnings("WeakerAccess")
public class ProceduresLayout extends BaseLayout implements Layout {

    // ------
    // Fields
    // ------

    private final Graph internalGraph;
    private boolean isVerb() { return false; }


    // ----------
    // Properties
    // ----------


    private boolean graphChanged = false;
    public void setChanged() {
        graphChanged = true;
    }
    /** If true, this class prints warning messages. */
    private boolean isWarnEnabled = true;
    @SuppressWarnings("WeakerAccess")
    public boolean getEnableWarning() { return isWarnEnabled; }
    @SuppressWarnings({"WeakerAccess", "RedundantSuppression", "unused"})
    protected boolean isWarn() { return getEnableWarning(); }
    @SuppressWarnings("WeakerAccess")
    public void setEnableWarning(boolean warnEnabled) {
        this.isWarnEnabled  = warnEnabled ;
    }
    /** If true, this class prints logs messages. */
    private boolean isLogEnabled = false;
    @SuppressWarnings("WeakerAccess")
    public boolean getEnableLog() { return isLogEnabled; }
    @SuppressWarnings("WeakerAccess")
    protected boolean isLog() { return getEnableLog(); }
    @SuppressWarnings("WeakerAccess")
    public void setEnableLog(boolean logEnabled) {
        this.isLogEnabled  = logEnabled ;
    }
    /** If true, this class prints debug messages. */
    private boolean isDebugEnabled = true;
    @SuppressWarnings("WeakerAccess")
    public boolean getEnableDebug() { return isDebugEnabled; }
    @SuppressWarnings("WeakerAccess")
    protected boolean isDeb() { return getEnableDebug(); }
    @SuppressWarnings("WeakerAccess")
    public void setEnableDebug(boolean debEnabled) {
        this.isDebugEnabled  = debEnabled ;
    }


    // -----------
    // Constructor
    // -----------

    @SuppressWarnings("WeakerAccess")
    public ProceduresLayout (Graph graph) {
        super();
        internalGraph = graph;
    }


    // -----------------
    // Render Operations
    // -----------------

    @Override
    public void compute() {
        if (!graphChanged) return;
        if (isDeb()) System.out.println(String.format("%-25s:\t\tStart computing Graph Layout....", "ProceduresLayout"));

        Pair<Point3,Point3> sizeProcs = renderProcedures(new Point3(0, 0, 0));
        Pair<Point3,Point3> sizeGhosts = renderProcedureGhosts(new Point3(0, 0, 0));
        Pair<Point3,Point3> sizeInfo = renderInfo(new Point3(0, 0, 0));
        //noinspection unused
        Pair<Point3,Point3> size = new Pair<>(
                new Point3(
                        sizeGhosts.getKey().x,
                        sizeInfo.getKey().y,
                        0),
                new Point3(
                        Math.max(sizeInfo.getValue().x,sizeProcs.getValue().x),
                        Math.min(sizeGhosts.getValue().x,sizeProcs.getValue().x),
                        0)
                );

        graphChanged = false;
        
        if (isDeb()) System.out.println(String.format("%-25s:\t\tGraph Layout computed.", "ProceduresLayout"));
    }

    private Pair<Point3,Point3> renderProcedures(Point3 offset) {
        Point3 offsetStart = new Point3(offset.x, offset.y, offset.z);
        Point3 offsetEnd = new Point3(offset.x, offset.y, offset.z);

        // order and render roots procedures
        for (Node n : internalGraph) {
            NodeProcedure proc = NodeProcedure.get(n);
            if (proc==null) continue;
            BoxProc procBox = BoxProc.get(proc);
            if (procBox==null) continue;

            if (procBox.Proc.isRoot()) {
                if (isDeb()) System.out.println(String.format("%-25s:\t\tRender root Procedure '%s'.", "ProceduresLayout", n.getId()));
                Pair<Point3, Point3> size = procBox.render(offset);

                offset.x = 0;
                offset.y = size.getValue().y;
                offset.z = 0;

                offsetEnd.x = Math.max(offsetEnd.x, size.getValue().x);
                offsetEnd.y = size.getValue().y;
                offsetEnd.z = size.getValue().z;
                if (isLog() && isVerb()) System.out.println(String.format("# Rendered root Procedure '%s'\t\tstart('%s')\tend('%s')", n.getId(), size.getKey(), size.getValue()));
            }
        }

        return new Pair<>(offsetStart,offsetEnd);
    }

    private Pair<Point3,Point3>  renderProcedureGhosts(Point3 offset) {
        Point3 offsetStart = new Point3(offset.x, offset.y, offset.z);
        Point3 offsetEnd = new Point3(offset.x, offset.y, offset.z);

        // order and render roots procedures
        for (Node n : internalGraph) {
            NodeGhost ghost = NodeGhost.get(n);
            if (ghost==null) continue;
            BoxGhost ghostBox = BoxGhost.get(ghost);
            if (ghostBox==null) continue;

            if (isDeb()) System.out.println(String.format("%-25s:\t\tRender Ghost Procedure '%s'.", "ProceduresLayout", n.getId()));
            Pair<Point3, Point3> size = ghostBox.render(offset);
            offset.x = size.getKey().x - size.getValue().x;
            size = ghostBox.render(offset);

            offsetEnd.x = Math.max(offsetEnd.x, size.getValue().x);
            offsetEnd.y = size.getValue().y;
            offsetEnd.z = size.getValue().z;

            offsetStart.x = Math.min(offsetStart.x, size.getKey().x);

            offset.x = 0;
            offset.y = size.getValue().y;
            offset.z = 0;
            if (isLog() && isVerb()) System.out.println(String.format("# Rendered Ghost Procedure '%s'\t\tstart('%s')\tend('%s')", n.getId(), size.getKey(), size.getValue()));
        }

        return new Pair<>(offsetStart,offsetEnd);
    }

    private Pair<Point3,Point3> renderInfo(Point3 offset) {
        Point3 offsetStart = new Point3(offset.x, offset.y, offset.z);
        Point3 offsetEnd = new Point3(offset.x, offset.y, offset.z);

        // order and render roots procedures
        for (Node n : internalGraph) {
            NodeInfo info = NodeInfo.get(n);
            if (info==null) continue;

            if (isDeb()) System.out.println(String.format("%-25s:\t\tRender Info '%s'.", "ProceduresLayout", n.getId()));
            Pair<Point3, Point3> size = info.render(offset);
            offset.y = size.getKey().y - size.getValue().y;
            size = info.render(offset);

            offset.x = size.getValue().x;
            offset.y = 0;
            offset.z = 0;

            offsetEnd.x = size.getValue().x;
            offsetEnd.y = Math.max(offsetEnd.y, size.getValue().y);
            offsetEnd.z = size.getValue().z;
            if (isLog() && isVerb()) System.out.println(String.format("# Rendered Info '%s'\t\tstart('%s')\tend('%s')", n.getId(), size.getKey(), size.getValue()));
        }

        return new Pair<>(offsetStart,offsetEnd);
    }

    // --------------
    // Graph's events
    // --------------

    // Default impl in PipeBase
    //// Graphs
    //public void graphAttributeAdded(String sourceId, long timeId, String attribute, Object value) {}
    //public void graphAttributeChanged(String sourceId, long timeId, String attribute, Object oldValue, Object newValue) {}
    //public void graphAttributeRemoved(String sourceId, long timeId, String attribute) {}
    //// Nodes
    public void nodeAdded(String sourceId, long timeId, String nodeId) { graphChanged=true; }
    //public void nodeRemoved(String sourceId, long timeId, String nodeId)  { }
    //public void nodeAttributeAdded(String sourceId, long timeId, String nodeId, String attribute, Object value) {}
    //public void nodeAttributeChanged(String sourceId, long timeId, String nodeId, String attribute, Object oldValue, Object newValue) {}
    //public void nodeAttributeRemoved(String sourceId, long timeId, String nodeId, String attribute) {}
    //// Edges
    //public void edgeAdded(String sourceId, long timeId, String edgeId, String fromNodeId, String toNodeId, boolean directed) { }
    //public void edgeRemoved(String sourceId, long timeId, String edgeId)  { }
    //public void edgeAttributeAdded(String sourceId, long timeId, String edgeId, String attribute, Object value) {}
    //public void edgeAttributeChanged(String sourceId, long timeId, String edgeId, String attribute, Object oldValue, Object newValue) {}
    //public void edgeAttributeRemoved(String sourceId, long timeId, String edgeId, String attribute) {}
    //// Generic
    //public void graphCleared(String sourceId, long timeId) {}
    //public void stepBegins(String sourceId, long timeId, double step) {}

    // Default impl in SourceBase <-ext- PipeBase
    //// Sink
    //public void addSink(Sink sink) {}
    //public void removeSink(Sink sink) {}
    //public void clearSinks() {}
    //// Attribute Sink
    //public void addAttributeSink(AttributeSink sink) {}
    //public void removeAttributeSink(AttributeSink sink) {}
    //// Element Sink
    //public void clearAttributeSinks() {}
    //public void addElementSink(ElementSink sink) {}
    //public void removeElementSink(ElementSink sink) {}
    //public void clearElementSinks() {}

}
