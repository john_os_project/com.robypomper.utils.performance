package com.robypomper.utils.performance.viewer;

import com.robypomper.utils.ColorPatterns;
import com.robypomper.utils.history.SampleRate;
import com.robypomper.utils.performance.analyzer.ExecutionStats;
import com.robypomper.utils.performance.analyzer.Procedure;
import com.robypomper.utils.performance.analyzer.ProceduresCollection;
import com.robypomper.utils.performance.viewer.nodes.NodeProcedure;
import org.graphstream.graph.Edge;
import org.graphstream.graph.Graph;
import org.graphstream.graph.Node;

import java.nio.file.Path;
import java.util.Iterator;
import java.util.Map;


@SuppressWarnings("UnnecessaryLocalVariable")
class ProceduresGraphPopulator {

    private static class Limits {
        int maxProcCount = 0;
        double maxProcAvg = 0;
        int maxBlockCount = 0;
        double maxBlockAvg = 0;
        int maxStackCount = 0;
        double maxStackAvg = 0;

        Limits(ProceduresCollection procedures) {
            for (Procedure proc : procedures.Procedures().values()) {
                if (maxProcCount < proc.Stats.ExecCount()) maxProcCount = proc.Stats.ExecCount();
                if (maxProcAvg < proc.Stats.ExecCount()) maxProcAvg = proc.Stats.ExecDurationAvg();

                for (ExecutionStats block : proc.Blocks().values()) {
                    if (maxBlockCount < proc.Stats.ExecCount()) maxBlockCount = block.ExecCount();
                    if (maxBlockAvg < proc.Stats.ExecCount()) maxBlockAvg = block.ExecCount();
                }

                for (ExecutionStats stack : proc.Stacks().values()) {
                    if (maxStackCount < proc.Stats.ExecCount()) maxStackCount = stack.ExecCount();
                    if (maxStackAvg < proc.Stats.ExecCount()) maxStackAvg = stack.ExecCount();
                }
            }
        }
    }


    // ------
    // Fields
    // ------

    private static boolean isVerb() { return false; }


    // ----------
    // Properties
    // ----------

    /** If true, this class prints warning messages. */
    private static boolean isWarnEnabled = true;
    @SuppressWarnings("WeakerAccess")
    public static boolean getEnableWarning() { return isWarnEnabled; }
    @SuppressWarnings({"WeakerAccess", "RedundantSuppression", "unused"})
    protected static boolean isWarn() { return getEnableWarning(); }
    @SuppressWarnings("WeakerAccess")
    public static void setEnableWarning(boolean warnEnabled) {
        isWarnEnabled  = warnEnabled ;
    }
    /** If true, this class prints logs messages. */
    private static boolean isLogEnabled = false;
    @SuppressWarnings("WeakerAccess")
    public static boolean getEnableLog() { return isLogEnabled; }
    @SuppressWarnings({"WeakerAccess", "RedundantSuppression", "unused"})
    protected static boolean isLog() { return getEnableLog(); }
    @SuppressWarnings("WeakerAccess")
    public static void setEnableLog(boolean logEnabled) {
        isLogEnabled  = logEnabled ;
    }
    /** If true, this class prints debug messages. */
    private static boolean isDebugEnabled = true;
    @SuppressWarnings("WeakerAccess")
    public static boolean getEnableDebug() { return isDebugEnabled; }
    @SuppressWarnings("WeakerAccess")
    protected static boolean isDeb() { return getEnableDebug(); }
    @SuppressWarnings("WeakerAccess")
    public static void setEnableDebug(boolean debEnabled) {
        isDebugEnabled  = debEnabled ;
    }

    private static boolean isPopulating = false;
    private static boolean isUpdating = false;

    // ----------------
    // Graph population
    // ----------------

    static void populateGraph(Graph graph, ProceduresCollection procedures) {
        if (graph==null || procedures==null) return;
        if (isPopulating || isUpdating) return;

        if (isDeb()) System.out.println(String.format("%-25s:\t\tStart populating Graph....", "PopulateGraphPopulator"));
        isPopulating=true;

        Limits limits = new Limits(procedures);

        for (Procedure proc : procedures.Procedures().values()) {
            Node procNode = addProcedure(graph,proc,limits);
            addProcedureErrors(graph,procNode,proc.Errors());
            addBlocks(graph,procNode,proc,limits);
        }

        if (isDeb() && isVerb()) System.out.println(String.format("%-25s:\t\tStart adding relationships...", "PopulateGraphPopulator"));
        for (Procedure proc : procedures.Procedures().values()) {
            Node procNode = graph.getNode(proc.ProcName);
            addStacks(graph,procNode,proc.ProcName,proc.Stacks(),limits);
            addBlockSequences(graph,procNode,proc.ProcName,proc.BlockSequences(),limits);
        }

        if (isDeb() && isVerb()) System.out.println(String.format("%-25s:\t\tStart adding procedures info...", "PopulateGraphPopulator"));
        addProceduresCollectionInfo(graph,procedures);

        isPopulating=false;
        if (isDeb()) System.out.println(String.format("%-25s:\t\tGraph Populated.", "PopulateGraphPopulator"));
    }

    static void updateGraph(Graph graph, ProceduresCollection procedures) {
        if (graph==null || procedures==null) return;
        if (isPopulating || isUpdating) return;

        if (isDeb()) System.out.println(String.format("%-25s:\t\tStart updating Graph....", "PopulateGraphPopulator"));
        isUpdating=true;

        Limits limits = new Limits(procedures);

        for (Procedure proc : procedures.Procedures().values()) {
            Node procNode = addProcedure(graph,proc,limits);
            addProcedureErrors(graph,procNode,proc.Errors());
            addBlocks(graph,procNode,proc,limits);
        }

        if (isDeb() && isVerb()) System.out.println(String.format("%-25s:\t\tStart adding relationships...", "PopulateGraphPopulator"));
        for (Procedure proc : procedures.Procedures().values()) {
            Node procNode = graph.getNode(proc.ProcName);
            addStacks(graph,procNode,proc.ProcName,proc.Stacks(),limits);
            addBlockSequences(graph,procNode,proc.ProcName,proc.BlockSequences(),limits);
        }

        if (isDeb() && isVerb()) System.out.println(String.format("%-25s:\t\tStart adding procedures info...", "PopulateGraphPopulator"));
        addProceduresCollectionInfo(graph,procedures);

        isUpdating=false;
        if (isDeb()) System.out.println(String.format("%-25s:\t\tGraph Updated.", "PopulateGraphPopulator"));
    }

    private static Node addProcedure(Graph graph, Procedure proc, Limits limits) {
        String name = proc.ProcName;

        // add procedure nodes
        Node procNode = addNode(graph,name,"Procedure");
        if (procNode==null) return null;
        procSetAttributes(procNode,proc,limits);
        addNodeLog(name,"Procedure");

        return procNode;
    }

    private static void addProcedureErrors(Graph graph, Node procNode, Map<String, ExecutionStats> errors) {
        for (Map.Entry<String, ExecutionStats> entry : errors.entrySet()) {
            String errorName = entry.getKey();
            ExecutionStats procErrorStats = entry.getValue();
            String name = procNode.getId() + ":=:" + errorName + "";

            // add error nodes
            Node procErrorNode = addNode(graph,name,"ProcedureError");
            if (procErrorNode==null) return;
            procErrorSetAttributes(procErrorNode,errorName,procErrorStats);
            addNodeLog(name,"ProcedureError");

            // add proc 2 error block edge
            String nameEdge = procNode.getId() + ">[" + errorName + "]";
            Edge proc2ErrProcEdge = addEdge(graph,nameEdge,procNode.getId(),procErrorNode.getId(),"Proc2Error");
            proc2ProcErrorSetAttributes(proc2ErrProcEdge,procErrorStats);
            addEdgeLog(nameEdge,procNode.getId(),procErrorNode.getId(),"Proc2Error");
        }
    }

    private static void addBlocks(Graph graph, Node procNode, Procedure proc, Limits limits) {
        for (Map.Entry<String, ExecutionStats> entry : proc.Blocks().entrySet()) {
            String blockName = entry.getKey();
            ExecutionStats blockStats = entry.getValue();
            String name = proc.ProcName + ":=:" + blockName;

            // add block nodes
            Node blockNode = addNode(graph,name,"Block");
            if (blockNode==null) return;
            blockSetAttributes(blockNode,proc.ProcName,blockName,blockStats,limits);
            addNodeLog(name,"Block");

            // add block's error nodes
            addBlockErrors(graph,blockNode,proc.BlocksErrors().get(blockName));

            // add procedure 2 block edge
            String nameEdge = proc.ProcName + ">" + blockName;
            Edge proc2BlockEdge = addEdge(graph,nameEdge,procNode.getId(),blockNode.getId(),"Proc2Block");  // Throw ElementNotFoundException
            proc2BlockSetAttributes(proc2BlockEdge,blockStats,limits);
            addEdgeLog(nameEdge,procNode.getId(),blockNode.getId(),"Proc2Block");
        }
    }

    private static void addBlockErrors(Graph graph, Node blockNode, Map<String, ExecutionStats> errors) {
        if (errors==null) return;
        for (Map.Entry<String, ExecutionStats> entry : errors.entrySet()) {
            String errorName = entry.getKey();
            ExecutionStats blockErrorStats = entry.getValue();
            String name = blockNode.getId() + "[" + errorName + "]";

            // add error nodes
            Node blockErrorNode = addNode(graph,name,"Block");
            if (blockErrorNode==null) return;
            blockErrorSetAttributes(blockErrorNode,errorName,blockErrorStats);
            addNodeLog(name,"BlockError");

            // add block 2 error block edge
            String nameEdge = blockNode.getId() + ">[" + errorName + "]";
            Edge block2ErrBlockEdge = addEdge(graph,nameEdge,blockNode.getId(),blockErrorNode.getId(),"Block2Error");  // Throw IdAlreadyInUseException
            block2BlockErrorSetAttributes(block2ErrBlockEdge,blockErrorStats);
            addEdgeLog(nameEdge,blockNode.getId(),blockErrorNode.getId(),"Block2Error");
        }
    }

    private static void addStacks(Graph graph, @SuppressWarnings("unused") Node procNode, String procName, Map<String, ExecutionStats> stacks, Limits limits) {
        for (Map.Entry<String, ExecutionStats> entry : stacks.entrySet()) {
            String stackPath = entry.getKey();
            ExecutionStats stackStats = entry.getValue();
            if (isDeb()) System.out.println(String.format("%-25s:\t\tAdd '%s' Stack '%s'.", "PopulateGraphPopulator", procName, stackPath));

            // get blocks call (last block and proc)
            if (stackPath.isEmpty()) continue;
            String[] strings = stackPath.split("\\\\");
            if (strings.length==0) continue;

            String lastBlock, lastProc = "__Unknow__";
            lastBlock = strings[strings.length - 1].trim();
            int count = 2;
            while (graph.getNode(lastProc)==null && strings.length - count >= 0) {
                if (!strings[strings.length - count].trim().isEmpty())
                    lastProc = strings[strings.length - count].trim();
                count++;
            }
            String labelFrom = lastProc + ":=:" + lastBlock;
            @SuppressWarnings("UnnecessaryLocalVariable") String labelTo = procName;

            // add ghost calls
            if (graph.getNode(labelFrom)==null)
                addGhostProcedure(graph,labelFrom);

            // add block 2 procedure edge
            String nameEdge = labelFrom + ">" + labelTo;
            Edge block2ProcEdge = addEdge(graph,nameEdge, labelFrom, labelTo,"Block2Proc");    // Throw2 IdAlreadyInUseException
            block2ProcSetAttributes(block2ProcEdge,stackStats,limits);
            addEdgeLog(nameEdge,labelFrom,labelTo,"Block2Proc");

        }
    }

    private static void addBlockSequences(Graph graph, @SuppressWarnings("unused") Node procNode, String procName, Map<String, ExecutionStats> blockSequences, Limits limits) {
        int seqCount = 1;
        for (Map.Entry<String, ExecutionStats> entry : blockSequences.entrySet()) {
            String seqPath = entry.getKey();
            ExecutionStats seqStats = entry.getValue();
            if (isDeb()) System.out.println(String.format("%-25s:\t\tAdd '%s' BlockSequence '%s'.", "PopulateGraphPopulator", procName, seqPath));


            if (seqPath.isEmpty()) continue;
            String[] blocks = seqPath.trim().split("\\\\");
            if (blocks.length==0) continue;
            String colorSequence = ColorPatterns.getColors(blockSequences.size())[seqCount++];

            int count = 0;
            while (blocks.length>count+1) {
                String prevBlock = blocks[count].trim();
                String nextBlock = blocks[count+1].trim();


                if (!prevBlock.isEmpty()) {
                    String labelFrom = procName + ":=:" + prevBlock;
                    String labelTo = procName + ":=:" + nextBlock;
                    String nameEdge = labelFrom + ">" + labelTo;

                    Edge block2BlockEdge = graph.getEdge(nameEdge);
                    if (block2BlockEdge == null) {
                        block2BlockEdge = addEdge(graph,nameEdge, labelFrom, labelTo, "Block2Block");
                        block2BlockSetAttributes(block2BlockEdge, seqStats, limits, colorSequence);
                        addEdgeLog(nameEdge,labelFrom,labelTo,"Block2Block");

                    } else {
                        block2BlockUpdAttributes(block2BlockEdge, seqStats, limits);
                    }

                    // remove edge from Procedure
                    Node nextBlockNode = graph.getNode(labelTo);
                    Iterator<Edge> it = nextBlockNode.getEnteringEdgeSet().iterator();
                    while (it.hasNext()) {
                        Node blockCaller = it.next().getOpposite(nextBlockNode);
                        if (NodeProcedure.isProcedure(blockCaller))
                            it.remove();
                    }
                }
                count++;
            }
        }
    }

    @SuppressWarnings("UnusedReturnValue")
    private static Node addGhostProcedure(Graph graph, String label) {

        // add ghost nodes
        Node ghostNode = addNode(graph,label,"GhostProc");
        if (ghostNode==null) return null;
        ghostSetAttribute(ghostNode,label);
        addNodeLog(label,"GhostProcedure");

        return ghostNode;
    }

    private static void addProceduresCollectionInfo(Graph graph, ProceduresCollection procedures) {
        Node srNode = addNode(graph,ProcedureViewersConsts.EXEC_INFO_SR,"InfoNode");
        if (srNode!=null) infoSetAttributes(srNode,"SampleRate: " + procedures.ProcsSampleRate.name());

        Node startNode = addNode(graph,ProcedureViewersConsts.EXEC_INFO_START,"InfoNode");
        if (startNode!=null) infoSetAttributes(startNode,"StartTime: " + SampleRate.toFormattedDate(procedures.ProcsSampleRate.getStart(procedures.StartTime().getTime())));

        Node startEffectiveNode = addNode(graph,ProcedureViewersConsts.EXEC_INFO_START_EFFECTIVE,"InfoNode");
        if (startEffectiveNode!=null) infoSetAttributes(startEffectiveNode,"StartTime*: " + SampleRate.toFormattedDate(procedures.StartTime().getTime()));

        Node endNode = addNode(graph,ProcedureViewersConsts.EXEC_INFO_END,"InfoNode");
        if (endNode!=null) infoSetAttributes(endNode,"EndTime: " + SampleRate.toFormattedDate(procedures.ProcsSampleRate.getEnd(procedures.EndTime().getTime())));

        Node endEffectiveNode = addNode(graph,ProcedureViewersConsts.EXEC_INFO_END_EFFECTIVE,"InfoNode");
        if (endEffectiveNode!=null) infoSetAttributes(endEffectiveNode,"EndTime*: " + SampleRate.toFormattedDate(procedures.EndTime().getTime()));

        Node fileNode = addNode(graph,ProcedureViewersConsts.EXEC_INFO_FILE,"InfoNode");
        if (fileNode!=null) infoSetAttributes(fileNode,"FileName: " + procedures.ProceduresFilename());
    }


    // ---------
    // Add Nodes
    // ---------

    private static void addNodeLog(String name, String type) {
        if (isDeb()) System.out.println(String.format("%-25s:\t\tAdded node %s '%s'.", "PopulateGraphPopulator", type, name));
    }

    @SuppressWarnings("unused")
    private static void addEdgeLog(String name, String from, String to, String type) {
        if (isDeb()) System.out.println(String.format("%-25s:\t\tAdded edge %s '%s'.", "PopulateGraphPopulator", type, name));
        //if (isDeb()) System.out.println(String.format("%-25s:\t\tAdded edge %s '%s'.\t\t\tfrom: %s\tto:%s", "PopulateGraphPopulator", type, name, from, to));
    }

    private static Node addNode(Graph graph, String name, String type) {
        Node node;
        try {
            node = graph.addNode(name);
        } catch (Exception e) {
            node = graph.getNode(name);
            if (node==null) {
                if (isWarn()) System.out.println(String.format("%-25s:\t\tError on add %s '%s': [%s] %s", "PopulateGraphPopulator", type, name, e, e.getMessage()));
                return null;
            } else {
                if (isWarn() && isPopulating) System.out.println(String.format("%-25s:\t\tError on add %s '%s': already added (isUpdating=%s)", "PopulateGraphPopulator", type, name, isUpdating));
            }
        }
        return node;
    }

    private static Edge addEdge(Graph graph, String name, String fromId, String toId, String type) {
        if (graph.getNode(fromId)==null) {
            if (isWarn()) System.out.println(String.format("%-25s:\t\tError on add %s edge '%s': From node '%s' not found in graph.", "PopulateGraphPopulator", type, name, fromId));
            return null;
        }
        if (graph.getNode(toId)==null) {
            if (isWarn()) System.out.println(String.format("%-25s:\t\tError on add %s edge '%s': To node '%s' not found in graph.", "PopulateGraphPopulator", type, name, toId));
            return null;
        }

        Edge edge;
        try {
            edge = graph.addEdge(name,fromId,toId,true);
        } catch (Exception e) {
            edge = graph.getEdge(name);
            if (edge==null) {
                if (isWarn()) System.out.println(String.format("%-25s:\t\tError on add %s edge '%s': [%s] %s", "PopulateGraphPopulator", type, name, e, e.getMessage()));
                return null;
            } else {
                if (isWarn() && isPopulating) System.out.println(String.format("%-25s:\t\tError on add %s edge '%s': already added (isUpdating=%s)", "PopulateGraphPopulator", type, name, isUpdating));
            }
        }
        return edge;
    }


    // -----------------------
    // Set attributes to Nodes
    // -----------------------

    static void graphSetAttributes(Graph graph, Path cssPath) {
        if (graph==null) return;

        graph.removeAttribute(ProcedureViewersConsts.STYLESHEET);
        graph.removeAttribute(ProcedureViewersConsts.STYLESHEET_OLD);
        String cssString = ProceduresGraph.parseCss(cssPath);
        graph.setAttribute(ProcedureViewersConsts.STYLESHEET, cssString);
        graph.setAttribute(ProcedureViewersConsts.STYLESHEET_OLD, cssString);
        graph.setAttribute(ProcedureViewersConsts.QUALITY);
        graph.setAttribute(ProcedureViewersConsts.ANTIALIAS);
    }

    private static void procSetAttributes(Node procNode, Procedure proc, Limits limits) {
        String name = proc.ProcName;
        if (isDeb() && isVerb()) System.out.println(String.format("%-25s:\t\tSet Procedure '%s' Attributes.", "PopulateGraphPopulator", name));

        String label = String.format("%s (#%d)", name, proc.Stats.ExecCount());
        double execCountNormalized = proc.Stats.ExecCount() / (double)limits.maxProcCount;
        //double execAvgNormalized = proc.Stats.ExecDurationAvg() / limits.maxProcAvg;

        procNode.addAttribute(ProcedureViewersConsts.CLASS,ProcedureViewersConsts.CLASS_PROC);
        procNode.addAttribute(ProcedureViewersConsts.LABEL,label);
        procNode.addAttribute(ProcedureViewersConsts.SIZE,execCountNormalized);
        //procNode.addAttribute(COLOR,execAvgNormalized);

        procNode.addAttribute(ProcedureViewersConsts.EXEC_STATS,proc.Stats);
    }

    private static void procErrorSetAttributes(Node procErrorNode, String errorName, ExecutionStats procErrorStats) {
        String name = errorName;
        if (isDeb() && isVerb()) System.out.println(String.format("%-25s:\t\tSet ProcedureError '%s' Attributes.", "PopulateGraphPopulator", name));

        String label = String.format("%s (#%d)", name, procErrorStats.ExecCount());
        //double execCountNormalized = procErrorStats.ExecCount() / (double)limits.maxProcCount;
        //double execAvgNormalized = procErrorStats.ExecDurationAvg() / limits.maxProcAvg;

        procErrorNode.addAttribute(ProcedureViewersConsts.CLASS,ProcedureViewersConsts.CLASS_ERR_PROC);
        procErrorNode.addAttribute(ProcedureViewersConsts.LABEL,label);
        //procErrorNode.addAttribute(ProcedureViewersConsts.SIZE,execCountNormalized);
        procErrorNode.addAttribute(ProcedureViewersConsts.COLOR,"red");

        procErrorNode.addAttribute(ProcedureViewersConsts.EXEC_STATS,procErrorStats);
    }

    private static void blockSetAttributes(Node blockNode, @SuppressWarnings("unused") String procName, String blockName, ExecutionStats blockStats, Limits limits) {
        String name = blockName;
        if (isDeb() && isVerb()) System.out.println(String.format("%-25s:\t\tSet Block '%s' Attributes.", "PopulateGraphPopulator", name));

        String label = String.format("%s (#%d)", name, blockStats.ExecCount());
        //double execCountNormalized = blockStats.ExecCount() / (double)limits.maxProcCount;
        double execAvgNormalized = blockStats.ExecDurationAvg() / limits.maxProcAvg;

        blockNode.addAttribute(ProcedureViewersConsts.CLASS,ProcedureViewersConsts.CLASS_BLOCK);
        blockNode.addAttribute(ProcedureViewersConsts.LABEL,label);
        blockNode.addAttribute(ProcedureViewersConsts.SIZE,execAvgNormalized);
        //blockNode.addAttribute(COLOR,execCountNormalized);
//        if (block.Errors().size()>0) {
//            procNode.addAttribute(ProcedureViewersConsts.COLOR, "red");
//            procNode.addAttribute(ProcedureViewersConsts.LABEL,label + "ERR ###");
//        }

        blockNode.addAttribute(ProcedureViewersConsts.EXEC_STATS,blockStats);
    }

    private static void blockErrorSetAttributes(Node blockErrorNode, String errorName, ExecutionStats blockErrorStats) {
        String name = errorName;
        if (isDeb() && isVerb()) System.out.println(String.format("%-25s:\t\tSet BlockError '%s' Attributes.", "PopulateGraphPopulator", name));

        String label = String.format("%s (#%d)", name, blockErrorStats.ExecCount());
        //double execCountNormalized = blockErrorStats.ExecCount() / (double)limits.maxProcCount;
        //double execAvgNormalized = blockErrorStats.ExecDurationAvg() / limits.maxProcAvg;

        blockErrorNode.addAttribute(ProcedureViewersConsts.CLASS,ProcedureViewersConsts.CLASS_ERR_BLOCK);
        blockErrorNode.addAttribute(ProcedureViewersConsts.LABEL,label);
        //blockErrorNode.addAttribute(ProcedureViewersConsts.SIZE,execCountNormalized);
        blockErrorNode.addAttribute(ProcedureViewersConsts.COLOR,"red");

        blockErrorNode.addAttribute(ProcedureViewersConsts.EXEC_STATS,blockErrorStats);
    }

    private static void ghostSetAttribute(Node ghostNode, String label) {
        if (isDeb() && isVerb()) System.out.println(String.format("%-25s:\t\tSet Ghost '%s' Attributes.", "PopulateGraphPopulator", label));

        ghostNode.addAttribute(ProcedureViewersConsts.CLASS, ProcedureViewersConsts.CLASS_GHOST);
        ghostNode.addAttribute(ProcedureViewersConsts.LABEL, label);
        //ghostNode.addAttribute(SIZE,execAvgNormalized);
        //ghostNode.addAttribute(COLOR,execCountNormalized);

        //ghostNode.addAttribute(EXEC_STATS,null);
    }

    private static void infoSetAttributes(Node infoNode, String label) {
        if (isDeb() && isVerb()) System.out.println(String.format("%-25s:\t\tSet Info '%s' Attributes.", "PopulateGraphPopulator", label));

        infoNode.addAttribute(ProcedureViewersConsts.CLASS,ProcedureViewersConsts.CLASS_INFO);
        infoNode.addAttribute(ProcedureViewersConsts.LABEL,label);
    }


    // -----------------------
    // Set attributes to Edges
    // -----------------------

    private static void proc2ProcErrorSetAttributes(Edge proc2ErrProcEdge, ExecutionStats procErrorStats) {
        if (isDeb() && isVerb()) System.out.println(String.format("%-25s:\t\tSet '%s' Proc2ProcError edge Attributes.", "PopulateGraphPopulator", proc2ErrProcEdge.getId()));

        //double execCountNormalized = procErrorStats.ExecCount() / (double)limits.maxProcCount;

        proc2ErrProcEdge.addAttribute(ProcedureViewersConsts.CLASS,ProcedureViewersConsts.CLASS_CALL_PROC_ERR);
        //proc2ErrProcEdge.addAttribute(ProcedureViewersConsts.SIZE,execCountNormalized);
        //proc2ErrProcEdge.addAttribute(COLOR,execAvgNormalized);

        proc2ErrProcEdge.addAttribute(ProcedureViewersConsts.EXEC_STATS,procErrorStats);
    }

    private static void proc2BlockSetAttributes(Edge proc2BlockEdge, ExecutionStats blockStats, Limits limits) {
        if (isDeb() && isVerb()) System.out.println(String.format("%-25s:\t\tSet '%s' edge Proc2Block Attributes.", "PopulateGraphPopulator", proc2BlockEdge.getId()));

        double execCountNormalized = blockStats.ExecCount() / (double)limits.maxProcCount;

        proc2BlockEdge.addAttribute(ProcedureViewersConsts.CLASS,ProcedureViewersConsts.CLASS_CALL_STACK);
        proc2BlockEdge.addAttribute(ProcedureViewersConsts.SIZE,execCountNormalized);
        //proc2BlockEdge.addAttribute(COLOR,execAvgNormalized);

        proc2BlockEdge.addAttribute(ProcedureViewersConsts.EXEC_STATS,blockStats);
    }

    private static void block2BlockErrorSetAttributes(Edge block2ErrBlockEdge, ExecutionStats blockErrorStats) {
        if (isDeb() && isVerb()) System.out.println(String.format("%-25s:\t\tSet '%s' Block2BlockError edge Attributes.", "PopulateGraphPopulator", block2ErrBlockEdge.getId()));

        //double execCountNormalized = procErrorStats.ExecCount() / (double)limits.maxProcCount;

        block2ErrBlockEdge.addAttribute(ProcedureViewersConsts.CLASS,ProcedureViewersConsts.CLASS_CALL_BLOCK_ERR);
        //block2ErrBlockEdge.addAttribute(ProcedureViewersConsts.SIZE,execCountNormalized);
        //block2ErrBlockEdge.addAttribute(COLOR,execAvgNormalized);

        block2ErrBlockEdge.addAttribute(ProcedureViewersConsts.EXEC_STATS,blockErrorStats);

    }

    private static void block2ProcSetAttributes(Edge block2ProcEdge, ExecutionStats stackStats, Limits limits) {
        if (isDeb() && isVerb()) System.out.println(String.format("%-25s:\t\tSet '%s' Block2Proc edge Attributes.", "PopulateGraphPopulator", block2ProcEdge.getId()));

        double execCountNormalized = stackStats.ExecCount() / (double)limits.maxProcCount;

        block2ProcEdge.addAttribute(ProcedureViewersConsts.CLASS, ProcedureViewersConsts.CLASS_CALL_STACK);
        block2ProcEdge.addAttribute(ProcedureViewersConsts.SIZE,execCountNormalized);
        //block2ProcEdge.addAttribute(COLOR,execAvgNormalized);

        block2ProcEdge.addAttribute(ProcedureViewersConsts.EXEC_STATS,stackStats);
    }

    private static void block2BlockSetAttributes(Edge block2BlockEdge, ExecutionStats seqStats, Limits limits, String color) {
        if (isDeb() && isVerb()) System.out.println(String.format("%-25s:\t\tSet '%s' Block2Block edge Attributes.", "PopulateGraphPopulator", block2BlockEdge.getId()));

        double execCountNormalized = seqStats.ExecCount() / (double)limits.maxProcCount;
        //double execAvgNormalized = seqStats.ExecDurationAvg() / limits.maxProcAvg;

        block2BlockEdge.addAttribute(ProcedureViewersConsts.CLASS, ProcedureViewersConsts.CLASS_CALL_SEQ);
        block2BlockEdge.addAttribute(ProcedureViewersConsts.SIZE,execCountNormalized);

        block2BlockEdge.addAttribute(ProcedureViewersConsts.EXEC_STATS,seqStats);

        block2BlockEdge.addAttribute(ProcedureViewersConsts.COLOR,color);
        block2BlockEdge.addAttribute(ProcedureViewersConsts.STYLE,"fill-color: " + color + ";");
    }

    private static void block2BlockUpdAttributes(Edge block2BlockEdge, ExecutionStats seqStats, @SuppressWarnings("unused") Limits limits) {
        if (isDeb() && isVerb()) System.out.println(String.format("%-25s:\t\tUpdate '%s' Block2Block edge Attributes.", "PopulateGraphPopulator", block2BlockEdge.getId()));

        ExecutionStats oldExecStats = block2BlockEdge.getAttribute(ProcedureViewersConsts.EXEC_STATS);
        oldExecStats.add(seqStats);

        block2BlockEdge.setAttribute(ProcedureViewersConsts.EXEC_STATS, oldExecStats);
    }

}
