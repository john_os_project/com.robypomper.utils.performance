package com.robypomper.utils.performance.viewer.nodes;


import com.robypomper.utils.performance.viewer.ProcedureViewersConsts;
import org.graphstream.graph.Node;

import java.util.ArrayList;
import java.util.List;

@SuppressWarnings({"WeakerAccess", "unused"})
public class NodeError extends Base {

    // -----------------------
    // Static instances access
    // -----------------------

    private static List<NodeError> nodes = new ArrayList<>();

    public static NodeError get(Node nodeError) {
        // search
        for (NodeError error : nodes)
            if (error.MainNode==nodeError)
                return error;

        // or create
        try {
            NodeError error = new NodeError(nodeError);
            nodes.add(error);
            return error;
        } catch (IllegalArgumentException ignore) {
            return null;
        }
    }


    // -----------
    // Constructor
    // -----------

    private NodeError(Node error) {
        super(error);
        if (!isError(error)) throw new IllegalArgumentException("Node is not a Error nodes.");
    }

    public static boolean isError(Node n) {
        return n!=null && n.hasLabel(ProcedureViewersConsts.CLASS) &&
                (n.getLabel(ProcedureViewersConsts.CLASS).equals(ProcedureViewersConsts.CLASS_ERR_PROC) ||
                        n.getLabel(ProcedureViewersConsts.CLASS).equals(ProcedureViewersConsts.CLASS_ERR_BLOCK));
    }


    // ----------
    // Get callers
    // ----------

    public List<Base> getCallers() { // out
        List<Base> res = new ArrayList<>();

        List<NodeProcedure> procs = cast(getInNode(ProcedureViewersConsts.CLASS, ProcedureViewersConsts.CLASS_PROC),NodeProcedure.class);
        if (procs!=null) res.addAll(procs);
        List<NodeBlock> blocks = cast(getInNode(ProcedureViewersConsts.CLASS, ProcedureViewersConsts.CLASS_BLOCK),NodeBlock.class);
        if (blocks!=null) res.addAll(blocks);

        return res;
    }

}

