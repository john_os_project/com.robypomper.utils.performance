package com.robypomper.utils.performance.viewer;

@SuppressWarnings({"WeakerAccess"})
public class ProcedureViewersConsts {

    // Graph & generic attributes
    public final static String STYLESHEET = "ui.stylesheet";
    public final static String STYLESHEET_OLD = "stylesheet";
    public final static String SCREENSHOTS = "ui.screenshot";
    public final static String QUALITY = "ui.quality";
    public final static String ANTIALIAS = "ui.antialias";

    // Nodes & Edge attributes
    public final static String CLASS = "ui.class";
    public final static String LABEL = "ui.label";
    public final static String SIZE = "ui.size";
    public final static String COLOR = "ui.color";
    public final static String STYLE = "ui.style";

    // Execution Stats attributes
    public final static String EXEC_STATS = "exec.stats";
    public final static String EXEC_INFO = "exec.info";
    public final static String EXEC_INFO_SR = EXEC_INFO + ".samplerate";
    public final static String EXEC_INFO_START = EXEC_INFO + ".starttime";
    public final static String EXEC_INFO_END = EXEC_INFO + ".endtime";
    public final static String EXEC_INFO_START_EFFECTIVE = EXEC_INFO + ".starttime_effective";
    public final static String EXEC_INFO_END_EFFECTIVE = EXEC_INFO + ".endtime_effective";
    public final static String EXEC_INFO_FILE = EXEC_INFO + ".filename";

    // Nodes and edges classes
    public final static String CLASS_PROC = "Procedure";
    public final static String CLASS_BLOCK = "Block";
    public final static String CLASS_GHOST = "Ghost";
    public final static String CLASS_CALL_STACK = "Stack_Call";
    public final static String CLASS_CALL_SEQ = "BlockSeq_Call";
    public final static String CLASS_CALL_PROC_ERR = "ProcedureError_Call";
    public final static String CLASS_CALL_BLOCK_ERR = "BlockError_Call";
    public final static String CLASS_ERR_PROC = "ProcedureError";
    public final static String CLASS_ERR_BLOCK = "BlockError";
    public final static String CLASS_INFO = "Info";

    // Procedures and block's nodes sizes
    public final static int ProcSize_X = 2;
    public final static int ProcSize_Y = 1;
    public final static int ProcSize_Z = 0;
    public final static int BlockSize_X = 2;
    public final static int BlockSize_Y = 1;
    public final static int BlockSize_Z = 0;
}
