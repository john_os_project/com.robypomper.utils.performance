package com.robypomper.utils.performance.viewer;

import com.robypomper.utils.history.SampleRate;
import com.robypomper.utils.performance.analyzer.ProceduresCollection;
import org.graphstream.graph.Graph;
import org.graphstream.graph.implementations.SingleGraph;
import org.graphstream.stream.file.FileSinkImages;
import org.graphstream.ui.layout.Layout;
import org.graphstream.ui.swingViewer.ViewPanel;
import org.graphstream.ui.view.Viewer;

import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;


public class ProceduresGraph {

    // ------
    // Fields
    // ------

    /** Procedures representation in GraphStream object. */
    private final Graph graph;
    /** Graph visualizer object. */
    @SuppressWarnings("FieldCanBeLocal")
    private final Viewer graphViewer;
    /** Java Graph's view. */
    private final ViewPanel graphView;
    public ViewPanel getView() {
        return graphView;
    }
    /** Graph visualizer's layout. */
    @SuppressWarnings("FieldCanBeLocal")
    private final Layout graphLayout;
    /** Data do display */
    @SuppressWarnings("FieldCanBeLocal")
    private ProceduresCollection procedures = null;


    // ----------
    // Properties
    // ----------

    /** If true, this class prints warning messages. */
    private boolean isWarnEnabled = true;
    @SuppressWarnings("WeakerAccess")
    public boolean getEnableWarning() { return isWarnEnabled; }
    @SuppressWarnings("WeakerAccess")
    protected boolean isWarn() { return getEnableWarning(); }
    @SuppressWarnings({"WeakerAccess", "RedundantSuppression"})
    public void setEnableWarning(boolean warnEnabled) {
        this.isWarnEnabled  = warnEnabled ;
        ProceduresGraphPopulator.setEnableWarning(warnEnabled);
        if (graphLayout instanceof ProceduresLayout)
            ((ProceduresLayout)graphLayout).setEnableWarning(warnEnabled);
    }
    /** If true, this class prints logs messages. */
    private boolean isLogEnabled = false;
    @SuppressWarnings("WeakerAccess")
    public boolean getEnableLog() { return isLogEnabled; }
    @SuppressWarnings({"WeakerAccess", "RedundantSuppression", "unused"})
    protected boolean isLog() { return getEnableLog(); }
    @SuppressWarnings({"WeakerAccess", "RedundantSuppression"})
    public void setEnableLog(boolean logEnabled) {
        this.isLogEnabled  = logEnabled ;
        ProceduresGraphPopulator.setEnableLog(logEnabled);
        if (graphLayout instanceof ProceduresLayout)
            ((ProceduresLayout)graphLayout).setEnableLog(logEnabled);
    }
    /** If true, this class prints debug messages. */
    private boolean isDebugEnabled = true;
    @SuppressWarnings("WeakerAccess")
    public boolean getEnableDebug() { return isDebugEnabled; }
    @SuppressWarnings("WeakerAccess")
    protected boolean isDeb() { return getEnableDebug(); }
    @SuppressWarnings({"WeakerAccess", "RedundantSuppression"})
    public void setEnableDebug(boolean debEnabled) {
        this.isDebugEnabled  = debEnabled ;
        ProceduresGraphPopulator.setEnableDebug(debEnabled);
        if (graphLayout instanceof ProceduresLayout)
            ((ProceduresLayout)graphLayout).setEnableDebug(debEnabled);
    }


    // -----------
    // Constructor
    // -----------

    public ProceduresGraph(Class<? extends Layout> layoutClass, Path cssPath) {
        // Setup environment for GraphStream
        System.setProperty("org.graphstream.ui.renderer", "org.graphstream.ui.j2dviewer.J2DGraphRenderer");
        System.setProperty("org.graphstream.ui.stylesheet", ProceduresGraph.parseCss(cssPath));

        // Disable default GraphStream logger
        Logger LOGGER = Logger.getLogger(FileSinkImages.class.getName());
        LOGGER.setFilter(record -> false);

        // Setup procedure's graph
        graph = new SingleGraph("Procedures");
        ProceduresGraphPopulator.graphSetAttributes(graph,cssPath);

        // Setup graph's layout
        graphLayout = loadLayout(layoutClass,graph);
        if (graphLayout instanceof ProceduresLayout) {
            ((ProceduresLayout) graphLayout).setEnableLog(getEnableLog());
            ((ProceduresLayout) graphLayout).setEnableWarning(getEnableWarning());
            ((ProceduresLayout)graphLayout).setEnableDebug(getEnableDebug());
        }

        // Setup graph's viewer and view
        graphViewer = new Viewer(graph, Viewer.ThreadingModel.GRAPH_IN_ANOTHER_THREAD);
        graphViewer.enableAutoLayout(graphLayout);
        graphView = graphViewer.addDefaultView(false);
    }


    // ---------
    // Data mngm
    // ---------

    public void setProcedures(ProceduresCollection procs) {
        if (procs==null) return;

        // Empty graph
        Map<String,Object> attrs = new HashMap<>();
        for (String key : graph.getAttributeKeySet())
            attrs.put(key,graph.getAttribute(key));

        graph.clear();

        for (Map.Entry<String,Object> entry : attrs.entrySet())
            graph.addAttribute(entry.getKey(),entry.getValue());

        procedures = procs;
        ProceduresGraphPopulator.populateGraph(graph,procedures);
        ((ProceduresLayout)graphLayout).setChanged();

        if (isDeb()) System.out.println(String.format("%-25s:\tSelected proceduresCollection '%s@%s'.", "PerformanceGraph", procs.ProcsSampleRate.name(), SampleRate.toFormattedDate(procs.ProcsSampleRate.getStart(procs.getTime()))));
    }

    public void updatesProcedure(ProceduresCollection procs) {
        if (procs==null) return;
        
        procedures = procs;
        ProceduresGraphPopulator.updateGraph(graph,procedures);
        ((ProceduresLayout)graphLayout).setChanged();

        if (isDeb()) System.out.println(String.format("%-25s:\tUpdated proceduresCollection '%s@%s'.", "PerformanceGraph", procs.ProcsSampleRate.name(), SampleRate.toFormattedDate(procs.ProcsSampleRate.getStart(procs.getTime()))));
    }

    public void storeGraph(String fileName) {
        graph.removeAttribute(ProcedureViewersConsts.SCREENSHOTS);
        graph.addAttribute(ProcedureViewersConsts.SCREENSHOTS,fileName);
        // ToDo implements storeGraph

//        FileSinkImages pic = new FileSinkImages(FileSinkImages.OutputType.PNG, FileSinkImages.Resolutions.VGA);
//        pic.setLayoutPolicy(FileSinkImages.LayoutPolicy.COMPUTED_ONCE_AT_NEW_IMAGE);
//        pic.setStyleSheet(graph.getLabel(ProcedureViewersConsts.STYLESHEET).toString());
//
//        try {
//            pic.writeAll(graph, fileName);
//        } catch (IOException e) {
//            e.printStackTrace();
//        }

        if (isWarn())  System.out.println(String.format("%-25s:\t------ procedure 'storeGraph()' not implemented.","PerformanceGraph"));
        if (isDeb()) System.out.println(String.format("%-25s:\tGraph '%s@%s' stored on file.", "PerformanceGraph", procedures.ProcsSampleRate.name(), SampleRate.toFormattedDate(procedures.ProcsSampleRate.getStart(procedures.getTime()))));
    }

    // -----
    // Utils
    // -----

    @SuppressWarnings("WeakerAccess")
    static Layout loadLayout(Class<? extends Layout> layoutClass, Graph graph) {
        try {
            Constructor constructor = layoutClass.getConstructor(Graph.class);
            return (Layout) constructor.newInstance(graph);
        } catch (NoSuchMethodException|InstantiationException|IllegalAccessException| InvocationTargetException e) {
            return new ProceduresLayout(graph);
        }
    }

    static String parseCss(Path cssPath) {
        if (cssPath==null) return "";

        String css = "";
        try {
            css = new String(Files.readAllBytes(cssPath));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return css;
    }

}
