package com.robypomper.utils.performance.viewer;

import org.graphstream.stream.PipeBase;
import org.graphstream.ui.geom.Point3;
import org.graphstream.ui.layout.Layout;

public class BaseLayout extends PipeBase implements Layout {
    // Layout fields

    @Override
    public String getLayoutAlgorithmName() {
        return this.getClass().getSimpleName();
    }

    @Override
    public int getNodeMovedCount() {
        throw new UnsupportedOperationException();
    }

    @Override
    public double getStabilization() {
        // Not used
        return 0;
        //return 1 - nodeMoved / (double) internalGraph.getNodeCount();
    }
    @Override
    public void setStabilizationLimit(double value) {
        throw new UnsupportedOperationException();
    }
    @Override
    public double getStabilizationLimit() {
        // Not used
        return 1;
    }

    @Override
    public Point3 getLowPoint() {
        throw new UnsupportedOperationException();
    }
    @Override
    public Point3 getHiPoint() {
        throw new UnsupportedOperationException();
    }

    @Override
    public int getSteps() {
        throw new UnsupportedOperationException();
    }
    @Override
    public long getLastStepTime() {
        throw new UnsupportedOperationException();
    }

    @Override
    public void setQuality(double qualityLevel) {
        throw new UnsupportedOperationException();
    }
    @Override
    public double getQuality() {
        throw new UnsupportedOperationException();
    }

    @Override
    public void setForce(double value) {
        throw new UnsupportedOperationException();
    }
    @Override
    public double getForce() {
        throw new UnsupportedOperationException();
    }

    @Override
    public void setSendNodeInfos(boolean send) {
        throw new UnsupportedOperationException();
    }


    @Override
    public void clear() {
        throw new UnsupportedOperationException();
    }

    @Override
    public void compute() {
        throw new UnsupportedOperationException();
    }

    @Override
    public void shake() {
        throw new UnsupportedOperationException();
    }

    @Override
    public void moveNode(String id, double x, double y, double z) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void freezeNode(String id, boolean frozen) {
        throw new UnsupportedOperationException();
    }
}
