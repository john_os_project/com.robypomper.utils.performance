package com.robypomper.utils.performance.viewer.nodes;


import com.robypomper.utils.performance.viewer.ProcedureViewersConsts;
import org.graphstream.graph.Node;

import java.util.ArrayList;
import java.util.List;

@SuppressWarnings("WeakerAccess")
public class NodeGhost extends Base {

    // -----------------------
    // Static instances access
    // -----------------------

    private static List<NodeGhost> nodes = new ArrayList<>();

    public static NodeGhost get(Node nodeGhost) {
        // search
        for (NodeGhost ghost : nodes)
            if (ghost.MainNode==nodeGhost)
                return ghost;

        // or create
        try {
            NodeGhost ghost = new NodeGhost(nodeGhost);
            nodes.add(ghost);
            return ghost;
        } catch (IllegalArgumentException ignore) {
            return null;
        }
    }


    // -----------
    // Constructor
    // -----------

    private NodeGhost(Node ghost) {
        super(ghost);
        if (!isGhost(ghost)) throw new IllegalArgumentException("Node is not a Ghost nodes.");
    }

    public static boolean isGhost(Node n) {
        return n!=null && n.hasLabel(ProcedureViewersConsts.CLASS) && n.getLabel(ProcedureViewersConsts.CLASS).equals(ProcedureViewersConsts.CLASS_GHOST);
    }


    // ------------------------
    // Get neighbors procedures
    // ------------------------

    public Node getProcedure() {
        if (getInNode(ProcedureViewersConsts.CLASS,ProcedureViewersConsts.CLASS_GHOST).size()==1)
            return getInNode(ProcedureViewersConsts.CLASS,ProcedureViewersConsts.CLASS_GHOST).get(0);
        return null;
    }

}
