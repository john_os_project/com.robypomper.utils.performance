package com.robypomper.utils.performance.viewer.nodes;


import com.robypomper.utils.performance.viewer.ProcedureViewersConsts;
import org.graphstream.graph.Node;

import java.util.ArrayList;
import java.util.List;

@SuppressWarnings("WeakerAccess")
public class NodeInfo extends Base {

    // -----------------------
    // Static instances access
    // -----------------------

    private static List<NodeInfo> nodes = new ArrayList<>();

    public static NodeInfo get(Node nodeInfo) {
        // search
        for (NodeInfo info : nodes)
            if (info.MainNode==nodeInfo)
                return info;

        // or create
        try {
            NodeInfo info = new NodeInfo(nodeInfo);
            nodes.add(info);
            return info;
        } catch (IllegalArgumentException ignore) {
            return null;
        }
    }


    // -----------
    // Constructor
    // -----------

    private NodeInfo(Node info) {
        super(info);
        if (!isInfo(info)) throw new IllegalArgumentException("Node is not a Info nodes.");
    }

    public static boolean isInfo(Node n) {
        return n!=null && n.hasLabel(ProcedureViewersConsts.CLASS) && n.getLabel(ProcedureViewersConsts.CLASS).equals(ProcedureViewersConsts.CLASS_INFO);
    }


    // ------------------------
    // Get neighbors procedures
    // ------------------------

    public Node getProcedure() {
        if (getInNode(ProcedureViewersConsts.CLASS, ProcedureViewersConsts.CLASS_INFO).size()==1)
            return getInNode(ProcedureViewersConsts.CLASS,ProcedureViewersConsts.CLASS_INFO).get(0);
        return null;
    }

}
