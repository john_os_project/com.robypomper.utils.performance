package com.robypomper.utils.performance.viewer.nodes;


import com.robypomper.utils.performance.viewer.ProcedureViewersConsts;
import javafx.util.Pair;
import org.graphstream.graph.Edge;
import org.graphstream.graph.Node;
import org.graphstream.ui.geom.Point3;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;


@SuppressWarnings({"WeakerAccess", "SameParameterValue", "unused"})
public class Base {

    // ------
    // Fields
    // ------

    public final Node MainNode;


    // -----------
    // Constructor
    // -----------

    Base(Node mainNode) {
        this.MainNode = mainNode;
    }

    public String toString() {
        return this.MainNode.getId();
    }

    public Pair<Point3,Point3> render(Point3 offset) {
        Point3 offsetStart = new Point3(offset.x,offset.y,offset.z);
        Point3 offsetEnd = new Point3(offset.x,offset.y,offset.z);

        MainNode.setAttribute("x",offset.x + (MainNode.getLabel(ProcedureViewersConsts.LABEL).length()/2));
        MainNode.setAttribute("y",-offset.y);
        MainNode.setAttribute("z",offset.z);

        offsetEnd.x += MainNode.getLabel(ProcedureViewersConsts.LABEL).length();
        offsetEnd.y += 5;
        offsetEnd.z += 1;

        return new Pair<>(offsetStart,offsetEnd);
    }

    // ---------------------
    // Connected nodes utils
    // ---------------------

    protected List<Node> getOutNode(String key, String value) {
        List<Node> res = new ArrayList<>();

        for (Edge e : MainNode.getLeavingEdgeSet()) {
            Node opposite = e.getOpposite(MainNode);
            if (!opposite.hasLabel(key) || opposite.getLabel(key).equals(value))
                res.add(opposite);
        }

        return res;
    }

    protected List<Node> getInNode(String key, String value) {
        List<Node> res = new ArrayList<>();

        for (Edge e : MainNode.getEnteringEdgeSet()) {
            Node opposite = e.getOpposite(MainNode);
            if (!opposite.hasLabel(key) || opposite.getLabel(key).equals(value))
                res.add(opposite);
        }

        return res;
    }

    protected static <T extends Base> List<T> cast(List<Node> nodeSubs, Class<T> clazz) {
        List<T> res = new ArrayList<>();

        Method method;
        try {
            method = clazz.getMethod("get", Node.class);
        } catch (IllegalArgumentException | SecurityException | NoSuchMethodException e) {
            e.printStackTrace();
            return null;
        }

        for (Node n : nodeSubs)
            try {
                @SuppressWarnings("unchecked") T sub = (T)method.invoke(null,n);
                res.add(sub);
            } catch (IllegalArgumentException | IllegalAccessException | InvocationTargetException | SecurityException e) {
                e.printStackTrace();
                return null;
            }

        return res;
    }

    public static <T extends Base> T getMax(String attribute, List<T> nodes) {
        T max = null;
        double maxVal = Double.MIN_VALUE;

        for (T b : nodes)
            if (b.MainNode.hasNumber(attribute) && b.MainNode.getNumber(attribute) > maxVal) {
                max = b;
                maxVal = b.MainNode.getNumber(attribute);
            }

        return max;
    }

    public static <T extends Base> T getMin(String attribute, List<T> nodes) {
        T min = null;
        double minVal = Double.MAX_VALUE;

        for (T b : nodes)
            if (b.MainNode.hasNumber(attribute) && b.MainNode.getNumber(attribute) < minVal) {
                min = b;
                minVal = b.MainNode.getNumber(attribute);
            }

        return min;
    }


    // ---------------------
    // Connected edges utils
    // ---------------------

    protected List<Edge> getOutEdges(String key, String value) {
        List<Edge> res = new ArrayList<>();

        for (Edge e : MainNode.getLeavingEdgeSet())
            if (!e.hasLabel(key) || e.getLabel(key).equals(value))
                res.add(e);

        return res;
    }

    protected List<Edge> getInEdges(String key, String value) {
        List<Edge> res = new ArrayList<>();

        for (Edge e : MainNode.getEnteringEdgeSet())
            if (!e.hasLabel(key) || e.getLabel(key).equals(value))
                res.add(e);

        return res;
    }

    protected static <T extends Base> List<Edge> getEdgesBetween(List<T> fromNodes, Base toNode) {
        List<Edge> res = new ArrayList<>();

        for (Base b : fromNodes)
            for (Edge edge : b.MainNode.getLeavingEdgeSet())
                if (edge.getOpposite(b.MainNode) == toNode.MainNode)
                    res.add(edge);

        return res;
    }

    protected static <T extends Base> List<Edge> getEdgesBetween(Base fromNode, List<T> toNodes) {
        List<Edge> res = new ArrayList<>();

        for (Base b : toNodes)
            for (Edge edge : b.MainNode.getEnteringEdgeSet())
                if (edge.getOpposite(b.MainNode) == fromNode.MainNode)
                    res.add(edge);

        return res;
    }

    protected static <T extends Base> List<Edge> getEdgesBetween(List<T> fromNodes, List<T> toNodes) {
        List<Edge> res = new ArrayList<>();

        for (Base b : fromNodes)
            for (Edge edge : b.MainNode.getLeavingEdgeSet())
                for (Base bTo : toNodes)
                    if (edge.getOpposite(b.MainNode) == bTo.MainNode)
                        res.add(edge);

        return res;
    }

    public static Edge getMaxEdges(String attribute, List<Edge> edges) {
        Edge max = null;
        double maxVal = Double.MIN_VALUE;

        for (Edge edge : edges)
            if (edge.hasNumber(attribute) && edge.getNumber(attribute) > maxVal) {
                max = edge;
                maxVal = edge.getNumber(attribute);
            }

        return max;
    }

    public static Edge getMinEdges(String attribute, List<Edge> edges) {
        Edge min = null;
        double minVal = Double.MAX_VALUE;

        for (Edge edge : edges)
            if (edge.hasNumber(attribute) && edge.getNumber(attribute) < minVal) {
                min = edge;
                minVal = edge.getNumber(attribute);
            }

        return min;
    }
}
