package com.robypomper.utils.performance.viewer.boxes;


import com.robypomper.utils.performance.viewer.nodes.NodeBlock;
import com.robypomper.utils.performance.viewer.nodes.NodeError;
import com.robypomper.utils.performance.viewer.nodes.NodeProcedure;
import javafx.util.Pair;
import org.graphstream.graph.Node;
import org.graphstream.ui.geom.Point3;

import java.util.ArrayList;
import java.util.List;

public class BoxProc implements Box {

    // -----------------------
    // Static instances access
    // -----------------------

    private static List<BoxProc> nodes = new ArrayList<>();

    public static BoxProc get(NodeProcedure proc) {
        // search
        for (BoxProc boxProc : nodes)
            if (boxProc.Proc==proc)
                return boxProc;

        // or create
        BoxProc boxProc = new BoxProc(proc);
        nodes.add(boxProc);
        return boxProc;
    }


    // ------
    // Fields
    // ------

    public NodeProcedure Proc;


    // -----------
    // Constructor
    // -----------

    private BoxProc(NodeProcedure proc) {
        this.Proc = proc;
    }

    /**
     * {@inheritDoc}
     */
    public Pair<Point3,Point3> render(Point3 offset) {
        Point3 offsetStart = new Point3(offset.x,offset.y,offset.z);
        Point3 offsetEnd = new Point3(offset.x,offset.y,offset.z);

        List<NodeBlock> blockRoots = getBlocksRoots();
        List<NodeProcedure> subProcs = getSubProcs(blockRoots);


        Point3 offset_1stCol = new Point3(offset.x,offset.y,offset.z);
        Point3 offset_2stCol = new Point3(offset.x,offset.y,offset.z);
        Point3 offset_3thCol = new Point3(offset.x,offset.y,offset.z);


        Pair<Point3,Point3> size_1stCol = renderRootProc(Proc,offset_1stCol);


        offset_2stCol.x = size_1stCol.getValue().x;
        Pair<Point3,Point3> size_2ndCol = renderBlockRoots(blockRoots,offset_2stCol);


        offset_3thCol.x = size_2ndCol.getValue().x;
        Pair<Point3,Point3> size_3thCol = renderSubProc(subProcs,offset_3thCol);
        //Pair<Point3,Point3> size_3thCol = new Pair(offset_3thCol,offset_3thCol);


        offsetEnd.x = size_3thCol.getValue().x;
        offsetEnd.y = Math.max(size_2ndCol.getValue().y,size_3thCol.getValue().y);
        offsetEnd.z = size_3thCol.getValue().z;

        return new Pair<>(offsetStart,offsetEnd);
    }


    // ---------------
    // Order functions
    // ---------------

    private List<NodeProcedure> getSubProcs(List<NodeBlock> blockRoots) {
        List<NodeProcedure> subProcNodes = new ArrayList<>();

        for (NodeBlock block : blockRoots) {
            subProcNodes.addAll(block.getProceduresCalled());
            subProcNodes.addAll(getSubProcs(block.getNextBlockSeq(true)));
        }

        return subProcNodes;
    }

    private List<NodeBlock> getBlocksRoots() {
        List<NodeBlock> blockRoots = new ArrayList<>();

        // Get procedure's root blocks
        for (Node blockNode : Proc.getBlocks()) {
            NodeBlock block = NodeBlock.get(blockNode);
            if (block!=null && block.isRootSeq())
                blockRoots.add(block);
        }

        return blockRoots;
    }


    // ----------------
    // Render functions
    // ----------------

    private static boolean debRender = false;

    private static Pair<Point3,Point3> renderRootProc(NodeProcedure proc, Point3 offset) {
        if (debRender) System.out.println(String.format("renderRootProc          (%-20s,%s)", proc.MainNode.getId(),offset));

        Point3 offsetStart = new Point3(offset.x,offset.y,offset.z);
        Point3 offsetEnd = new Point3(offset.x,offset.y,offset.z);

        Pair<Point3, Point3> size =  renderProc(proc,offset);

        offsetEnd.set(size.getValue().x,size.getValue().y,size.getValue().z);

        if (debRender) System.out.println(String.format("renderRootProc          (%-20s,%s) -> %s", proc.MainNode.getId(),offset,offsetEnd));
        return new Pair<>(offsetStart,offsetEnd);
    }

    private static Pair<Point3, Point3> renderBlockRoots(List<NodeBlock> blockRoots, Point3 offset) {
        Point3 offsetStart = new Point3(offset.x,offset.y,offset.z);
        Point3 offsetEnd = new Point3(offset.x,offset.y,offset.z);

        for (NodeBlock block : blockRoots) {
            if (debRender) System.out.println(String.format("  renderBlockRoots      (%-20s,%s)", block.MainNode.getId(),offset));

            Pair<Point3, Point3> size = renderBlockandSub(block,offset,null);

            offset.y = size.getValue().y;

            offsetEnd.x = size.getValue().x;
            offsetEnd.y = size.getValue().y;
            offsetEnd.z = size.getValue().z;

            if (debRender) System.out.println(String.format("  renderBlockRoots      (%-20s,%s) -> %s", block.MainNode.getId(),offset,offsetEnd));
        }

        return new Pair<>(offsetStart,offsetEnd);
    }

    private static Pair<Point3, Point3> renderBlockandSub(NodeBlock block, Point3 offset, NodeBlock blockPrev) {
        if (debRender) System.out.println(String.format("    renderBlockandSub   (%-20s,%s)", block.MainNode.getId(),offset));

        Point3 offsetStart = new Point3(offset.x,offset.y,offset.z);
        Point3 offsetEnd = new Point3(offset.x,offset.y,offset.z);

        // if blockPrev is not main prev block
        if (block.getPrevBlockSeq_Main()!=blockPrev)
            return new Pair<>(offsetStart,offsetEnd);

        Point3 offsetBlock = new Point3(offset.x,offset.y,offset.z);
        //Pair<Point3,Point3> size = renderBlock(block,offsetBlock);    // render only block
        Pair<Point3,Point3> size = renderBlockWithErrors(block,offsetBlock);
        offsetEnd.set(size.getValue().x,size.getValue().y,size.getValue().z);

        Point3 offsetSubError = new Point3(offset.x,size.getValue().y,offset.z);
        for (NodeError subError : block.getNextErrorSeq()) {
            size = renderBlockError(subError,offsetSubError);
            offsetEnd.set(Math.max(offsetEnd.x,size.getValue().x),size.getValue().y,size.getValue().z);

            offsetSubError.x = size.getValue().x;
        }

        Point3 offsetSubBlock = new Point3(offset.x,size.getValue().y,offset.z);
        for (NodeBlock subBlock : block.getNextBlockSeq()) {
            size = renderBlockandSub(subBlock,offsetSubBlock,block);
            offsetEnd.set(Math.max(offsetEnd.x,size.getValue().x),size.getValue().y,size.getValue().z);

            offsetSubBlock.x = size.getValue().x;
        }

        if (debRender) System.out.println(String.format("    renderBlockandSub   (%-20s,%s) -> %s", block.MainNode.getId(),offset,offsetEnd));
        return new Pair<>(offsetStart,offsetEnd);
    }

    private static Pair<Point3, Point3> renderBlockWithErrors(NodeBlock block, Point3 offset) {
        if (debRender) System.out.println(String.format("      renderBlockWithErr(%-20s,%s)", block.MainNode.getId(),offset));

        Point3 offsetStart = new Point3(offset.x,offset.y,offset.z);
        Point3 offsetEnd = new Point3(offset.x,offset.y,offset.z);

        Point3 offsetBlock = new Point3(offset.x,offset.y,offset.z);
        Pair<Point3,Point3> size = renderBlock(block,offsetBlock);
        offsetEnd.set(size.getValue().x,size.getValue().y,size.getValue().z);


        Point3 offsetError = new Point3(size.getValue().x,offset.y,offset.z);
        for (NodeError error : block.getErrors()) {
            size = renderBlockError(error,offsetError);
            offsetEnd.set(size.getValue().x,Math.max(offsetEnd.y,size.getValue().y),size.getValue().z);

            offsetError.y = size.getValue().y;
        }

        if (debRender) System.out.println(String.format("      renderBlockWithErr(%-20s,%s) -> %s", block.MainNode.getId(),offset,offsetEnd));
        return new Pair<>(offsetStart,offsetEnd);
    }

    private static Pair<Point3, Point3> renderSubProc(List<NodeProcedure> subProcs, Point3 offset) {
        Point3 offsetStart = new Point3(offset.x,offset.y,offset.z);
        Point3 offsetEnd = new Point3(offset.x,offset.y,offset.z);

        for (NodeProcedure proc : subProcs) {
            if (debRender) System.out.println(String.format("      renderSubProc     (%-20s,%s)", proc.MainNode.getId(),offset));
            BoxProc subBoxProc = BoxProc.get(proc);

            Pair<Point3, Point3> size = subBoxProc.render(offset);

            offset.y = size.getValue().y;

            offsetEnd.x = Math.max(offsetEnd.x,size.getValue().x);
            offsetEnd.y = size.getValue().y;

            if (debRender) System.out.println(String.format("      renderSubProc     (%-20s,%s) -> %s", proc.MainNode.getId(),offset,offsetEnd));
        }

        return new Pair<>(offsetStart,offsetEnd);
    }


    // --------------------
    // Render elements base
    // --------------------

    private static Pair<Point3,Point3> renderProc(NodeProcedure proc, Point3 offset) {
        return proc.render(offset);
    }

    private static Pair<Point3, Point3> renderBlock(NodeBlock block, Point3 offset) {
        return block.render(offset);
    }

    private static Pair<Point3, Point3> renderBlockError(NodeError error, Point3 offset) {
        return error.render(offset);
    }

}
