package com.robypomper.utils.performance.viewer.boxes;

import javafx.util.Pair;
import org.graphstream.ui.geom.Point3;

public interface Box {
    Pair<Point3,Point3> render(Point3 offset);
}
