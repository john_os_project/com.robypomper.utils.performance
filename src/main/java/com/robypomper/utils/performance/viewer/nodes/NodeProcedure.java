package com.robypomper.utils.performance.viewer.nodes;


import com.robypomper.utils.performance.viewer.ProcedureViewersConsts;
import org.graphstream.graph.Node;

import java.util.ArrayList;
import java.util.List;

@SuppressWarnings({"unused"})
public class NodeProcedure extends Base {

    // -----------------------
    // Static instances access
    // -----------------------

    private static List<NodeProcedure> nodes = new ArrayList<>();

    public static NodeProcedure get(Node nodeProc) {
        // search
        for (NodeProcedure proc : nodes)
            if (proc.MainNode==nodeProc)
                return proc;

        // or create
        try {
            NodeProcedure proc = new NodeProcedure(nodeProc);
            nodes.add(proc);
            return proc;
        } catch (IllegalArgumentException ignore) {
            return null;
        }
    }


    // -----------
    // Constructor
    // -----------

    private NodeProcedure(Node proc) {
        super(proc);
        if (!isProcedure(proc)) throw new IllegalArgumentException("Node is not a Procedure nodes.");
    }

    public static boolean isProcedure(Node n) {
        return n!=null && n.hasLabel(ProcedureViewersConsts.CLASS) && n.getLabel(ProcedureViewersConsts.CLASS).equals(ProcedureViewersConsts.CLASS_PROC);
    }


    // ----------
    // Get errors
    // ----------

    public List<NodeError> getErrors() { // out
        return cast(getOutNode(ProcedureViewersConsts.CLASS, ProcedureViewersConsts.CLASS_ERR_PROC),NodeError.class);
    }


    // ------------------------
    // Get neighbors procedures
    // ------------------------

    public boolean isRoot() {
        return getBlockCallers().size()==0;
    }

    public List<Node> getGhosts() {
        return getInNode(ProcedureViewersConsts.CLASS,ProcedureViewersConsts.CLASS_GHOST);
    }

    // --------------------
    // Get neighbors blocks
    // --------------------

    public List<Node> getBlocks() {
        return getOutNode(ProcedureViewersConsts.CLASS,ProcedureViewersConsts.CLASS_BLOCK);
    }

    private List<Node> getBlockCallers() {  // in
        return getInNode(ProcedureViewersConsts.CLASS,ProcedureViewersConsts.CLASS_BLOCK);
    }

}
