package com.robypomper.utils.performance.viewer.boxes;


import com.robypomper.utils.performance.viewer.nodes.NodeGhost;
import javafx.util.Pair;
import org.graphstream.ui.geom.Point3;

import java.util.ArrayList;
import java.util.List;


@SuppressWarnings("WeakerAccess")
public class BoxGhost implements Box {

    // -----------------------
    // Static instances access
    // -----------------------

    private static List<BoxGhost> nodes = new ArrayList<>();

    public static BoxGhost get(NodeGhost ghost) {
        // search
        for (BoxGhost boxGhost : nodes)
            if (boxGhost.Ghost==ghost)
                return boxGhost;

        // or create
        BoxGhost boxGhost = new BoxGhost(ghost);
        nodes.add(boxGhost);
        return boxGhost;
    }


    // ------
    // Fields
    // ------

    public NodeGhost Ghost;


    // -----------
    // Constructor
    // -----------

    public BoxGhost(NodeGhost ghost) {
        this.Ghost = ghost;
    }

    /**
     * {@inheritDoc}
     */
    public Pair<Point3,Point3> render(Point3 offset) {
        return renderGhost(Ghost,offset);
    }


    // --------------------
    // Render elements base
    // --------------------

    private static Pair<Point3,Point3> renderGhost(NodeGhost ghost, Point3 offset) {
        return ghost.render(offset);
    }

}
