package com.robypomper.utils.performance.annotations.aspect;

import com.robypomper.utils.performance.PerformanceCollector;
import com.robypomper.utils.performance.annotations.block.EndBlock;
import com.robypomper.utils.performance.annotations.block.StartBlock;
import com.robypomper.utils.performance.annotations.block.TrackBlock;
import com.robypomper.utils.performance.annotations.execution.EndExecution;
import com.robypomper.utils.performance.annotations.execution.StartExecution;
import com.robypomper.utils.performance.annotations.execution.TrackExecution;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.*;

/**
 * Performance Aspect Class.
 *
 * It's possibly load this class inside a weaver to replace annotated method with a proxy class (AOP)
 *
 *
 * Handled annotation:
 * ------------------------------ Execution ------------------------------
 * - @StartExecution
 *   {
 *      PerformanceCollector.registerNewExecution(executionName);
 *      executeAnnotatedMethod();
 *   }
 *
 * - @EndExecution
 *   {
 *      try {
 *          executeAnnotatedMethod();
 *          PerformanceCollector.terminateExecution();
 *      } catch(Exception e) {
 *          PerformanceCollector.terminateExecution(e);
 *          throw(e)
 *      }
 *   }
 *
 * - @TrackExecution
 *   {
 *      PerformanceCollector.registerNewExecution(executionName);
 *      try {
 *          executeAnnotatedMethod();
 *          PerformanceCollector.terminateExecution();
 *      } catch(Exception e) {
 *          PerformanceCollector.terminateExecution(e);
 *          throw(e)
 *      }
 *   }
 *
 * -------------------------------- Block --------------------------------
 * - @StartBlock
 *   {
*       PerformanceCollector.startRecording(blockName);
 *      executeAnnotatedMethod();
 *   }
 *
 * - @EndBlock
 *   {
 *      try {
 *          executeAnnotatedMethod();
 *          PerformanceCollector.endRecording(blockName);
 *      } catch(Exception e) {
 *          PerformanceCollector.endRecording(blockName, e);
 *          throw(e)
 *      }
 *   }
 *
 * - @TrackBlock
 *   {
 *      PerformanceCollector.startRecording(blockName);
 *      try {
 *          executeAnnotatedMethod();
 *          PerformanceCollector.endRecording(blockName);
 *      } catch(Exception e) {
 *          PerformanceCollector.endRecording(blockName, e);
 *          throw(e)
 *      }
 *   }
 * -----------------------------------------------------------------------
 *
 * @since 0.3
 */

@SuppressWarnings("unused")
@Aspect
public class PerformanceAspect {

    public enum PerformanceNameType {
        DEFAULT,
        CUSTOM,
        METHOD_SIGNATURE,
        METHOD_PARAMS_VALUES,
        METHOD_SIGN_AND_VALUES,
    }

    private final static String Pointcuts = "(execution(* * (..)) || execution(*.new(..)) || call(abstract * * (..)))";

    // --------------------------------
    // Annotations interceptor methods
    // --------------------------------

    // ---------
    // Execution
    // ---------

    /**
     * Register a new execution with the name getted from the annotation or using the default one.
     * Handles @StartExecution.
     *
     * @param point where exactly to apply advice
     * @param startExecution {@link StartExecution} annotation handled
     */
    @Before(Pointcuts+" && @annotation(startExecution)")
    public void startExecution(final JoinPoint point, StartExecution startExecution) {
        trackExecutionStart(getIdentity(point, startExecution.nameType(), startExecution.value()));
    }

    /**
     * Register an execution end.
     * Handles @EndExecution.
     *
     * @param point where exactly to apply advice
     * @param endExecution {@link EndExecution} annotation handled
     * @param t throw object with error occured
     */
    @AfterThrowing(pointcut = Pointcuts+" && @annotation(endExecution)", throwing = "t")
    public void endExecutionException(final JoinPoint point, EndExecution endExecution, Throwable t) {
        trackExecutionEndWithError(new Exception(t));
    }

    /**
     * Register an execution end.
     * Handles @EndExecution.
     *
     * @param point where exactly to apply advice
     * @param endExecution {@link EndExecution} annotation handled
     */
    @After(Pointcuts+" && @annotation(endExecution)")
    public void endExecution(final JoinPoint point, EndExecution endExecution) {
        trackExecutionEnd();
    }

    /**
     * Register a new execution with the name getted from the annotation or using the default one.
     * Handles @TrackExecution.
     *
     * @param point where exactly to apply advice
     * @param trackExecution {@link TrackExecution} annotation handled
     */
    @Before(Pointcuts+" && @annotation(trackExecution)")
    public void startExecution(final JoinPoint point, TrackExecution trackExecution) {
        trackExecutionStart(getIdentity(point,trackExecution.nameType(),trackExecution.value()));
    }

    /**
     * Register an execution end.
     * Handles @TrackExecution.
     *
     * @param point where exactly to apply advice
     * @param trackExecution {@link TrackExecution} annotation handled
     * @param t throw object with error occured
     */
    @AfterThrowing(pointcut = Pointcuts+" && @annotation(trackExecution)", throwing = "t")
    public void endExecutionException(final JoinPoint point, TrackExecution trackExecution, Throwable t) {
        trackExecutionEndWithError(new Exception(t));
    }

    /**
     * Register an execution end.
     * Handles @EndExecution.
     *
     * @param point where exactly to apply advice
     * @param trackExecution {@link TrackExecution} annotation handled
     */
    @AfterReturning(Pointcuts+" && @annotation(trackExecution)")
    public void endExecution(final JoinPoint point, TrackExecution trackExecution) {
        trackExecutionEnd();
    }

    // -----
    // Block
    // -----

    /**
     * Start a new block recording with the name getted from the annotation or using the default one.
     * Handles @StartBlock.
     *
     * @param point where exactly to apply advice
     * @param startBlock {@link StartBlock} annotation handled
     */
    @Before(Pointcuts+" && @annotation(startBlock)")
    public void startBlock(final JoinPoint point, StartBlock startBlock) {
        trackBlockStart(getIdentity(point,startBlock.nameType(),startBlock.value()));
    }

    /**
     * End a block recording with the name getted from the annotation or using the default one.
     * Handles @EndBlock.
     *
     * @param point where exactly to apply advice
     * @param endBlock {@link EndBlock} annotation handled
     * @param t throw object with error occured
     */
    @AfterThrowing(pointcut = Pointcuts+" && @annotation(endBlock)", throwing = "t")
    public void endBlockException(final JoinPoint point, EndBlock endBlock, Throwable t) {
        trackBlockEndWithError(getIdentity(point,endBlock.nameType(),endBlock.value()), new Exception(t));
    }

    /**
     * End a block recording with the name getted from the annotation or using the default one.
     * Handles @EndBlock.
     *
     * @param point where exactly to apply advice
     * @param endBlock {@link EndBlock} annotation handled
     */
    @After(Pointcuts+" && @annotation(endBlock)")
    public void endBlock(final JoinPoint point, EndBlock endBlock) {
        trackBlockEnd(getIdentity(point,endBlock.nameType(),endBlock.value()));
    }

    /**
     * Start a new block recording with the name getted from the annotation or using the default one.
     * Handles @TrackBlock.
     *
     * @param point where exactly to apply advice
     * @param trackBlock {@link TrackBlock} annotation handled
     */
    @Before(Pointcuts+" && @annotation(trackBlock)")
    public void startBlock(final JoinPoint point, TrackBlock trackBlock) {
        trackBlockStart(getIdentity(point,trackBlock.nameType(),trackBlock.value()));
    }

    /**
     * End a block recording with the name getted from the annotation or using the default one.
     * Handles @TrackBlock.
     *
     * @param point where exactly to apply advice
     * @param trackBlock {@link TrackBlock} annotation handled
     * @param t throw object with error occured
     */
    @AfterThrowing(pointcut = Pointcuts+" && @annotation(trackBlock)", throwing = "t")
    public void endBlockException(final JoinPoint point, TrackBlock trackBlock, Throwable t) {
        trackBlockEndWithError(getIdentity(point,trackBlock.nameType(),trackBlock.value()), new Exception(t));
    }

    /**
     * End a block recording with the name getted from the annotation or using the default one.
     * Handles @TrackBlock.
     *
     * @param point where exactly to apply advice
     * @param trackBlock {@link TrackBlock} annotation handled
     */
    @AfterReturning(Pointcuts+" && @annotation(trackBlock)")
    public void endBlock(final JoinPoint point, TrackBlock trackBlock) {
        trackBlockEnd(getIdentity(point,trackBlock.nameType(),trackBlock.value()));
    }

    // -----
    // Utils
    // -----

    /**
     * Get the name to use to track.
     *
     * @param joinPoint the pointcut
     * @param annotationIdentity the annotationIdentity type
     * @return name value. If nameValue not specified return the identity name by PerformanceNameType.
     */
    private String getIdentity(JoinPoint joinPoint, PerformanceNameType annotationIdentity, String nameValue){
        String identity;
        if(!nameValue.equals(""))
            annotationIdentity = PerformanceNameType.CUSTOM;
        switch (annotationIdentity){
            case CUSTOM: identity = nameValue; break;
            case METHOD_SIGNATURE: identity = PerformanceNameCreator.methodSign(joinPoint.getSignature()); break;
            case METHOD_PARAMS_VALUES: identity = PerformanceNameCreator.methodParamsValues(joinPoint); break;
            case METHOD_SIGN_AND_VALUES: identity = PerformanceNameCreator.methodSignAndValues(joinPoint); break;
            default: identity = PerformanceNameCreator.methodName(joinPoint.getSignature()); break;

        }
        return identity;
    }


    // ----------------
    // Performance Core
    // ----------------

    /**
     * Register a new execution.
     *
     * @param executionName the name of the new traced execution
     */
    private void trackExecutionStart(String executionName){
        PerformanceCollector.registerNewExecution(executionName);
    }

    /**
     * End track of current execution.
     *
     * @param e the error occurred
     */
    private void trackExecutionEndWithError(Exception e){
        PerformanceCollector.terminateExecution(e);
    }

    /**
     * End track of current execution.
     */
    private void trackExecutionEnd(){
        PerformanceCollector.terminateExecution();
    }

    /**
     * Register a new block.
     *
     * @param blockName the name of the new traced block
     */
    private void trackBlockStart(String blockName){
        PerformanceCollector.startRecording(blockName);
    }

    /**
     * End track of a block.
     *
     * @param blockName the name of the block of which the tracking ends
     * @param e the error occurred
     */
    private void trackBlockEndWithError(String blockName, Exception e){
        PerformanceCollector.endRecording(blockName, e);
    }

    /**
     * End track of a block.
     *
     * @param blockName the name of the block of which the tracking ends
     */
    private void trackBlockEnd(String blockName){
        PerformanceCollector.endRecording(blockName);
    }

}