package com.robypomper.utils.performance.annotations.block;

import com.robypomper.utils.performance.annotations.aspect.PerformanceAspect;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD,ElementType.CONSTRUCTOR})
public @interface TrackBlock {
    PerformanceAspect.PerformanceNameType nameType() default PerformanceAspect.PerformanceNameType.DEFAULT;
    String value() default "";}
