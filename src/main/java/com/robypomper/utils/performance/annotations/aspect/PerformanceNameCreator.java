package com.robypomper.utils.performance.annotations.aspect;

import com.robypomper.utils.performance.Performance;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.reflect.MethodSignature;

import java.lang.reflect.Parameter;

class PerformanceNameCreator {

    private final static String DELIMITER = "::";

    ////////////////////////////////////////////
    // Identity Creator method (uses by Aspect)
    ////////////////////////////////////////////

    static String methodName(final Signature signature){
        if (Performance.isAnnotationsNamesFullPath())
            return getClassName(signature) + DELIMITER + getMethodName(signature);
        else
            return getMethodName(signature);
    }

    static String methodSign(final Signature signature){
        return methodName(signature)+"("+getMethodArgsTypeList(signature)+")";
    }

    static String methodParamsValues(final JoinPoint joinPoint){
        return methodName(joinPoint.getSignature())+"("+getMethodArgsValueList(joinPoint)+")";
    }

    static String methodSignAndValues(final JoinPoint joinPoint){
        return methodName(joinPoint.getSignature())+"("+getMethodArgsTypeAndValueList(joinPoint)+")";
    }

    ///////////
    // Utils
    ///////////

    private static String getClassName(final Signature signature){
        return signature.getDeclaringTypeName();
    }

    private static String getMethodName(final Signature signature){
        return signature.getName();
    }

    private static String getMethodArgsTypeList(final Signature signature){
        MethodSignature methodSignature = (MethodSignature) signature;
        StringBuilder argsTypes = new StringBuilder();
        for(int i=0; i<methodSignature.getMethod().getParameterCount(); i++){
            Parameter param = methodSignature.getMethod().getParameters()[i];
            argsTypes.append(param.getType().getName());
            if(i<methodSignature.getMethod().getParameterCount()-1)
                argsTypes.append(", ");
        }
        return argsTypes.toString();

    }

    private static String getMethodArgsValueList(final JoinPoint joinPoint){
        MethodSignature methodSignature = (MethodSignature) joinPoint.getSignature();
        StringBuilder argsValues = new StringBuilder();
        Object[] args = joinPoint.getArgs();
        for(int i=0; i<args.length; i++){
            argsValues.append(args[i]);
            if(i<methodSignature.getMethod().getParameterCount()-1)
                argsValues.append(", ");
        }
        return argsValues.toString();
    }

    private static String getMethodArgsTypeAndValueList(final JoinPoint joinPoint){
        MethodSignature methodSignature = (MethodSignature) joinPoint.getSignature();
        StringBuilder argsTypeValues = new StringBuilder();
        Object[] args = joinPoint.getArgs();
        for(int i=0; i<methodSignature.getMethod().getParameterCount(); i++){
            Parameter param = methodSignature.getMethod().getParameters()[i];
            argsTypeValues.append(param.getType().getName()).append(DELIMITER).append(args[i]);
            if(i<methodSignature.getMethod().getParameterCount()-1)
                argsTypeValues.append(", ");
        }
        return argsTypeValues.toString();
    }
}
