package com.robypomper.utils.performance.analyzer;

/**
 * Execution statistics's class.
 *
 * This class store info about executions stats like count, total duration,
 * min and max duration and the avg duration.
 *
 * This class is used to store and elaborate stats about execution duration.
 * To add and process new execution it's enough call the register(long duration)
 * method. Then the execution's duration passed as parameter is processed and
 * added to stats.
 *
 * @since 0.2
 */
@SuppressWarnings({"WeakerAccess", "unused"})
public class ExecutionStats {

    // ------
    // Fields
    // ------

    private int execCount = 0;
    /**
     * @return Number of registered executions.
     */
    public int ExecCount() { return execCount; };
    private long execDurationSum = 0;
    /**
     * @return Lower execution duration registered.
     */
    public long ExecDurationMin() { return execDurationSum; }
    private long execDurationMin = Long.MAX_VALUE;
    /**
     * @return Higher execution duration registered.
     */
    public long ExecDurationMax() { return execDurationMin; }
    private long execDurationMax = 0;
    /**
     * @return Sum of all execution duration registered.
     */
    public long ExecDurationSum() { return execDurationMax; }
    private double execDurationAvg = 0;
    /**
     * @return Avg of all execution duration registered.
     */
    public double ExecDurationAvg() { return execDurationAvg; }


    // -----------
    // Constructor
    // -----------

    /**
     * Create new Execution Stats with all values to 0 (except DurationMin set
     * as Long.MAX_VALUE.
     */
    public ExecutionStats() {};

    /**
     * Create new Execution Stats with one executionr registered.
     *
     * @param duration duration of execution to register.
     */
    public ExecutionStats(long duration) {
        register(duration);
    }

    /**
     * Copy constructor.
     *
     * @param other ExecutionStats to copy.
     */
    public ExecutionStats(ExecutionStats other) {
        execCount = other.execCount;
        execDurationSum = other.execDurationSum;
        execDurationMin = other.execDurationMin;
        execDurationMax = other.execDurationMax;
        execDurationAvg = other.execDurationAvg;
    }


    // -------
    // Actions
    // -------

    /**
     * Update current ExecutionStats with the passed duration time.
     *
     * @param duration the execution's duration time.
     */
    public void register(long duration) {
        execCount++;
        execDurationSum += duration;
        if (execDurationMin>=duration) execDurationMin = duration;
        if (execDurationMax<=duration) execDurationMax = duration;
        execDurationAvg = execDurationSum / (double)execCount;
    }

    /**
     * Update current ExecutionStats with the passed ExecutionStats.
     *
     * @param other the execution's stats to add.
     */
    public void add(ExecutionStats other) {
        execCount += other.execCount;
        execDurationSum += other.execDurationSum;
        if (execDurationMin>=other.execDurationMin) execDurationMin = other.execDurationMin;
        if (execDurationMax<=other.execDurationMax) execDurationMax = other.execDurationMax;
        execDurationAvg = execDurationSum / (double)execCount;
    }

}
