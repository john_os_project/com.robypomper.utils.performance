package com.robypomper.utils.performance.analyzer;

import com.robypomper.utils.performance.PerformanceCollector;
import com.robypomper.utils.performance.collector.Execution;
import com.robypomper.utils.performance.collector.ExecutionBlock;
import com.robypomper.utils.performance.collector.ExecutionUnit;

import java.util.*;

/**
 * Performance representation.
 *
 * This class store info about all Procedure's execution: execution times, total
 * execution duration and execution duration avg.
 *
 * ToDo: implement analyzeBlocksPaths
 *
 * @since 0.1
 */
@SuppressWarnings({"WeakerAccess", "unused"})
public class Procedure {

    // ------
    // Fields
    // ------

    /**
     * Procedure name and Id.
     */
    public final String ProcName;
    /**
     * Procedure executions's stats based on registered Executions.
     * @since 0.2
     */
    public ExecutionStats Stats = new ExecutionStats();
    private Map<String,ExecutionStats> blocksStats = new TreeMap<>();
    /**
     * @return Procedure executions's stats by ExecutionBlocks.
     * @since 0.2
     */
    public Map<String,ExecutionStats> Blocks() { return blocksStats; }
    private Map<String,ExecutionStats> stacksStats = new TreeMap<>();
    /**
     * @return Procedure executions's stats by Procedure's Stack.
     * @since 0.2
     */
    public Map<String,ExecutionStats> Stacks() { return stacksStats; }
    private Map<String,ExecutionStats> blockSequencesStats = new TreeMap<>();
    /**
     * @return Procedure executions's stats by ExecutionBlock Sequences.
     * @since 0.2
     */
    public Map<String,ExecutionStats> BlockSequences() { return blockSequencesStats; }
    private Map<String,ExecutionStats> errorStats = new HashMap<>();
    /**
     * @return Procedure executions's stats by Execution's Errors.
     * @since 0.2
     */
    public Map<String,ExecutionStats> Errors() { return errorStats; }
    private Map<String,Map<String,ExecutionStats>> errorBlockStats = new HashMap<>();
    /**
     * @return Procedure executions's stats by Execution's Blocks / Errors.
     * @since 0.2
     */
    public Map<String,Map<String,ExecutionStats>> BlocksErrors() { return errorBlockStats; }


    // -----------
    // Constructor
    // -----------

    Procedure(String name) {
        ProcName = name;
    }


    // -------
    // Actions
    // -------

    /**
     * Add execution to current procedure.
     *
     * This method analyze the execution passed and update Procedure Execution
     * stats.
     *
     * @param execution Execution to add to the procedure.
     */
    public void addExecution(Execution execution) {
        Stats.register(execution.Stats.ExecDurationSum());
        analyzeStack(execution,execution.Stack());
        analyzeBlocks(execution,execution.BlocksOrderedByStart());
        analyzeBlocksPaths(execution.BlocksOrderedByStart());
        if (execution.Error()!=null)
            analyzeError(execution);
    }

    private void analyzeStack(Execution execution, Stack<ExecutionUnit> stack) {
        String stackPath = PerformanceCollector.printStack(stack,false);
        ExecutionStats stackStats = stacksStats.get(stackPath);
        if (stackStats==null) stacksStats.put(stackPath,new ExecutionStats(execution.Stats.ExecDurationSum()));
        else stackStats.register(execution.Stats.ExecDurationSum());
    }

    private void analyzeBlocks(Execution execution, List<ExecutionBlock> blocks) {
        for (ExecutionBlock block : blocks) {

            ExecutionStats stackStats = blocksStats.get(block.BlockName);
            if (stackStats == null) {
                stackStats = new ExecutionStats(block.Duration());
                blocksStats.put(block.BlockName, stackStats);
            }
            else stackStats.register(block.Duration());

            if (block.Error()!=null)
                analyzeBlockError(block);
        }

        // Block sequence
        String stackPath = ExecutionBlock.printBlocks(blocks);
        if (execution.Error()!=null)
            stackPath += " \\ " + execution.Error();
        ExecutionStats stackStats = blockSequencesStats.get(stackPath);
        if (stackStats==null) blockSequencesStats.put(stackPath,new ExecutionStats(execution.Stats.ExecDurationSum()));
        else stackStats.register(execution.Stats.ExecDurationSum());
    }

    private void analyzeError(Execution execution) {
        ExecutionStats errStats = errorStats.get(execution.Error().toString());
        if (errStats==null)
            errStats = new ExecutionStats(execution.EndTime().getTime() - execution.StartTime().getTime());
        else
            errStats.register(execution.EndTime().getTime()-execution.StartTime().getTime());
        errorStats.put(execution.Error().toString(),errStats);
    }

    private void analyzeBlockError(ExecutionBlock block) {
        String keyBlock = block.BlockName;
        String keyError = block.Error().toString();

        Map<String,ExecutionStats> errsBlockStats = errorBlockStats.get(keyBlock);
        if (errsBlockStats==null) {
            errorBlockStats.put(keyBlock,new TreeMap<>());
            errsBlockStats = errorBlockStats.get(keyBlock);
        }
        ExecutionStats errBlockStats = errsBlockStats.get(keyError);
        if (errsBlockStats.get(keyError)==null)
            errsBlockStats.put(keyError,new ExecutionStats(block.Duration()));
        else
            errsBlockStats.get(keyError).register(block.Duration());
    }

    private void analyzeBlocksPaths(List<ExecutionBlock> blocks) {
        // ...
    }

    private void analyseErrorBlocks(Execution execution, List<ExecutionBlock> blocks){
        // ...
    }

}
