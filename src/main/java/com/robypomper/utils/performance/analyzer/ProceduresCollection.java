package com.robypomper.utils.performance.analyzer;

import com.robypomper.utils.performance.collector.Execution;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.robypomper.utils.history.SampleRate;
import com.robypomper.utils.history.Timed;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.sql.Timestamp;
import java.util.Map;
import java.util.TreeMap;

/**
 * Collection of Procedures with Same SampleRate and StartTime.
 *
 * ToDo: document public methods
 * ToDo: manage exception on Store and Load resources methods
 *
 * @since 0.1
 */
@SuppressWarnings({"WeakerAccess", "unused"})
public class ProceduresCollection implements Timed {

    // ------
    // Fields
    // ------

    /**
     * @return procedure Collection Id.
     */
    public String ProceduresCollectionId() {
        return String.format("%s@%s", ProcsSampleRate, SampleRate.toFormattedDate(ProcsSampleRate.getStart(StartTime().getTime())));
    }
    /**
     * Procedure's collection file name is composed by SampleRate and the
     * Procedure Collection's StartTime.
     * @return give the Procedure Collection file name.
     */
    public String ProceduresFilename() {
        return String.format("%s@%s", ProcsSampleRate, SampleRate.toFilenameDate(ProcsSampleRate.getStart(StartTime().getTime())));
    }
    /**
     * Procedure Collection's SampleRate.
     */
    public final SampleRate ProcsSampleRate;
    /**
     * Timed interface Implementation.
     * {@inheritDoc}
     */
    public long getTime() {
        return StartTime().getTime();
    }
    private Timestamp startTime;
    /**
     * Executions start time.
     *
     * @return the StartTime of the first (StartTime ordered) execution, or null if
     *         no blocks has registered.
     */
    public Timestamp StartTime() { return startTime; }
    private Timestamp endTime;
    /**
     * Executions end time.
     *
     * @return the EndTime of the last (EndTime ordered) execution, or null if
     *         no blocks has registered.
     */
    public Timestamp EndTime() { return endTime; }
    private final Map<String,Procedure> procedures;
    /**
     * @return get all Procedures
     * @since 0.2
     */
    public Map<String,Procedure> Procedures() {
        return procedures;
    }
    /**
     * @param procName required procedure's name.
     * @return get Procedure by name, or null if no procedure with given name.
     * @since 0.2
     */
    public Procedure get(String procName) {
        return procedures.get(procName);
    }

    // -----------
    // Constructor
    // -----------

    public ProceduresCollection(SampleRate sampleRate) {
        ProcsSampleRate = sampleRate;
        startTime = null;
        endTime = null;
        procedures = new TreeMap<>();
    }

    public String toString() {
        return String.format("%s (%s > %s) # procs: %d", ProceduresCollectionId(),StartTime(),EndTime(),procedures.size());
    }


    // -------
    // Storage
    // -------

    public static ProceduresCollection loadJson(Path procPath) {
        try {
            String fileStr = new String(Files.readAllBytes(procPath));
            Gson gson = new GsonBuilder()
                    .enableComplexMapKeySerialization()
                    .setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ")
                    .create();
            return gson.fromJson(fileStr, ProceduresCollection.class);

        } catch (IOException|ClassCastException  e) {
            e.printStackTrace();
            return null;
        }
    }

    public void storeJson(Path procPath) {
        try {
            Gson gson = new GsonBuilder().setPrettyPrinting().setDateFormat("yyyy-MM-dd'T'HH:mm:ssZ").create();
            String procsJson = gson.toJson(this);
            Files.write(procPath, procsJson.getBytes());

        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    // -------
    // Actions
    // -------

    public void addExecution(String procName, Execution execution) {
        if (startTime==null) startTime = execution.StartTime();
        if (endTime==null) endTime = execution.EndTime();

        // if PerformanceAnalyzer::addProcedures(Map<String, Execution>) do it correctly following asserts will pass
        assert ProcsSampleRate.getStart(startTime.getTime()) <= execution.StartTime().getTime(): "Can't add execution '%s', because it's StartTime is earlier than that of Procedure Collection.";
        assert ProcsSampleRate.getEnd(startTime.getTime()) >= execution.StartTime().getTime(): "Can't add execution '%s', because it's StartTime is later than the EndTime of Procedure Collection.";

        if (startTime.getTime()>execution.StartTime().getTime()) startTime = execution.StartTime();
        if (endTime.getTime()<execution.EndTime().getTime()) endTime = execution.EndTime();

        Procedure proc = procedures.get(procName);
        if (proc==null) {
            proc = new Procedure(procName);
            proc.addExecution(execution);
            procedures.put(procName,proc);
        } else
            proc.addExecution(execution);
    }

}
