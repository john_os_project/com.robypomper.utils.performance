package com.robypomper.utils.performance.checker;


import com.robypomper.utils.history.History;
import com.robypomper.utils.history.SampleRate;
import com.robypomper.utils.performance.analyzer.Procedure;
import com.robypomper.utils.performance.analyzer.ProceduresCollection;
import com.robypomper.utils.performance.collector.Execution;

/**
 * Simple Execution Checker.
 *
 * This class checks execution times comparing it with the avg duration time
 * of the same procedure in the previous registered sample rate (see
 * DEF_SAMPLERATE and History).
 * If the current execution duration is less or more than THRESHOLD_OK (%) of the
 * avg duration, then the check result will be ExecutionCheckResult.Error,
 * otherwise the check result will be ExecutionCheckResult.Ok.
 *
 * If there's no previous sample rate execution registered, then the current
 * registered executions will be used to get the avg duration. This mean the
 * same executions that are being checked, provide also the avg duration time.
 *
 * Both: the THRESHOLD_OK and DEF_SAMPLERATE, are fixed as local private fields;
 * respectively with the values 15 (%) and SampleRate.Minute.
 *
 * @since 0.2
 */
public class SimpleChecker extends ExecutionCheck {

    private static final double THRESHOLD_OK = 15;
    private static final SampleRate DEF_SAMPLERATE = SampleRate.Minute;

    // -----------
    // Constructor
    // -----------

    /** {@inheritDoc} */
    public SimpleChecker(Execution execution, History<ProceduresCollection> procedures) {
        super(execution,procedures);
    }


    // ------
    // Action
    // ------

    /** {@inheritDoc} */
    @Override
    protected void check(Execution execution, History<ProceduresCollection> procedures) {
        Procedure proc = null;

        if (procedures.getPrev(DEF_SAMPLERATE)!=null)
            proc = procedures.getPrev(DEF_SAMPLERATE).get(execution.ProcName);
        if (proc == null && procedures.getCurrent(DEF_SAMPLERATE)!=null)
            proc = procedures.getCurrent(DEF_SAMPLERATE).get(execution.ProcName);
        if (proc == null) {
            checkResult = ExecutionCheckResult.NotChecked;
            return;
        }

        double procDurationAvg = proc.Stats.ExecDurationAvg();
        long execDuration = execution.Stats.ExecDurationSum();

        double minOk = 1 - 100 / THRESHOLD_OK;
        double maxOk = 1 + 100 / THRESHOLD_OK;

        if ( execDuration > procDurationAvg * minOk
                && execDuration < procDurationAvg * maxOk)
            checkResult = ExecutionCheckResult.Ok;
        else
            checkResult = ExecutionCheckResult.Error;
    }
}
