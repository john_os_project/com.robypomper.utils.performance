package com.robypomper.utils.performance.checker;

import com.robypomper.utils.history.History;
import com.robypomper.utils.performance.analyzer.ProceduresCollection;
import com.robypomper.utils.performance.collector.Execution;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;

/**
 * Class to store execution check's results.
 *
 * ToDo: manage exception on Store and Load resources methods
 *
 * @since 0.1
 */
@SuppressWarnings({"WeakerAccess", "unused"})
public abstract class ExecutionCheck {

    /** Possible check results. */
    public enum ExecutionCheckResult { NotChecked, Ok, Warning, Error }

    // ------
    // Fields
    // ------

    /**
     * Procedure name of checked Execution.
     * @since 0.2
     */
    public final String ProcName;
    /**
     * Id of checked Execution.
     * @since 0.2
     */
    public final String ExecutionId;
    protected ExecutionCheckResult checkResult;
    /**
     * @since 0.2
     * @return check outcome.
     */
    public ExecutionCheckResult CheckResult() { return checkResult; }


    // -----------
    // Constructor
    // -----------

    /**
     * ExecutionCheck constructor and check executor.
     *
     * @since 0.2
     * @param execution execution to check.
     * @param procedures stored procedures stats.
     */
    public ExecutionCheck(Execution execution, History<ProceduresCollection> procedures) {
        ProcName = execution.ProcName;
        ExecutionId = execution.ExecutionId;
        try {
            check(execution, procedures);
        } catch (Throwable t) {
            System.out.println(t + t.getMessage());
            t.printStackTrace();
        }
    }


    // -------
    // Storage
    // -------

    /**
     * Store the ExecutionCheck list on json file.
     *
     * @since 0.2
     * @param checkedExecutions Executions Checks Results to store.
     * @param execStorePath path of the json file to save.
     */
    public static void store(List<ExecutionCheck> checkedExecutions, Path execStorePath) {
        try {
            Gson gson = new GsonBuilder().setPrettyPrinting().setDateFormat("yyyy-MM-dd'T'HH:mm:ssZ").create();
            String procsJson = gson.toJson(checkedExecutions);
            Files.write(execStorePath, procsJson.getBytes());

        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    // ------
    // Action
    // ------

    /**
     * Abstract method called for Execution check.
     *
     * Subclass must implements this method depending on Check Logic provided.
     * The protected checkResult field is used to store the check's result.
     *
     * @since 0.2
     * @param execution Execution to check
     * @param procedures Performance's Procedures stats to use for comparison checks.
     */
    protected abstract void check(Execution execution, History<ProceduresCollection> procedures);

}
