package com.robypomper.utils.performance;

import com.robypomper.utils.performance.analyzer.Procedure;
import com.robypomper.utils.performance.analyzer.ProceduresCollection;
import com.robypomper.utils.performance.checker.ExecutionCheck;
import com.robypomper.utils.performance.collector.Execution;
import com.robypomper.utils.history.History;
import com.robypomper.utils.history.SampleRate;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Collection of Performance Analyzer functions and scheduler main task.
 *
 * Method mainTask() can be called by Performance scheduleder or
 * Performance::executeAnalyzer().
 *
 * @since 0.1
 */
class PerformanceAnalyzer extends PerformanceCollector {

    private static History<ProceduresCollection> procedures = null;
    static History<ProceduresCollection> getProcedures() { return procedures; }


    // ----------
    // Main tasks
    // ----------

    /**
     * Main Analyser's task:
     * - empty PerformanceCollector
     * - update Procedures with latest executions
     * - check latest execution and store results
     * - store updated Procedures
     *
     * This task can be executed only by AnalyzerTimer or executeAnalyzer method.
     */
    static void mainTask() {
        if (procedures==null)
            procedures = loadProcedures();

        // Take latest executions from collector (thats empty it)
        Map<String, Execution> executionsFromCollector = PerformanceCollector.take();

        // Update procedures with latest executions
        List<ProceduresCollection> updatedProcs = addProcedures(executionsFromCollector);

        // Check latest executions
        List<ExecutionCheck> checkedExecutions = checkExecutions(executionsFromCollector);

        // Store latest executions
        storeLatestExecutions(checkedExecutions);

        // Store latest procedures (updates already stored procedures)
        storeUpdatedProcedures(updatedProcs);

        Performance.notifyAnalyticsExecution(executionsFromCollector,updatedProcs,checkedExecutions);
    }


    // ---------
    // Sub tasks
    // ---------

    /**
     * Load and return ProceduresCollection's History from files contained in the resource
     * Performance.getProcsLoadPaths().
     *
     * @return an History object contains all ProceduresCollection loaded.
     */
    private static History<ProceduresCollection> loadProcedures() {
        History<ProceduresCollection> loadedProcedureHistory = new History<>();

        List<Path> historyPaths = Performance.getProcsLoadPaths();
        for (Path p : historyPaths) {
            ProceduresCollection loadedProcs = ProceduresCollection.loadJson(p);
            if (loadedProcs!=null) loadedProcedureHistory.add(loadedProcs.ProcsSampleRate,loadedProcs);
        }

        return loadedProcedureHistory;
    }

    /**
     * Scan executions and generate/update ProceduresCollection to add to ProceduresCollection's History.
     *
     * @param executions map of executions to scan.
     * @return list of created or updated ProceduresCollection.
     */
    private static List<ProceduresCollection> addProcedures(Map<String, Execution> executions) {
        List<ProceduresCollection> updProcs = new ArrayList<>();

        for (Execution execution : executions.values()) {
            // ottieni tutti samplerate associabili all'esecuzione
            Map<SampleRate,Map<Long, ProceduresCollection>> execProcs = procedures.get(execution.StartTime().getTime(),true);

            // per ogni samplerate/procedures
            for (Map.Entry<SampleRate,Map<Long, ProceduresCollection>> entrySR : execProcs.entrySet()) {
                SampleRate sr = entrySR.getKey();
                ProceduresCollection proc = History.getFirstInSamples(entrySR.getValue());

                // already exist or create new one
                if (proc==null) {
                    // Create, add execution, add to procedures
                    proc = new ProceduresCollection(sr);
                    proc.addExecution(execution.ProcName,execution);
                    procedures.add(proc.ProcsSampleRate,proc);

                } else
                    // add execution
                    proc.addExecution(execution.ProcName,execution);

                updProcs.add(proc);
            }
        }
        return updProcs;
    }

    /**
     * Run check on executions and provide ExecutionChecks results.
     *
     * For each execution passed as parameter, this method initialize
     * corresponding checker (@see Performance::setChecker).
     *
     * @since 0.2
     * @param executions executions list to check.
     * @return executions checks results
     */
    private static List<ExecutionCheck> checkExecutions(Map<String, Execution> executions) {
        List<ExecutionCheck> checks = new ArrayList<>();

        for (Execution execution : executions.values()) {
            Class<? extends ExecutionCheck> checkClass = Performance.getChecker(execution.ProcName);
            try {
                Constructor<?> constructor = checkClass.getConstructor(Execution.class, History.class);
                checks.add((ExecutionCheck) constructor.newInstance(execution,procedures));
            } catch (NoSuchMethodException | InstantiationException | IllegalAccessException e) {
                assert false : String.format("Error on checkExecutions on src code inclusion of '%s' class: [%s] %s", checkClass.getName(), e, e.getMessage());
            } catch (InvocationTargetException e) {
                assert false : String.format("Error on checkExecutions on src code execution of '%s' class: [%s] %s", checkClass.getName(), e, e.getCause());
            }
        }
        return checks;
    }

    /**
     * Store checked executions on Performance.getExecStorePath(String) resource.
     *
     * @param checkedExecutions list of results about executions checks.
     */
    private static void storeLatestExecutions(List<ExecutionCheck> checkedExecutions) {
        String fileName = SampleRate.toFilenameDate(Performance.getMillisSince());
        Path filePath = Performance.getExecStorePath(fileName);
        ExecutionCheck.store(checkedExecutions,filePath);
    }

    /**
     * Store all procedures contained in updatedProcedures.
     *
     * If a procedures was already store, then it will be overwrited.
     *
     * @param updatedProcedures procedures to be stored.
     */
    private static void storeUpdatedProcedures(List<ProceduresCollection> updatedProcedures) {
        for (ProceduresCollection p : updatedProcedures) {
            Path storeProc = Performance.getProcsStorePath(p.ProceduresFilename());
            p.storeJson(storeProc);
        }
    }

    public static List<String> getMainProceduress(ProceduresCollection procedures) {
        List<String> mains = new ArrayList<>();
        for (Procedure proc : procedures.Procedures().values())
            for (String stack : proc.Stacks().keySet())
                if (stack.isEmpty() && !mains.contains(proc.ProcName))
                    mains.add(proc.ProcName);
        return mains;
    }
}
