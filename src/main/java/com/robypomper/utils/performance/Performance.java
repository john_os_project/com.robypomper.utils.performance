package com.robypomper.utils.performance;

import com.robypomper.utils.IOResources;
import com.robypomper.utils.history.SampleRate;
import com.robypomper.utils.performance.analyzer.ProceduresCollection;
import com.robypomper.utils.performance.checker.ExecutionCheck;
import com.robypomper.utils.performance.checker.SimpleChecker;
import com.robypomper.utils.performance.collector.Execution;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.NotDirectoryException;
import java.nio.file.Path;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Performance Main Class.
 *
 * With the aim of exposed methods by this class, developer can configure the
 * Performance framework. Developer can:
 * <ul>
 * <li>Performance Collector: enable/disable, set relative time</li>
 * <li>Performance Analyzer: start/startSync/stop/execute, load/store procedures resources
 *                           store execution checks, configurables checker classes</li>
 * <li>PerformanceViewer: enable/disable, configurable CSS path, store graphs resource</li>
 * <li>enable/disable Performance Viewer</li>
 * <li>Listeners: Analytics</li>
 * </ul>
 *
 * ToDo: document public methods
 * ToDo: manage exception on Store and Load resources methods
 *
 * @since 0.1
 */
@SuppressWarnings({"WeakerAccess", "unused"})
public class Performance {

    // -------
    // Generic
    // -------

    private static boolean annotationsNamesFullpath = false;
    public static void setAnnotationsNamesFullPath(boolean enable) {
        annotationsNamesFullpath = enable;
    }
    public static boolean isAnnotationsNamesFullPath() {
        return annotationsNamesFullpath;
    }


    // -------------------------------------
    // Performance Collector: enable/disable
    // -------------------------------------

    private static boolean enableCollector = true;
    private static Timestamp lastEnableCollectorUpd = new Timestamp(Performance.getMillisSince());

    public static boolean EnableCollector() { return enableCollector; }

    public static void enableCollector() { enableCollector = true; lastEnableCollectorUpd = new Timestamp(Performance.getMillisSince()); }

    public static void disableCollector() { enableCollector = false; lastEnableCollectorUpd = new Timestamp(Performance.getMillisSince()); }

    public static Timestamp LastEnabledCollectorUpd() { return lastEnableCollectorUpd; }


    // ----------------------------------------
    // Performance Collector: set relative time
    // ----------------------------------------

    private static long offset = 0;

    public static void setNewNow(String newNow) {
        setNewNow(newNow,"yyyy-MM-dd hh:mm:ss.SSS");
    }

    public static void setNewNow(String newNow, String dateFormat) {
        try {
            setNewNow(new SimpleDateFormat(dateFormat).parse(newNow));
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    public static void setNewNow(Date newNow) {
        setNewNow(newNow.getTime());
    }

    public static void setNewNow(Timestamp newNow) {
        setNewNow(newNow.getTime());
    }

    public static void setNewNow(long newNow) {
        offset = newNow-System.currentTimeMillis();
    }

    public static long getMillisSince() {
        return System.currentTimeMillis() + offset;
    }


    // ------------------------------------------
    // Performance Analyzer: load/store resources
    // ------------------------------------------

    private static Object procsLoadResource = "./.perf/procedures";
    private static Object procsStoreResource = procsLoadResource;

    public static void setProcsLoadResource(Object procsLoadResource) {
        Performance.procsLoadResource = procsLoadResource;
    }

    public static List<Path> getProcsLoadPaths() { return IOResources.resourceToInPaths(procsLoadResource); }

    public static List<ObjectInputStream> getProcsLoadObjStreams() { return IOResources.resourceToInObjStreams(procsLoadResource); }

    public static void setProcsStoreResource(Object procsStoreResource) {
        Performance.procsStoreResource = procsStoreResource;
    }

    public static Path getProcsStorePath(String id) {
        String execExt = ".ppr";
        if (!id.endsWith(execExt))
            id += execExt;
        try {
            // se non ce
            return IOResources.resourceToOutPath(procsStoreResource,id,false,true);
        } catch (FileAlreadyExistsException|NotDirectoryException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static ObjectOutputStream getProcsStoreObjStreams(String id) throws IOException {
        String execExt = ".ppr_ser";
        if (!id.endsWith(execExt))
            id += execExt;

        try {
            return IOResources.resourceToOutObjStream(procsStoreResource,id,false,true);
        } catch (FileAlreadyExistsException|NotDirectoryException e) {
            e.printStackTrace();
            return null;
        }
    }


    // --------------------------------------------
    // Performance Analyzer: store execution checks
    // --------------------------------------------

    private static Object execStoreResource = "./.perf/executions";

    public static void setExecStoreResource(Object execStoreResource) {
        Performance.execStoreResource = execStoreResource;
    }

    public static Path getExecStorePath(String id) {
        String execExt = ".pex";
        if (!id.endsWith(execExt))
            id += execExt;

        try {
            return IOResources.resourceToOutPath(execStoreResource,id,false,true);
        } catch (FileAlreadyExistsException|NotDirectoryException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static ObjectOutputStream getExecStoreObjStreams(String id) throws IOException {
        String execExt = ".pex_ser";
        if (!id.endsWith(execExt))
            id += execExt;

        try {
            return IOResources.resourceToOutObjStream(execStoreResource,id,false,true);
        } catch (FileAlreadyExistsException|NotDirectoryException e) {
            e.printStackTrace();
            return null;
        }
    }


    // --------------------------------------------------
    // Performance Analyzer: start/startSync/stop/execute
    // --------------------------------------------------

    private static int AnalyzerExecutionPeriod = 60;
    private static Timer analyzerTimer;
    private final static TimerTask analyzerTask = new TimerTask() {
        @Override
        public void run() {
            executeAnalyzer();
        }
    };

    public static void setAnalyzerExecutionPeriod(int analyzerExecutionPeriod) {
        AnalyzerExecutionPeriod = analyzerExecutionPeriod;
    }

    public static void executeAnalyzer() {
        PerformanceAnalyzer.mainTask();
    }

    public static void startAnalyzer() {
        analyzerTimer = new Timer(true);
        analyzerTimer.schedule(analyzerTask,0,AnalyzerExecutionPeriod*1000);
    }

    public static void startAnalyzerSync() {
        int delay = (int)(SampleRate.Minute.getCurrentEnd() - Performance.getMillisSince());
        int period = (int) SampleRate.Minute.getLength();
        analyzerTimer = new Timer(true);
        analyzerTimer.schedule(analyzerTask,delay,period);
    }

    public static void stopAnalyzer() {
        analyzerTimer.cancel();
        analyzerTimer = null;
        executeAnalyzer();
    }


    // ---------------------------------------------------
    // Performance Analyzer: configurables checker classes
    // ---------------------------------------------------

    private final static Class<? extends ExecutionCheck> CHECKER_DEFAULT_CALLS = SimpleChecker.class;
    private final static Map<String,Class<? extends ExecutionCheck>> checkerClassesByProc = new TreeMap<>();
    private static int executionCounter = 0;

    public static Class<? extends ExecutionCheck> getChecker(String procName) {
        Class<? extends ExecutionCheck> c = checkerClassesByProc.get(procName);
        if (c==null) return CHECKER_DEFAULT_CALLS;
        return c;
    }

    public static void setChecker(String procName, Class<? extends ExecutionCheck> checker) {
        checkerClassesByProc.put(procName,checker);
    }


    // ---------------------------------
    // PerformanceViewer: enable/disable
    // ---------------------------------

    private static Thread viewerThread = null;

    public static boolean EnabledViewer() { return viewerThread!=null; }

    public static void enableViewer() {
        if (viewerThread!=null) return;

        // See PerformanceViewer.CmdLineArgs
        List<String> argsStr = new ArrayList<>();
        argsStr.add ("--procLoadResource");
        argsStr.add (cmdParamsQuoted(procsStoreResource.toString()));
        argsStr.add ("--selectedTime");
        argsStr.add (cmdParamsQuoted(SampleRate.toFormattedDate(Performance.getMillisSince())));
        argsStr.add ("--selectedSampleRate");
        argsStr.add (SampleRate.Minute.name());
        argsStr.add ("--fullscreen");
        argsStr.add ("true");
        argsStr.add ("--updateTime");
        argsStr.add ("10");
        //argsStr.add ("--warn");
        //argsStr.add ("false");
        argsStr.add ("--log");
        argsStr.add ("true");
        //argsStr.add ("--verbose");
        //argsStr.add ("true");

        startViewer(argsStr.toArray(new String[0]));
    }

    public static void enableViewerSilent() {
        if (viewerThread != null) return;

        // See PerformanceViewer.CmdLineArgs
        List<String> argsStr = new ArrayList<>();
        argsStr.add ("--procLoadResource");
        argsStr.add (cmdParamsQuoted(procsStoreResource.toString()));
        argsStr.add ("--selectedTime");
        argsStr.add (cmdParamsQuoted(SampleRate.toFormattedDate(Performance.getMillisSince())));
        argsStr.add ("--selectedSampleRate");
        argsStr.add (SampleRate.Minute.name());
        //argsStr.add ("--fullscreen");
        //argsStr.add ("true");
        argsStr.add ("--updateTime");
        argsStr.add ("10");
        argsStr.add ("--warn");
        argsStr.add ("false");
        //argsStr.add ("--log");
        //argsStr.add ("true");
        //argsStr.add ("--verbose");
        //argsStr.add ("true");

        startViewer(argsStr.toArray(new String[0]));
    }

    public static void enableViewerRealtime() {
        if (viewerThread != null) return;

        // See PerformanceViewer.CmdLineArgs
        List<String> argsStr = new ArrayList<>();
        argsStr.add ("--procLoadResource");
        argsStr.add (cmdParamsQuoted(procsStoreResource.toString()));
        argsStr.add ("--selectedTime");
        argsStr.add (cmdParamsQuoted(SampleRate.toFormattedDate(Performance.getMillisSince())));
        argsStr.add ("--selectedSampleRate");
        argsStr.add (SampleRate.Minute.name());
        //argsStr.add ("--fullscreen");
        //argsStr.add ("true");
        argsStr.add ("--updateTime");
        argsStr.add ("1");
        //argsStr.add ("--warn");
        //argsStr.add ("false");
        //argsStr.add ("--log");
        //argsStr.add ("true");
        //argsStr.add ("--verbose");
        //argsStr.add ("true");

        startViewer(argsStr.toArray(new String[0]));
    }

    public static void disableViewer() {
        viewerThread.interrupt();
        viewerThread = null;
    }

    private static void startViewer(String[] args) {
        viewerThread = new Thread(() -> {
            try { PerformanceViewer.main(args);
            } catch (InterruptedException ignore) {}
        });
        viewerThread.start();
    }

    private static String cmdParamsQuoted(String original) {
        return String.format("\"%s\"", original.replace("\"", "\\\""));
        //return String.format("\'%s\'", original.replace("\'", "\'\"\'\"\'"));
    }


    // ----------------------------------------
    // PerformanceViewer: configurable CSS path
    // ----------------------------------------

    private static Object viewerCssLoadResource = "/com/robypomper/utils/performance/stylesheet.css";

    public static void setViewerCssLoadResource(Object viewerCssLoadResource) {
        Performance.viewerCssLoadResource = viewerCssLoadResource;
    }

    public static Path getViewerCssLoadPaths() {
        try {
            return IOResources.resourceToOutPath(viewerCssLoadResource,false);
        } catch (FileAlreadyExistsException e) {
            e.printStackTrace();
            return null;
        }
    }


    // ----------------------------------------
    // PerformanceViewer: store graphs resource
    // ----------------------------------------

    private static Object viewerStoreResource = "./.perf/procedures-graph";

    public static void setViewerStoreResource(Object viewerStoreResource) {
        Performance.viewerStoreResource = viewerStoreResource;
    }

    static Path getViewerStorePath(String id) {
        try {
            return IOResources.resourceToOutPath(viewerStoreResource,id,false,true);
        } catch (FileAlreadyExistsException|NotDirectoryException e) {
            e.printStackTrace();
            return null;
        }
    }


    // ---------
    // Listeners
    // ---------

    private final static List<AnalyticsListener> analyticsListeners = new ArrayList<>();
    private final static List<AnalyticsExecutionsListener> analyticsExecListeners = new ArrayList<>();
    private final static List<AnalyticsUpdListener> analyticsUpdListeners = new ArrayList<>();
    private final static List<AnalyticsChecksListener> analyticsCheckListeners = new ArrayList<>();

    public static void addAnalyticsListener(AnalyticsListener listener) {
        analyticsListeners.add(listener);
    }

    public static void removeAnalyticsListener(AnalyticsListener listener) {
        analyticsListeners.remove(listener);
    }

    public static void addAnalyticsExecutionsListener(AnalyticsExecutionsListener listener) {
        analyticsExecListeners.add(listener);
    }

    public static void removeAnalyticsExecutionsListener(AnalyticsExecutionsListener listener) {
        analyticsExecListeners.remove(listener);
    }

    public static void addAnalyticsUpdListener(AnalyticsUpdListener listener) {
        analyticsUpdListeners.add(listener);
    }

    public static void removeAnalyticsUpdListener(AnalyticsUpdListener listener) {
        analyticsUpdListeners.remove(listener);
    }

    public static void addAnalyticsChecksListener(AnalyticsChecksListener listener) {
        analyticsCheckListeners.add(listener);
    }

    public static void removeAnalyticsChecksListener(AnalyticsChecksListener listener) {
        analyticsCheckListeners.remove(listener);
    }

    public interface AnalyticsListener {
        void notifyAnalyticExecution();
    }

    public interface AnalyticsExecutionsListener {
        void notifyAnalyticExecutions(Map<String, Execution> executionsFromCollector);
    }

    public interface AnalyticsUpdListener {
        void notifyAnalyticUpd(List<ProceduresCollection> updatedProcs);
    }

    public interface AnalyticsChecksListener {
        void notifyAnalyticChecks(List<ExecutionCheck> checkedExecutions);
    }

    static void notifyAnalyticsExecution(Map<String, Execution> executionsFromCollector, List<ProceduresCollection> updatedProcs, List<ExecutionCheck> checkedExecutions) {
        for (AnalyticsListener listener : analyticsListeners)
            listener.notifyAnalyticExecution();

        for (AnalyticsExecutionsListener listener : analyticsExecListeners)
            listener.notifyAnalyticExecutions(executionsFromCollector);

        for (AnalyticsUpdListener listener : analyticsUpdListeners)
            listener.notifyAnalyticUpd(updatedProcs);

        for (AnalyticsChecksListener listener : analyticsCheckListeners)
            listener.notifyAnalyticChecks(checkedExecutions);
    }


    // -----
    // Utils
    // -----

    public static String generateUniqueId() {
        return String.format("%05d", executionCounter++);
    }


    // -------------------------------------------------------------------------
    // -------------------------------------------------------------------------

    // -------
    // Testing
    // -------

    @SuppressWarnings({"RedundantSuppression", "UnnecessaryLocalVariable", "ConstantConditions"})
    public static void main(String[] args) throws InterruptedException {
        System.out.println("Started at " + SampleRate.toFormattedDate(Performance.getMillisSince()));

        boolean scheduled = false;
        int times = 5;

        // params parsing
        for (String a: args) {
            if (a.startsWith("scheduled")) {
                scheduled = !a.contains("false");
            }

            else if (a.startsWith("times")) {
                a = a.substring(a.indexOf("=")+1).trim();
                try {
                    times = Integer.parseInt(a);
                } catch (Exception e) {e.printStackTrace();
                    System.err.println("Only a single integer args is allowed.");
                    System.exit(-1);
                }
            }
        }

        // start performance scheduled
        if (scheduled)
            Performance.startAnalyzerSync();

        for (int i = 0; i<times; i++) {
            // Procedure "testProcedure"
            // blocks "Step A", "Step B", "Step C"
            // execution time 300-400ms
            PerformanceCollector.test_simpleCustomExecution("testProcedure");
            Thread.sleep(10);

            // Procedure "testProcedure"
            // blocks customizable
            // execution customizable
            //List<String> blocks = new ArrayList<>();
            //blocks.add("Step A");
            //blocks.add("Step B");
            //blocks.add("Step C");
            //int min = 300;
            //int max = 400;
            //PerformanceCollector.test_simpleCustomExecution("testProcedure",blocks,min,max);
        }

        // exec performance analyzer, evend if not scheduled
        Performance.stopAnalyzer();
        System.out.println("Ended at " + SampleRate.toFormattedDate(Performance.getMillisSince()));
        System.exit(0);
    }

}
