package com.robypomper.utils.performance;

import com.robypomper.utils.performance.analyzer.Procedure;
import com.robypomper.utils.performance.collector.Execution;
import com.robypomper.utils.performance.collector.ExecutionBlock;
import com.robypomper.utils.performance.collector.ExecutionUnit;

import java.sql.Timestamp;
import java.util.*;

/**
 * <p>Class used by developers to collect data about executions.
 *
 * <p>This class provide a set of static method that can be called by Developers
 * from their own code. This methods helps register the software execution and
 * describe his structure.
 *
 * <p><b>Integrate Performance Utils:</b>
 * <p>Developers can start by recording a newExecution giving the Procedure's Name,
 * for example on main method, they canc all the {@link #registerNewExecution(String)}
 * method:
 * <pre>{@code
 * public static void main(String[] args) {
 *     PerformanceCollector.registerNewExecution("Main");
 *     // ...
 * }
 * }</pre>
 *
 * <p>Before calling any of {@link PerformanceCollector#terminateExecution()} methods,
 * developers can register has many blocks he needs. Each block must be started
 * and ended by any of {@link PerformanceCollector#startRecording(String)} and
 * {@link PerformanceCollector#endRecording(String)} methods.
 *
 * <p>Developers can integrate Performance library in their functions like in the
 * following examples:
 * <pre>{@code
 * public static void function(String[] args) {
 *     PerformanceCollector.startRecording("BlockFunction");
 *     // ...
 *     PerformanceCollector.endRecording("BlockFunction");
 * }
 *
 * public static void functionIf(String[] args) {
 *     if ( condition ) {
 *         PerformanceCollector.startRecording("BlockFunction_A");
 *         // ...
 *         PerformanceCollector.endRecording("BlockFunction_A");
 *     } else {
 *         PerformanceCollector.startRecording("BlockFunction_B");
 *         // ...
 *         PerformanceCollector.endRecording("BlockFunction_B");
 *     }
 * }
 *
 * public static void functionSequence(String[] args) {
 *     PerformanceCollector.startRecording("BlockFunction_1");
 *     // ...
 *     PerformanceCollector.endRecording("BlockFunction_1");
 *
 *     PerformanceCollector.startRecording("BlockFunction_2");
 *     // ...
 *     PerformanceCollector.endRecording("BlockFunction_2");
 *
 *     if ( condition ) {
 *         PerformanceCollector.startRecording("BlockFunction_2_Opz");
 *         // ...
 *         PerformanceCollector.endRecording("BlockFunction_2_Opz");
 *     }
 *
 *     PerformanceCollector.startRecording("BlockFunction_3");
 *     // ...
 *     PerformanceCollector.endRecording("BlockFunction_3");
 * }
 * }</pre>
 *
 * <p><b>Clone execution:</b>
 * <p>Sometimes is also necessary clone an Execution: imagine a Procedure that
 * process some parallel blocks. To do that developers can call the
 * {@link PerformanceCollector#registerNewExecutionFrom(String)} methods. This
 * methods start a new Execution cloning the current Execution, or that one of
 * given executionId. All block's records after this method call are added to the
 * cloned Execution.
 * Original execution can be termianted as usual with one of
 * {@link PerformanceCollector#terminateExecution()} methods (this move the
 * original execution to terminatedExecutions) or by one of
 * {@link PerformanceCollector#freeExecution()} methods (this destroy the
 * original execution).
 *
 * <p>NB!: If developer don't respect the right calling sequence, assertions will throed.
 *
 * <p><b>Execution to Procedure:</b>
 * <p>PerformanceCollector only collect Executions.
 * <p>When an Execution ends by calling ony of the {@link PerformanceCollector#terminateExecution()}
 * methods, PerformanceColelctor move the Execution to terminatedExecutions and
 * made it available for {@link PerformanceAnalyzer#mainTask()} via
 * {@link PerformanceCollector#take()} method.
 *
 * <p><b>Threads support:.</b>
 * <p>All PerformanceCollector's methods called using the executionId param,
 * are thread safety. When developer uses simplified versions of
 * PerformanceCollector's methods without executionId param, when start a new
 * thread the {@link PerformanceCollector#switchThread(String, Thread)} method
 * must be called.
 *
 * <p><b>Internal stack Procedures and Blocks:</b>
 * <p>During Execution recording, <b>PerformanceCollector records also information
 * about stack calls between Procedures and Blocks</b>. Stack management it's
 * completely internal in PerformanceCollector and PerformanceAnalyzer can access
 * to it by {@link Procedure#BlockSequences()} and {@link Procedure#Stacks()}
 * methods.
 * StackCalls are based on {@link ExecutionUnit} interface, this means that
 * "Procedure to Blocks" and "Block to Procedures" calls are considered in
 * StackCalls.
 *
 * @since 0.1
 */
@SuppressWarnings({"WeakerAccess", "unused"})
public class PerformanceCollector {

    /**
     * Timestamp of last {@link PerformanceCollector#take()} call.
     * It refers to the last time the terminatedExecutions was emptied. If it's
     * null terminatedExecutions was never emptied.
     */
    private static Timestamp lastReset = null;
    /**
     * It contains all Executions currently running.
     *
     * Ordered Map of ExecutionId (String) and Executions->ExecutionBlocks.
     */
    private final static Map<String,Execution> runningExecutions = new TreeMap<>();
    /**
     * It contains all terminated Executions since lastReset.
     *
     * Ordered Map of ExecutionId (String) and Executions->ExecutionBlocks.
     */
    private final static Map<String,Execution> terminatedExecutions = new TreeMap<>();
    /**
     * It contains the Execution - Thread associations.
     *
     * Ordered Map of ExecutionId (String) and Threads.
     */
    private final static Map<String,Thread> runningExecutionsThreads = new TreeMap<>();
    /**
     * This Map store if a Procedure is Enable/Disable to be monitored.
     *
     * ToDO: more info about enabling/disabling PerformanceCollector
     *
     * Ordered Map of ExecutionId (String) and Executions->ExecutionBlocks.
     */
    private final static Map<String,Boolean> runningExecutionsEnabled = new TreeMap<>();
    /**
     * This Map store a stack call (between Procedures and Blocks) for each thread.
     *
     * Map of Threads and Stack<ExecutionUnit>.
     */
    private final static Map<Thread,Stack<ExecutionUnit>> runningStacks = new HashMap<>();


    // ---------------
    // Executions Mngm (by User)
    // ---------------

    /**
     * Register a new Execution with a new generated ExecutionId.
     *
     * New execution can be registered in the main method or within the block
     * recording as sub Procedure. Next block's record will be part of registered
     * execution until {@link #terminateExecution()} methods will call.
     *
     * This method is thread-safe.
     *
     * @param procName Procedure's name.
     * @return generated executionId. it can be used as parameter for others
     *         Executions Management methods.
     * @see #registerNewExecutionFrom(String)
     */
    public static String registerNewExecution(String procName) {
        String newExecutionId = Performance.generateUniqueId();
        Execution newExecution = new Execution(newExecutionId, procName);

        runningExecutions.put(newExecutionId, newExecution);
        runningExecutionsThreads.put(newExecutionId,Thread.currentThread());
        runningExecutionsEnabled.put(newExecutionId,Performance.EnableCollector());
        pushStackExecutionUnit(newExecution);
        return newExecutionId;
    }

    /**
     * Clone oldExecutionId and register the clone as new Execution.
     *
     * This method act as {@link #registerNewExecution(String)} but avoid to push
     * the call to the Execution Stack. So any next block's record will be part
     * of cloned execution until {@link #terminateExecution()} methods will call.
     *
     * If not found oldExecutionId an assertion will towed or null will returned.
     *
     * This method is thread-safe.
     *
     * @param oldExecutionId reference to the ExecutionId to clone.
     * @return generated executionId for cloned execution. it can be used as
     *         parameter for others Executions Management methods.
     *         Null if oldExecutionId was not a running executionId.
     * @see #registerNewExecution(String)
     * @see #registerNewExecutionFrom()
     */
    public static String registerNewExecutionFrom(String oldExecutionId) {
        String newExecutionId = Performance.generateUniqueId();

        Execution oldExecution = runningExecutions.get(oldExecutionId);
        //noinspection ConstantConditions
        assert oldExecution!=null : String.format("Can't clone execution because unknown origin execution '%s'.", oldExecution);
        //noinspection ConstantConditions
        if (oldExecution==null) return null;

        Execution newExecution = Execution.clone(oldExecution,newExecutionId);

        runningExecutions.put(newExecutionId, newExecution);
        runningExecutionsThreads.put(newExecutionId,Thread.currentThread());
        runningExecutionsEnabled.put(newExecutionId,Performance.EnableCollector());
        return newExecutionId;
    }

    /**
     * Wrapp {@link #terminateExecution(String, Throwable)}
     *
     * This method is thread-safe.
     *
     * @param executionId reference to the ExecutionId to terminate.
     * @see #terminateExecution(String, Throwable)
     * @see #terminateExecution()
     * @see #terminateExecution(Throwable)
     *
     */
    public static void terminateExecution(String executionId) {
        terminateExecution(executionId,null);
    }

    /**
     * Move the execution from running executions to terminated executions.
     *
     * This method register the error (if any) and current stack to Execution
     * before add it to terminatedExecutions.
     *
     * If not found executionId an assertion will towed or null will returned.
     *
     * This method is thread-safe.
     *
     * @param executionId reference to the ExecutionId to terminate.
     * @param error error occurred during Procedure execution, null if no error
     *                    occurred.
     * @see #terminateExecution(String)
     * @see #terminateExecution()
     * @see #terminateExecution(Throwable)
     */
    public static void terminateExecution(String executionId, Throwable error) {
        Execution execution = runningExecutions.remove(executionId);
        assert execution!=null : String.format("Can't terminate execution because unknown execution '%s'.", executionId);
        //noinspection ConstantConditions
        if (execution==null) return;

        try {
            execution.registerError(error);
            execution.registerStack(currentStack());
        } catch (IllegalStateException e) {
            assert false : String.format("Can't terminate execution '%s' because: %s.", execution.ProcName, e.getMessage());
        }
        if (!execution.isEmpty()) {
            ExecutionBlock lastBlock = execution.BlocksOrderedByEnd().get(0);
            if (!lastBlock.isTerminated()) {
                lastBlock.stop(error);
            }
        }

        terminatedExecutions.put(executionId,execution);
        runningExecutionsThreads.remove(executionId);
        runningExecutionsEnabled.remove(executionId);
        popStackExecutionUnit(execution);
    }

    /**
     * Permanently delete the running execution.
     *
     * This method is used after {@link #registerNewExecutionFrom()} methods to
     * delete original Execution.
     *
     * This method is thread-safe.
     *
     * @param executionId reference to the ExecutionId to delete.
     * @see #freeExecution()
     */
    public static void freeExecution(String executionId) {
        Execution execution = runningExecutions.remove(executionId);
        assert execution!=null : String.format("Can't free execution because unknown execution '%s'.", executionId);
        //noinspection ConstantConditions
        if (execution==null) return;

        runningExecutionsThreads.remove(executionId);
        runningExecutionsEnabled.remove(executionId);
    }

    /**
     * Start a new block registration for executionId.
     *
     * This start {@link ExecutionBlock} registration for executionId. After
     * this method new Execution can be registered as sub Procedures of this
     * block.
     *
     * If this Execution is disabled in PerformanceCollector, then it abort and
     * return  null.
     *
     * This method is thread-safe.
     *
     * @param executionId reference to the ExecutionId to start block registration.
     * @param blockId Block's name
     * @see #startRecording(String)
     */
    public static void startRecording(String executionId, String blockId) {
        Boolean enabled = runningExecutionsEnabled.get(executionId);
        assert enabled!=null : String.format("Can't start recording block '%s' on execution because unknown execution '%s' (on runningExecutionsEnabled).", blockId, executionId);
        //noinspection ConstantConditions
        if (enabled==null || !enabled) return;

        Execution execution = runningExecutions.get(executionId);
        assert execution!=null : String.format("Can't start recording block '%s' on execution because unknown execution '%s'.", blockId, executionId);
        //noinspection ConstantConditions
        if (execution==null) return;

        ExecutionBlock block = execution.startBlock(blockId);
        pushStackExecutionUnit(block);
    }

    /**
     * Wrapp {@link #endRecording(String, String, Throwable)}
     *
     * This method is thread-safe.
     *
     * @param executionId reference to the ExecutionId to end block registration.
     * @param blockId Block's name
     * @see #endRecording(String, String, Throwable)
     * @see #endRecording(String)
     * @see #endRecording(String, Throwable)
     */
    public static void endRecording(String executionId, String blockId) {
        endRecording(executionId, blockId, null);
    }

    /**
     * End block registration for executionId.
     *
     * If this Execution is disabled in PerformanceCollector, then it abort and
     * return  null.
     *
     * This method is thread-safe.
     *
     * @param executionId reference to the ExecutionId to end block registration.
     * @param blockId Block's name
     * @param error error occurred during Procedure execution, null if no error
     *                    occurred.
     * @see #endRecording(String, String)
     * @see #endRecording(String)
     * @see #endRecording(String, Throwable)
     */
    public static void endRecording(String executionId, String blockId, Throwable error) {
        Boolean enabled = runningExecutionsEnabled.get(executionId);
        assert enabled!=null : String.format("Can't end recording block '%s' on execution because unknown execution '%s' (on runningExecutionsEnabled).", blockId, executionId);
        //noinspection ConstantConditions
        if (enabled==null || !enabled) return;

        Execution execution = runningExecutions.get(executionId);
        assert execution!=null : String.format("Can't end recording block '%s' on execution because unknown execution '%s'.", blockId, executionId);
        //noinspection ConstantConditions
        if (execution==null) return;

        try {
            ExecutionBlock block = execution.endBlock(blockId, error);
            popStackExecutionUnit(block);
        } catch (IllegalStateException e) {
            assert false : String.format("Can't end recording block '%s' on execution '%s' because: %s.", blockId, execution.ProcName, e.getMessage());
        }
    }


    // ------
    // Stacks
    // ------

    private static Stack<ExecutionUnit> currentStack() {
        Stack<ExecutionUnit> s = runningStacks.get(Thread.currentThread());
        if (s!=null)
            return s;

        s = new Stack<>();
        runningStacks.put(Thread.currentThread(),s);
        return s;
    }

    private static void pushStackExecutionUnit(ExecutionUnit executionUnit) {
        currentStack().push(executionUnit);
    }

    private static void popStackExecutionUnit(ExecutionUnit executionUnit) {
        assert !currentStack().empty() : String.format("Can't pop '%s' from stack because stack is empty.", executionUnit.getName());
        currentStack().pop();
    }

    public static Stack<ExecutionUnit> getStack(String executionId) { return getStack(runningExecutionsThreads.get(executionId)); }

    public static Stack<ExecutionUnit> getStack(Thread thread) { return runningStacks.get(thread); }

    public static int getStackLevel(String executionId) { return getStackLevel(runningExecutionsThreads.get(executionId)); }

    public static int getStackLevel(Thread thread) { return new ArrayList<>(getStack(thread)).size(); }

    public static String printStack() { return printStack(Thread.currentThread()); }

    public static String printStack(String executionId) { return printStack(runningExecutionsThreads.get(executionId)); }

    public static String printStack(Thread thread) { return printStack(getStack(thread),true); }

    public static String printStack(Stack<ExecutionUnit> stack, boolean multiLines) {
        List<ExecutionUnit> executions = new ArrayList<>(stack);
        StringBuilder msg = new StringBuilder();
        String sep, ind, bol;
        if (multiLines) {
            sep = "\n";
            ind = "  ";
            bol = ">";
        } else {
            sep = " ";
            ind = "";
            bol = "\\";
        }

        int count=0;
        for (ExecutionUnit execution : executions) {
            msg.append(new String(new char[count]).replace("\0", ind));
            msg.append(bol).append(" ").append(execution.getName()).append(sep);
            count++;
        }
        return msg.toString();
    }


    // -------
    // Threads
    // -------

    private static String getExecutionByThread(Thread thread) {
        LinkedList<Map.Entry<String,Thread>> list = new LinkedList<>(runningExecutionsThreads.entrySet());
        Iterator<Map.Entry<String,Thread>> itr = list.descendingIterator();
        while(itr.hasNext()) {
            Map.Entry<String,Thread> entry = itr.next();
            if (entry.getValue()==thread)
                return entry.getKey();
        }

        return null;
    }

    public static void switchThread(Thread oldTh) {
        String executionId = getExecutionByThread(oldTh);
        assert executionId!=null : "Can't switch execution's thread because no execution registered on this thread.";
        //noinspection ConstantConditions
        if (executionId==null) return;

        switchThread(executionId,Thread.currentThread());
    }

    public static void switchThread(String executionId) {
        switchThread(executionId,Thread.currentThread());
    }

    public static void switchThread(String executionId,Thread newTh) {
        assert runningExecutions.get(executionId)!=null : String.format("Can't switch execution's thread because no execution '%s' registered on this thread.", executionId);

        runningExecutionsThreads.put(executionId,newTh);
    }


    // ---------------
    // Executions Mngm (by User with Threads support)
    // ---------------

    public static String registerNewExecutionFrom() {
        String executionId = getExecutionByThread(Thread.currentThread());
        assert executionId!=null : "No execution registred on this thread";
        //noinspection ConstantConditions
        if (executionId==null) return null;

        return registerNewExecutionFrom(executionId);
    }

    public static void terminateExecution() {
        String executionId = getExecutionByThread(Thread.currentThread());
        assert executionId!=null : "No execution registred on this thread";
        //noinspection ConstantConditions
        if (executionId==null) return;

        terminateExecution(executionId);
    }

    public static void terminateExecution(Throwable error) {
        String executionId = getExecutionByThread(Thread.currentThread());
        assert executionId!=null : "No execution registred on this thread";
        //noinspection ConstantConditions
        if (executionId==null) return;

        terminateExecution(executionId, error);
    }

    public static void freeExecution() {
        String executionId = getExecutionByThread(Thread.currentThread());
        assert executionId!=null : "No execution registred on this thread";
        //noinspection ConstantConditions
        if (executionId==null) return;

        freeExecution(executionId);
    }

    public static void startRecording(String blockId) {
        String executionId = getExecutionByThread(Thread.currentThread());
        assert executionId!=null : "No execution registred on this thread";
        //noinspection ConstantConditions
        if (executionId==null) return;

        startRecording(executionId,blockId);
    }

    public static void endRecording(String blockId) {
        String executionId = getExecutionByThread(Thread.currentThread());
        assert executionId!=null : "No execution registred on this thread";
        //noinspection ConstantConditions
        if (executionId==null) return;

        endRecording(executionId,blockId);
    }

    public static void endRecording(String blockId, Throwable error) {
        String executionId = getExecutionByThread(Thread.currentThread());
        assert executionId!=null : "No execution registred on this thread";
        //noinspection ConstantConditions
        if (executionId==null) return;

        endRecording(executionId,blockId, error);
    }


    // --------------------------
    // Terminated Executions Mngm
    // --------------------------

    static Collection<Execution> getAll() {
        return Collections.unmodifiableCollection(terminatedExecutions.values());
    }

    static List<Execution> get(Timestamp from, Timestamp to) {
        return getByStart(from,to);
    }

    static List<Execution> getByStart(Timestamp from, Timestamp to) {
        List<Execution> res = new ArrayList<>();
        for (Execution exec : terminatedExecutions.values())
            if (exec.StartTime().getTime()>=from.getTime() && exec.StartTime().getTime()<=to.getTime())
                res.add(exec);
        return res;
    }

    static List<Execution> getByEnd(Timestamp from, Timestamp to) {
        List<Execution> res = new ArrayList<>();
        for (Execution exec : terminatedExecutions.values())
            if (exec.EndTime().getTime()>=from.getTime() && exec.EndTime().getTime()<=to.getTime())
                res.add(exec);
        return res;
    }

    static List<Execution> getByStartAndEnd(Timestamp from, Timestamp to) {
        List<Execution> res = new ArrayList<>();
        for (Execution exec : terminatedExecutions.values())
            if (exec.StartTime().getTime()>=from.getTime() && exec.StartTime().getTime()<=to.getTime())
                if (exec.EndTime().getTime()>=from.getTime() && exec.EndTime().getTime()<=to.getTime())
                    res.add(exec);
        return res;
    }

    static Map<String,Execution> take() {
        Map<String, Execution> tmp = new TreeMap<>(terminatedExecutions);
        terminatedExecutions.clear();
        lastReset = new Timestamp(Performance.getMillisSince());
        return tmp;
    }

    static void clean() {
        take();
    }


    // ---------------
    // Collector Utils
    // ---------------

    public static String printCollectorStatus() {
        return String.format("PerformanceCollector Status:       runningExecutions: %d;\tterminatedExecution: %d;\t lastClean: %s;", runningExecutions.size(),terminatedExecutions.size(), lastReset);
    }

    // -------------------------------------------------------------------------
    // -------------------------------------------------------------------------

    // -------
    // Testing
    // -------

    public static void main(String[] args) {
        Performance.setNewNow("2000-01-01 00:00:00.0");

        // Register the execution of a simple procedure with 3 methods
        System.out.println("\n\n##########");
        System.out.println("simpleTest");
        System.out.println("##########\n");
        PerformanceCollector.clean();
        simpleTest();

        // Register the execution of 3 different procedures, one execution for procedure.
        System.out.println("\n\n##########");
        System.out.println("simpleTest");
        System.out.println("##########\n");
        PerformanceCollector.clean();
        multiProceduresTest();

        // Register the execution of a simple procedure then split in other two executions.
        System.out.println("\n\n##########");
        System.out.println("splitProcedureTest");
        System.out.println("##########\n");
        PerformanceCollector.clean();
        splitProcedureTest();

        // Register the execution of a sequence of procedures then filter execution with get methods.
        System.out.println("\n\n##########");
        System.out.println("gettersMethods");
        System.out.println("##########\n");
        PerformanceCollector.clean();
        gettersMethodsTest();

        System.exit(0);
    }

    // Test functions

    private static void simpleTest() {
        System.out.println("\n# Initial state");
        System.out.println(PerformanceCollector.printCollectorStatus());

        System.out.println("\n# Execute simpleDefaultExecution");
        test_simpleDefaultExecution();
        System.out.println(PerformanceCollector.printCollectorStatus());

        System.out.println("\n# Final state");
        System.out.println(PerformanceCollector.printCollectorStatus());
    }

    private static void multiProceduresTest() {
        System.out.println("\n# Initial state");
        System.out.println(PerformanceCollector.printCollectorStatus());

        System.out.println("\n# Execute simpleCustomExecution");
        test_simpleCustomExecution("Procedure A");
        test_simpleCustomExecution("Procedure B");
        test_simpleCustomExecution("Procedure C");
        System.out.println(PerformanceCollector.printCollectorStatus());

        System.out.println("\n# Final state");
        System.out.println(PerformanceCollector.printCollectorStatus());
    }

    private static void splitProcedureTest() {
        System.out.println("\n# Initial state");
        System.out.println(PerformanceCollector.printCollectorStatus());


        System.out.println("\n# Create original procedure and execute simpleCustomExecutionBlock");
        String execOriginal = PerformanceCollector.registerNewExecution("ProcedureSplit");
        test_simpleCustomExecutionBlock(execOriginal,"Shared Block");
        System.out.println(PerformanceCollector.printCollectorStatus());

        System.out.println("\n# Split original procedure");
        String execA = PerformanceCollector.registerNewExecutionFrom(execOriginal);
        String execB = PerformanceCollector.registerNewExecutionFrom(execOriginal);
        System.out.println(PerformanceCollector.printCollectorStatus());

        System.out.println("\n# Execute derived procedures");
        test_simpleCustomExecutionBlock(execA,"Block 2");
        test_simpleCustomExecutionBlock(execB,"Block 2");
        test_simpleCustomExecutionBlock(execA,"Block 3 ExecA");
        test_simpleCustomExecutionBlock(execB,"Block 3 ExecB");
        PerformanceCollector.terminateExecution(execA);
        PerformanceCollector.terminateExecution(execB);
        System.out.println(PerformanceCollector.printCollectorStatus());


        System.out.println("\n# Free original procedure");
        PerformanceCollector.freeExecution(execOriginal);
        System.out.println(PerformanceCollector.printCollectorStatus());

        System.out.println("\n# Final state");
        System.out.println(PerformanceCollector.printCollectorStatus());
    }

    private static void gettersMethodsTest() {
        Map<String,Timestamp> times = test_regTimeSequence();
        Timestamp startTime = times.get("startTime");
        Timestamp rangeFrom = times.get("rangeFrom");
        Timestamp rangeTo = times.get("rangeTo");
        Timestamp endTime = times.get("endTime");

        assert getAll().size()==4;                              // All
        assert get(startTime,endTime).size()==4;                // All
        assert getByStart(startTime,endTime).size()==4;         // All
        assert getByEnd(startTime,endTime).size()==4;           // All
        assert getByStartAndEnd(startTime,endTime).size()==4;   // All
        System.out.println(getAll());
        System.out.println(get(startTime,endTime));
        System.out.println(getByStart(startTime,endTime));
        System.out.println(getByEnd(startTime,endTime));
        System.out.println(getByStartAndEnd(startTime,endTime));

        assert get(rangeFrom,rangeTo).size()==2;                // Internal, EndOut
        assert getByStart(rangeFrom,rangeTo).size()==2;         // Internal, EndOut
        assert getByEnd(rangeFrom,rangeTo).size()==2;           // Internal, StartOut
        assert getByStartAndEnd(rangeFrom,rangeTo).size()==1;   // Internal
        System.out.println(get(rangeFrom,rangeTo));
        System.out.println(getByStart(rangeFrom,rangeTo));
        System.out.println(getByEnd(rangeFrom,rangeTo));
        System.out.println(getByStartAndEnd(rangeFrom,rangeTo));
    }

    // Test utils

    protected static void test_simpleDefaultExecution() {
        String procName = "Procedure 1";
        test_simpleCustomExecution(procName);
    }

    protected static void test_simpleCustomExecution(String procName) {
        List<String> blocks = new ArrayList<>();
        blocks.add("Step A");
        blocks.add("Step B");
        blocks.add("Step C");
        int min = 300;
        int max = 400;
        test_simpleCustomExecution(procName,blocks,min,max);
    }

    protected static void test_simpleCustomExecution(String procName, List<String> blocks, int min, int max) {
        String execId = PerformanceCollector.registerNewExecution(procName);

        for (String blockName : blocks) {
            test_simpleCustomExecutionBlock(execId,blockName,min,max);
        }

        PerformanceCollector.terminateExecution(execId);
    }

    protected static void test_simpleCustomExecutionBlock(String executionId, String blockName) {
        int min = 300;
        int max = 400;
        test_simpleCustomExecutionBlock(executionId,blockName,min,max);
    }

    protected static void test_simpleCustomExecutionBlock(String executionId, String blockName, int min, int max) {
        PerformanceCollector.startRecording(executionId, blockName);
        try {
            int sleepTime = (int) (Math.random() * (max - min)) + min;
            Thread.sleep(sleepTime);
        } catch (InterruptedException e) {
            System.err.println("InterruptedException on simpleCustomExecutionBlock: " + e);
            return;
        }
        PerformanceCollector.endRecording(executionId, blockName);
    }

    private static Map<String,Timestamp> test_regTimeSequence() {
        Map<String,Timestamp> times = new HashMap<>();

        // Register executions
        String blockName = "Step";
        String Internal = PerformanceCollector.registerNewExecution("Internal");
        String EndOut = PerformanceCollector.registerNewExecution("EndOut");
        String StartOut = PerformanceCollector.registerNewExecution("StartOut");
        String Outer = PerformanceCollector.registerNewExecution("Outer");


        times.put("startTime",new Timestamp(Performance.getMillisSince()));

        PerformanceCollector.startRecording(Outer, blockName);
        PerformanceCollector.startRecording(StartOut, blockName);

        try {
            Thread.sleep(250);
        } catch (InterruptedException e) {
            System.err.println("InterruptedException on simpleCustomExecutionBlock: " + e);
        }

        times.put("rangeFrom",new Timestamp(Performance.getMillisSince()));

        PerformanceCollector.startRecording(EndOut, blockName);
        PerformanceCollector.startRecording(Internal, blockName);

        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            System.err.println("InterruptedException on simpleCustomExecutionBlock: " + e);
        }

        PerformanceCollector.endRecording(Internal, blockName);
        PerformanceCollector.endRecording(StartOut, blockName);

        times.put("rangeTo",new Timestamp(Performance.getMillisSince()));

        try {
            Thread.sleep(250);
        } catch (InterruptedException e) {
            System.err.println("InterruptedException on simpleCustomExecutionBlock: " + e);
        }

        PerformanceCollector.endRecording(EndOut, blockName);
        PerformanceCollector.endRecording(Outer, blockName);

        times.put("endTime",new Timestamp(Performance.getMillisSince()));


        // Terminate executions
        PerformanceCollector.terminateExecution(Internal);
        PerformanceCollector.terminateExecution(EndOut);
        PerformanceCollector.terminateExecution(StartOut);
        PerformanceCollector.terminateExecution(Outer);

        return times;
    }

}
