package com.robypomper.utils.performance

import com.robypomper.utils.aspectj.AspectJCompileTask
import org.gradle.api.InvalidUserDataException


/**
 * Execute AspectJCompile on specified sourceSet.
 *
 * Create the 'ajc' configuration and add dependencies for AspectJCompiler
 * execution.
 */
@SuppressWarnings("unused")
class PerformanceAnnotationsTask extends AspectJCompileTask {
    PerformanceAnnotationsTask() {
        super();
        project.configurations { ajcPerformance }
        try {
            project.dependencies {
                ajcPerformance  group: 'com.robypomper.utils.performance', name: 'performance', version: '0.2'
            }
        } catch (InvalidUserDataException ignore) {}
        aspectJCompiler = project.configurations['ajcPerformance']
    }
}
