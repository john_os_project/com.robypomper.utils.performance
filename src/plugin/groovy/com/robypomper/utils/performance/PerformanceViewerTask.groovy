package com.robypomper.utils.performance

import org.gradle.api.DefaultTask
import org.gradle.api.Task
import org.gradle.api.file.FileCollection
import org.gradle.api.tasks.SourceSet


/**
 * Execute PerformanceViewer with specified configurations.
 */
@SuppressWarnings("unused")
class PerformanceViewerTask extends DefaultTask {

    // ----------
    // Properties
    // ----------

    /**
     * Class path to execute PerformanceViewer.
     * By default it's the path of library "com.robypomper.utils.performance:performance:0.2".
     */
    String classpath
    void setClasspath(String classpath) {
        this.classpath = classpath
        //reconfigClasspath()
    }
    void setClasspath(FileCollection classpath) {
        this.classpath = classpath.asPath
        //reconfigClasspath()
    }
    void setSourceSet(SourceSet sourceSet) {
        setClasspath(sourceSet.runtimeClasspath)
        //reconfigClasspath()
    }

    /**
     * If set to true, PerformanceViewer will throws annotations exceptions.
     * By default it's false.
     */
    boolean enableAssertions = false
    void setEnableAssertions(boolean enableAssertions) {
        this.enableAssertions = enableAssertions
        //reconfigEnableAssertions()
    }

    /**
     * Path of ProceduresCollection's files.
     * By default it's '.perf/procedures'.
     */
    String proceduresPath = null
    void setProceduresPath(String proceduresPath) {
        this.proceduresPath = proceduresPath
        //reconfigProceduresPath()
    }

    /**
     * Time to show in "yyyy-MM-dd HH:mm:ss.S" format or "" to set now.
     * By default it's set to "" that means now.
     */
    String selectedTime = null
    void setSelectedTime(String time) {
        this.selectedTime = selectedTime
        //reconfigSelectedTime()
    }

    /**
     * Name of SampleRate to show (Minute, Hour, HalFay, Day), Minute as default.
     * By default it's "Minute" (SampleRate.Minute).
     */
    String selectedSampleRate = null
    void setSelectedSampleRate(String time) {
        this.selectedSampleRate = selectedSampleRate
        //reconfigSelectedSampleRate()
    }

    /**
     * ...
     */
    boolean fullscreen = null
    void setFullscreen(boolean fullscreen) {
        this.fullscreen = fullscreen
        //reconfigFullscreen()
    }

    /**
     * ...
     */
    int updateTime = 5
    void setUpdateTime(int updateTime) {
        this.updateTime = updateTime
        //reconfigUpdateTime()
    }

    /**
     * ...
     */
    boolean warn = null
    void setWarning(boolean warn) {
        this.warn = warn
        //reconfigWarning()
    }

    /**
     * ...
     */
    boolean log = null
    void setLog(boolean log) {
        this.log = log
        //reconfigLog()
    }

    /**
     * ...
     */
    boolean verbose = null
    void setVerbose(boolean verbose) {
        this.verbose = verbose
        //reconfigVerbose()
    }

    /**
     * If set to true, PerformanceViewer print his log on Gradle output streams.
     * By default it's set false.
     */
    boolean printLog = false
    void setPrintOnGradle(boolean printLog) {
        this.printLog = printLog
        //reconfigPrintOnGradle()
    }

    /**
     * Task dependant on PerformanceViewer execution, normally it's the hosting
     * project's run task. Only once runTask at time can be setted.
     */
    Task runTask = null
    void setRunTask(Task run) {
        if (runTask!=null)
            runTask.dependsOn.remove this
        run.dependsOn this
    }

    // -----------
    // Constructor
    // -----------

    PerformanceViewerTask() {
        //noinspection GroovyAssignabilityCheck
        group "runnables"

        getProject().configurations { performance }
        getProject().dependencies {
            performance  group: 'com.robypomper.utils.performance', name: 'performance', version: '0.2'
        }
        classpath = getProject().configurations['performance'].asPath

        doFirst {
            exec()
        }
    }


    // --------
    // Executor
    // --------

    void exec() {
        String main = 'com.robypomper.utils.performance.PerformanceViewer'
        List<String> argsStr = new ArrayList<>()
        if (proceduresPath!=null) {
            argsStr.add '--procLoadResource'
            argsStr.add "\"$proceduresPath\""
        }
        if (selectedTime!=null) {
            argsStr.add '--selectedTime'
            argsStr.add "\"$selectedTime\""
        }
        if (selectedSampleRate!=null) {
            argsStr.add '--selectedSampleRate'
            argsStr.add "\"$selectedSampleRate\""
        }
        if (fullscreen!=null) {
            argsStr.add '--fullscreen'
            argsStr.add "\"$fullscreen\""
        }
        if (updateTime!=null) {
            argsStr.add '--updateTime'
            argsStr.add "\"$updateTime\""
        }
        if (warn!=null) {
            argsStr.add '--warn'
            argsStr.add "\"$warn\""
        }
        if (log!=null) {
            argsStr.add '--log'
            argsStr.add "\"$log\""
        }
        if (verbose!=null) {
            argsStr.add '--verbose'
            argsStr.add "\"$verbose\""
        }

        def cmd = "java -classpath \".:${classpath}\" "
        //cmd += '-verbose:class '
        if (enableAssertions) cmd += '-ea '
        cmd += main + ' ' + argsStr.join(' ')
        println main + ' ' + argsStr.join(' ')

        def proc = cmd.execute()
        if (printLog)
            //noinspection GroovyAssignabilityCheck
            proc.consumeProcessOutput System.out, System.err
    }
}
