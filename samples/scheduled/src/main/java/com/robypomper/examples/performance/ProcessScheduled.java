package com.robypomper.examples.performance;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import com.robypomper.utils.history.SampleRate;
import com.robypomper.utils.performance.Performance;
import com.robypomper.utils.performance.PerformanceCollector;
import com.robypomper.utils.performance.PerformanceViewer;
import com.robypomper.utils.performance.annotations.block.TrackBlock;
import com.robypomper.utils.performance.annotations.execution.TrackExecution;

import java.util.Random;

public class ProcessScheduled {

    private static boolean AUTO_ANALYZE = true;
    private static boolean SHOW_GUI = true;
    private static int EXEC_MIN = 100;
    private static int EXEC_MAX = 300;

    static class CmdLineArgs {
        @Parameter(names = { "--disableViewer" }, description = "if present, don't start PerformanceViewer, default false")
        private boolean disableViewer = false;

        @Parameter(names = { "--realtimeAnalyze" }, arity = 1, description = "if true, execution will analyzed for each cycle, default true")
        private boolean autoAnalyze = true;

        @Parameter(names = {"--execMin"}, description = "low bound of block execution time in ms, default 300")
        private int execMin = 100;

        @Parameter(names = {"--execMax"}, description = "hi bound of block execution time in ms, default 300")
        private int execMax = 300;
    }

    private static void parseArgs(String[] args) {
        CmdLineArgs argsParser = new CmdLineArgs();
        JCommander.newBuilder()
                .addObject(argsParser)
                .build()
                .parse(args);
        AUTO_ANALYZE = argsParser.autoAnalyze;
        SHOW_GUI = !argsParser.disableViewer;
        EXEC_MIN = argsParser.execMin;
        EXEC_MAX = argsParser.execMax;
    }

    @TrackExecution("mainScheduled")
    public static void main(String[] args) {
        parseArgs(args);

        Performance.startAnalyzerSync();
        if (SHOW_GUI) Performance.enableViewer();

        ProcessScheduled p = new ProcessScheduled();

        //noinspection InfiniteLoopStatement
        while (true) {

            // ProcSimple
            // Record a simple execution with 3 blocks

            // start recording main Execution's Block
            PerformanceCollector.startRecording("ProcSimple");

            // @TrackExecution("ProcSimple")
            // track an execution with 3 blocks
            p.executionSimple();

            // stop recording main Execution's Block
            PerformanceCollector.endRecording("ProcSimple");


            // ProcsNested
            // Record a simple execution with 3 blocks, 2nd block run a sub-execution
            // also with 3 blocks. Between 2nd and 3th block can be throw an
            // exception.

            // start recording main Execution's Block
            PerformanceCollector.startRecording("ProcsNested");

            // @TrackExecution("ProcsNested")
            // track an execution with 3 blocks ans 1 sub-execution
            try {
                p.executionsNested();
            } catch (RuntimeException ignore) {}

            // stop recording main Execution's Block
            PerformanceCollector.endRecording("ProcsNested");


            // ProcError
            // Record an execution with 3 blocks that throw an exception. The exception is
            // throed randomly 50-50 after each block.

            // start recording main Execution's Block
            PerformanceCollector.startRecording("ProcError");
            try {
                // @TrackExecution("ProcError")
                // track an execution that emit an exception
                p.executionException();
            } catch (Exception ignore) {
            }

            // stop recording main Execution's Block
            PerformanceCollector.endRecording("ProcError");


            // BlockError
            // Record an execution with 3 blocks that throw an exception inside a block. Each
            // time the block that throws the exception is choose randomly. So the sum of all
            // block's exceptions executions count must be equal to the procedure and blocks
            // executions count.

            // start recording main Execution's Block
            PerformanceCollector.startRecording("BlockError");

            // @TrackExecution("BlockError")
            // track an execution with 3 blocks and last one throw an exception
            p.executionExceptionBlock(new NullPointerException("Test exception!"));

            // stop recording main Execution's Block
            PerformanceCollector.endRecording("BlockError");


            // Execute PerformanceAnalyzer
            System.out.println("\t\t\t\t\t\t\t\t\t" + SampleRate.toFormattedDate(Performance.getMillisSince()) + " ProcessScheduled: Execute PerformanceAnalyzer");
            if (AUTO_ANALYZE) Performance.executeAnalyzer();
        }
    }


    // ----------
    // ProcSimple
    // ----------

    @TrackExecution("ProcSimple")
    private void executionSimple() {
        blockA();
        blockB();
        blockC();
    }


    // -----------
    // ProcsNested
    // -----------

    @TrackExecution("ProcsNested")
    private void executionsNested() {
        blockA();
        for (int i = (new Random().nextInt(10)+1); i>0; i--)
            blockB(this);   // Execute executionsNestedSub()

        int occurNum = new Random().nextInt(2);
        if (occurNum==0)
            throw new RuntimeException("Exception from ProcsNested");

        blockC();
    }

    @TrackExecution("ProcsNested-Sub")
    private void executionsNestedSub() {
        blockA();
        blockB();
        blockC();
    }


    // ---------
    // ProcError
    // ---------

    @TrackExecution("ProcError")
    private void executionException() {
        int exceptionNum = new Random().nextInt(3);

        blockA();
        if (exceptionNum==0)
            throw new NullPointerException("Test exception after blockA!");

        blockB();
        if (exceptionNum==1)
            throw new NullPointerException("Test exception after blockB!");

        blockC();
        if (exceptionNum==2)
          throw new NullPointerException("Test exception after blockC!");
    }


    // ----------
    // BlockError
    // ----------

    @TrackExecution("BlockError")
    private void executionExceptionBlock(Exception newE) {
        int exceptionNum = new Random().nextInt(3);

        if (exceptionNum==0)
            try {
                blockA(newE);
            } catch (Exception e) { assert e == newE: "Another object was throed."; }
        else
            blockA();


        if (exceptionNum==1)
            try {
                blockB(newE);
            } catch (Exception e) { assert e == newE: "Another object was throed."; }
        else
            blockB();

        if (exceptionNum==2)
            try {
                blockC(newE);
            } catch (Exception e) { assert e == newE: "Another object was throed."; }
        else
            blockC();
    }


    // ------
    // Blocks
    // ------

    //@TrackBlock("blockA")
    private void blockA() {
        PerformanceCollector.startRecording("blockA");
        block(EXEC_MIN,EXEC_MAX);
        PerformanceCollector.endRecording("blockA");
    }
    @TrackBlock("blockA")
    private void blockA(Exception e) throws Exception { block(EXEC_MIN,EXEC_MAX);
        throw e;
    }

    @TrackBlock("blockB")
    private void blockB() { block(EXEC_MIN,EXEC_MAX); }
    @TrackBlock("blockB")
    private void blockB(ProcessScheduled execute) { execute.executionsNestedSub(); }
    @TrackBlock("blockB")
    private void blockB(Exception e) throws Exception { block(EXEC_MIN,EXEC_MAX);
        throw e;
    }

    @TrackBlock("blockC")
    private void blockC() { block(EXEC_MIN,EXEC_MAX); }
    @TrackBlock("blockC")
    private void blockC(Exception e) throws Exception { block(EXEC_MIN,EXEC_MAX);
        throw e;
    }

    @SuppressWarnings("SameParameterValue")
    private void block(int minMs, int maxMs) {
        try {
            Thread.sleep(new Random().nextInt(maxMs - minMs) + minMs);
        } catch (InterruptedException ignore) {}
    }

}
