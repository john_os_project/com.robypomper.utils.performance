# Performance Scheduled Example
This example show how to execute a software, register his performances, analyze
and show his procedures, when it still running. This is really helpful when you
need to monitor daemon process.

To execute PerformanceAnalytics at runtime of software execution, simply add
following lines at the start of your main method.
```java
// Execute PerformanceAnalyzer
public static void main (Stirng[] args) {
    // Start Performance Analyzer Timer
    Performance.startAnalyzerSync();
    if (SHOW_GUI) Performance.enableViewer();

    // Software main method and Performance collection
    // ...
}
```
Every time you need you can execute the Performance Analyzer, than analyze and
update Performance files, calling the static method
```Performance.executeAnalyzer();```

## Examples
**ProcSimple:**

Record a simple execution with 3 blocks, always the same.

**ProcsNested:**

Record a simple execution with 3 blocks, 2nd block run a sub-execution
also with 3 blocks. The sub-execution can be run random number of times
between 1 and 10. Always 2nd block and sub-execution must have same execution
count.

**ProcError:**

Record an execution with 3 blocks that throw an exception. The exception is
throed randomly 50-50 after each block.

**BlockError:**

Record an execution with 3 blocks that throw an exception inside a block. Each
time the block that throws the exception is choose randomly. So the sum of all
block's exceptions executions count must be equal to the procedure and blocks
executions count.


## Build

To build this sample needs to include the Performance Libraries for main sourceSet
and apply Performance Library annotations.