package com.robypomper.examples.performance;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import com.robypomper.utils.performance.Performance;
import com.robypomper.utils.performance.PerformanceCollector;
import com.robypomper.utils.performance.annotations.execution.TrackExecution;


public class ProcessBase {

    private static boolean AUTO_ANALYZE = true;
    private static boolean SHOW_GUI = true;
    private static int EXEC_MIN = 100;
    private static int EXEC_MAX = 300;

    static class CmdLineArgs {
        @Parameter(names = { "--disableViewer" }, description = "if true, don't start PerformanceViewer, default false")
        private boolean disableViewer = false;

        @Parameter(names = { "--realtimeAnalyze" }, arity = 1, description = "if true, execution will analyzed for each cycle, default true")
        private boolean autoAnalyze = true;

        @Parameter(names = {"--execMin"}, description = "Low bound of block execution time")
        private int execMin = 100;

        @Parameter(names = {"--execMax"}, description = "Hi bound of block execution time")
        private int execMax = 300;
    }

    private static void parseArgs(String[] args) {
        CmdLineArgs argsParser = new CmdLineArgs();
        JCommander.newBuilder()
                .addObject(argsParser)
                .build()
                .parse(args);
        AUTO_ANALYZE = argsParser.autoAnalyze;
        SHOW_GUI = !argsParser.disableViewer;
        EXEC_MIN = argsParser.execMin;
        EXEC_MAX = argsParser.execMax;
    }

    @TrackExecution("mainBase")
    public static void main(String[] args) {
        parseArgs(args);


        // mainBase
        // Show how to record a basic execution with PerformanceCollector methods
        // It start recording main method as execution then, run 4 blocks.
        // Last blocks was not closed because it's end demonstrate how errors
        // on Performance flow definitions are manged. If assertions are enabled
        // with -ea option in the jvm, it throws an exception.
        // Blocks are recorded using different start/endRecording methods
        // Depending on passed params, this methods are thread safe or needs some
        // thread management with PerformanceCollector methods.


        //// start recording main Execution
        String execId = PerformanceCollector.registerNewExecution("mainBase");

        // Start/Stop block
        PerformanceCollector.startRecording(execId,"blockA");
        PerformanceCollector.endRecording(execId,"blockA");

        // Start/Stop block (threads safe version)
        PerformanceCollector.startRecording(execId,"blockB");
        PerformanceCollector.endRecording(execId,"blockB");

        // Start/Stop block (threads must be managed by Performance)
        PerformanceCollector.startRecording("blockC");
        PerformanceCollector.endRecording("blockC");

        // Error on Start/Stop block
        PerformanceCollector.startRecording("blockD");
        try {
            PerformanceCollector.endRecording("blockD with wrong name");
        } catch (AssertionError e) {
            System.out.println("Assertions enabled!");
            System.out.println("Assertion cached: " + e.getMessage());
        }

        // stop recording main Execution
        PerformanceCollector.terminateExecution(execId);


        // Execute PerformanceAnalyzer
        Performance.executeAnalyzer();
        if (SHOW_GUI) Performance.enableViewer();
    }

}
