# Performance Example
This sample show how use the Performance Library to track the execution of a simple
Procedure with 4 Blocks.

## Examples
This example's source code is provided by ``../annotations/src/main``.

**mainBase:**

Show how to record a basic execution with PerformanceCollector methods
It start recording main method as execution then, run 4 blocks.

Last blocks was not closed because it's end demonstrate how errors
on Performance flow definitions are manged. If assertions are enabled
with -ea option in the jvm, it throws an exception.

Blocks are recorded using different start/endRecording methods
Depending on passed params, this methods are thread safe or needs some
thread management with PerformanceCollector methods.
        
## Build

**Configure your ``build.gradle``**:

* Configure as usual, like the code below:

```gradle
    // Apply plugins
    plugins {
        id 'java'
        id 'application'
    }
    
    // Set general properties
    mainClassName = 'com.robypomper.examples.performance.ProcessBase'
    sourceCompatibility = 1.8
    targetCompatibility = 1.8
    
    repositories {
        jcenter()
    }
    
    // ...
```

* Add mavenLocal() to repository:  
  Performance Library are distribute via mavenLocal repository.

```gradle
    repositories {
        // Performance libs are provided by mavenLocal()
        mavenLocal()
        jcenter()
    }
```

* Add ``Performance-0.2`` to main sourceSet dependencies:

```gradle
    dependencies {
        implementation 'com.robypomper.utils.performance:performance:0.2'
    }
```
* Enable assertions on run task:

```gradle
tasks.run {
    // Enable assertion during development helps to detect wrong execution flows
    enableAssertions = true
}
