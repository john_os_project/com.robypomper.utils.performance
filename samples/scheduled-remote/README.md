# Performance Scheduled Remote Example
This example, similar to [Scheduled](../scheduled) sample, show how to execute a
software, register his performances and analyze his procedures. But differently
to Scheduled sample it don't show the procedure graph within the same software
process. This sample run another java process that show the procedures executed
by main Scheduled Remote Example process.
PerformanceViewer can be configured to monitor and show specific procedure's
directory. This is really helpful when you need to monitor a daemon process that
runs on another environment (virtualization, remote...).

Like in Scheduled sample, you need to start the Performance Analyzer Timer
calling the static method ```Performance.startAnalyzerSync();``` at start of
you main method.

Then you must define ```runPerformanceViewer``` Gradle task and make ```run```
task dependant to it. See Build for section more details.

## Examples
**ProcSimple:**

Record a simple execution with 3 blocks, always the same.

**ProcsNested:**

Record a simple execution with 3 blocks, 2nd block run a sub-execution
also with 3 blocks. The sub-execution can be run random number of times
between 1 and 10. Always 2nd block and sub-execution must have same execution
count.

**ProcError:**

Record an execution with 3 blocks that throw an exception. The exception is
throed randomly 50-50 after 2nd block.
Procedure and first 2 blocks have always same execution count, that's equal
to the sum of exception and 3th block execution counts.

**BlockError:**

Record an execution with 3 blocks that throw an exception inside a block. Each
time the block that throws the exception is choosed randomly. So the sum of all
block's exceptions executions count must be equal to the procedure and blocks
executions count.


## Build

To build this sample needs to include the Performance Libraries for main sourceSet
and apply Performance Library annotations.

**Exec ```PerformanceViewer``` as independent process:***

* Create custom task ```runPerformanceViewer```:  
  PerformanceViewer must be execute as non blocking Java process in Gradle project,
  so JavaExec can't be used. The Groovy standard ```String.execution()``` methods
  will be used. Java configurations are collected by ```main``` source set, that
  include the Performance Library.
```gradle
    task runPerformanceViewer() {
        group "runnables"
    
        def sampleProcPath = '.perf/procedures'
            
        def classpath = sourceSets.main.runtimeClasspath
        def main = 'com.robypomper.utils.performance.PerformanceViewer'
        def args = ['--procLoadResource',       sampleProcPath,         // procedures folders
                    //'--selectedTime',           '',                   // timestamp or '' as now()
                    '--selectedSampleRate',     'Minute',               // Some of SampleRate values
        ]
        def enableAssertions = true
    
        doFirst {
            // java cmd assembly
            def cmd = 'java -cp "' + classpath.asPath + '" '
            if (enableAssertions) cmd += '-ea '
            cmd += main + ' ' + args.join(' ')
            
            // java cmd execution
            cmd.execute()
        }
    }
```

* Make task ```run``` dependant to ```runPerformanceViewer```:  
  Scheduled Remote sample is based on Scheduled sample that show by default
  internal PerformanceViewer, in this sample it's necessary disable the
  internal viewer.
```gradle
tasks.run {
    dependsOn runPerformanceViewer
    args = [
            '--disableViewer',              // if present, don't start PerformanceViewer, default false
    ]

    enableAssertions = true;
}