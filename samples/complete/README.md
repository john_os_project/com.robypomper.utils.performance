# Performance Complete Example
This sample show how Performance Library can record different procedures as a
complete example.

## Examples
**ProcSimple:**

Record a simple execution with 3 blocks.

**ProcsNested:**

Record a simple execution with 3 blocks, 2nd block run a sub-execution
also with 3 blocks.

**ProcError:**

Record an execution with 3 blocks that throw an exception after run 3th block.

**BlockError:**

Record an execution with 3 blocks that throw an exception inside the 3th block.

## Build

To build this sample needs to include the Performance Libraries for main sourceSet
and apply Performance Library annotations.