# Performance Single Example
This example show how to execute a software, register his performances,
and, when it's terminated, analyze and show his procedures. This is really helpful
because prevent CPU or other resource consumption when analysis are not needed during
software elaboration.

To execute PerformanceAnalytics at the end of software execution, simply add
following lines at the end of your main method.
```java
// Execute PerformanceAnalyzer
public static void main (Stirng[] args) {

    // Software main method and Performance collection
    // ...

    // Execute Performance Analyzer
    Performance.executeAnalyzer();
    if (SHOW_GUI) Performance.enableViewer();
    System.exit(EXIT_CODE);
}
```

## Examples
**ProcSimple:**

Record a simple execution with 3 blocks, always the same.

**ProcsNested:**

Record a simple execution with 3 blocks, 2nd block run a sub-execution
also with 3 blocks. The sub-execution can be run random number of times
between 1 and 10. Always 2nd block and sub-execution must have same execution
count.

**ProcError:**

Record an execution with 3 blocks that throw an exception. The exception is
throed randomly 50-50 after 2nd block.
Procedure and first 2 blocks have always same execution count, that's equal
to the sum of exception and 3th block execution counts.

**BlockError:**

Record an execution with 3 blocks that throw an exception inside a block. Each
time the block that throws the exception is choose randomly. So the sum of all
block's exceptions executions count must be equal to the procedure and blocks
executions count.


## Build

To build this sample needs to include the Performance Libraries for main sourceSet
and apply Performance Library annotations.