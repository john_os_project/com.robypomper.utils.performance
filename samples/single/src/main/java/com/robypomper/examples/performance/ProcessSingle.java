package com.robypomper.examples.performance;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import com.robypomper.utils.performance.Performance;
import com.robypomper.utils.performance.PerformanceCollector;
import com.robypomper.utils.performance.annotations.block.TrackBlock;
import com.robypomper.utils.performance.annotations.execution.TrackExecution;

import java.util.Random;

public class ProcessSingle {
    private static boolean AUTO_ANALYZE = true;
    private static boolean SHOW_GUI = true;
    private static int EXEC_MIN = 100;
    private static int EXEC_MAX = 300;

    static class CmdLineArgs {
        @Parameter(names = { "--disableViewer" }, description = "if true, don't start PerformanceViewer, default false")
        private boolean disableViewer = false;

        @Parameter(names = { "--realtimeAnalyze" }, arity = 1, description = "if true, execution will analyzed for each cycle, default true")
        private boolean autoAnalyze = true;

        @Parameter(names = {"--execMin"}, description = "Low bound of block execution time")
        private int execMin = 100;

        @Parameter(names = {"--execMax"}, description = "Hi bound of block execution time")
        private int execMax = 300;
    }

    private static void parseArgs(String[] args) {
        CmdLineArgs argsParser = new CmdLineArgs();
        JCommander.newBuilder()
                .addObject(argsParser)
                .build()
                .parse(args);
        AUTO_ANALYZE = argsParser.autoAnalyze;
        SHOW_GUI = !argsParser.disableViewer;
        EXEC_MIN = argsParser.execMin;
        EXEC_MAX = argsParser.execMax;
    }

    @TrackExecution("mainSingle")
    public static void main(String[] args) {
        parseArgs(args);

        ProcessSingle p = new ProcessSingle();


        // ProcSimple
        // Record a simple execution with 3 blocks

        // start recording main Execution's Block
        PerformanceCollector.startRecording("ProcSimple");

        // @TrackExecution("ProcSimple")
        // track an execution with 3 blocks
        p.executionSimple();

        // stop recording main Execution's Block
        PerformanceCollector.endRecording("ProcSimple");


        // ProcsNested
        // Record a simple execution with 3 blocks, 2nd block run a sub-execution
        // also with 3 blocks

        // start recording main Execution's Block
        PerformanceCollector.startRecording("ProcsNested");

        // @TrackExecution("ProcsNested")
        // track an execution with 3 blocks ans 1 sub-execution
        p.executionsNested();

        // stop recording main Execution's Block
        PerformanceCollector.endRecording("ProcsNested");


        // ProcError
        // Record an execution that throw an exception after run 3 blocks

        // start recording main Execution's Block
        PerformanceCollector.startRecording("ProcError");
        try {
            // @TrackExecution("ProcError")
            // track an execution that emit an exception
            p.executionException();
        } catch (Exception ignore) { }

        // stop recording main Execution's Block
        PerformanceCollector.endRecording("ProcError");


        // BlockError
        // Record an execution that throw an exception after run 3 blocks

        // start recording main Execution's Block
        PerformanceCollector.startRecording("BlockError");

        // @TrackExecution("BlockError")
        // track an execution with 3 blocks and last one throw an exception
        p.executionExceptionBlock(new NullPointerException("Test exception!"));

        // stop recording main Execution's Block
        PerformanceCollector.endRecording("BlockError");


        // Execute PerformanceAnalyzer
        Performance.executeAnalyzer();
        if (SHOW_GUI) Performance.enableViewer();
    }


    // ----------
    // ProcSimple
    // ----------

    @TrackExecution("ProcSimple")
    private void executionSimple() {
        blockA();
        blockB();
        blockC();
    }


    // -----------
    // ProcsNested
    // -----------

    @TrackExecution("ProcsNested")
    private void executionsNested() {
        blockA();
        for (int i = (new Random().nextInt(10)+1); i>0; i--)
            blockB(this);   // Execute executionsNestedSub()
        blockC();
    }

    @TrackExecution("ProcsNested-Sub")
    private void executionsNestedSub() {
        blockA();
        blockB();
        blockC();
    }


    // ---------
    // ProcError
    // ---------

    @TrackExecution("ProcError")
    private void executionException() {
        int exceptionNum = new Random().nextInt(3);

        blockA();
        if (exceptionNum==0)
            throw new NullPointerException("Test exception after blockA!");
        blockB();
        if (exceptionNum==1)
            throw new NullPointerException("Test exception after blockB!");
        blockC();
        if (exceptionNum==2)
            throw new NullPointerException("Test exception after blockC!");
    }


    // ----------
    // BlockError
    // ----------

    @TrackExecution("BlockError")
    private void executionExceptionBlock(Exception newE) {
        int exceptionNum = new Random().nextInt(3);

        if (exceptionNum==0)
            try {
                blockA(newE);
            } catch (Exception e) { assert e == newE: "Another object was throed."; }
        else
            blockA();


        if (exceptionNum==1)
            try {
                blockB(newE);
            } catch (Exception e) { assert e == newE: "Another object was throed."; }
        else
            blockB();

        if (exceptionNum==2)
            try {
                blockC(newE);
            } catch (Exception e) { assert e == newE: "Another object was throed."; }
        else
            blockC();
    }


    // ------
    // Blocks
    // ------

    @TrackBlock("blockA")
    private void blockA() { block(EXEC_MIN,EXEC_MAX); }
    @TrackBlock("blockA")
    private void blockA(Exception e) throws Exception { block(EXEC_MIN,EXEC_MAX);
        throw e;
    }

    @TrackBlock("blockB")
    private void blockB() { block(EXEC_MIN,EXEC_MAX); }
    @TrackBlock("blockB")
    private void blockB(ProcessSingle execute) { execute.executionsNestedSub(); }
    @TrackBlock("blockB")
    private void blockB(Exception e) throws Exception { block(EXEC_MIN,EXEC_MAX);
        throw e;
    }

    @TrackBlock("blockC")
    private void blockC() { block(EXEC_MIN,EXEC_MAX); }
    @TrackBlock("blockC")
    private void blockC(Exception e) throws Exception { block(EXEC_MIN,EXEC_MAX);
        throw e;
    }

    @SuppressWarnings("SameParameterValue")
    private void block(int minMs, int maxMs) {
        try {
            Thread.sleep(new Random().nextInt(maxMs - minMs) + minMs);
        } catch (InterruptedException ignore) {}
    }

}
