# Errors handling
This sample show how Performance Library can record exception and provide
stats about them.

## Examples
**ProcException:**

This Execution throw an exception outside procedure block's and it's
caught-recorded-throed by @TrackExecution annotation.
        

**BlockException:**

This Execution throw an exception in the last procedure block's and it's
caught-recorded-throed by @TrackBlock annotation.

## Build

To build this sample needs to include the Performance Libraries for main sourceSet
and apply Performance Library annotations.