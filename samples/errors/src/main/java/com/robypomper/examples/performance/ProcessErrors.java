package com.robypomper.examples.performance;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import com.robypomper.utils.performance.Performance;
import com.robypomper.utils.performance.PerformanceCollector;
import com.robypomper.utils.performance.annotations.block.TrackBlock;
import com.robypomper.utils.performance.annotations.execution.TrackExecution;

import java.util.Random;

public class ProcessErrors {

    private static boolean AUTO_ANALYZE = true;
    private static boolean SHOW_GUI = true;
    private static int EXEC_MIN = 100;
    private static int EXEC_MAX = 300;

    static class CmdLineArgs {
        @Parameter(names = { "--disableViewer" }, description = "if true, don't start PerformanceViewer, default false")
        private boolean disableViewer = false;

        @Parameter(names = { "--realtimeAnalyze" }, arity = 1, description = "if true, execution will analyzed for each cycle, default true")
        private boolean autoAnalyze = true;

        @Parameter(names = {"--execMin"}, description = "Low bound of block execution time")
        private int execMin = 100;

        @Parameter(names = {"--execMax"}, description = "Hi bound of block execution time")
        private int execMax = 300;
    }

    private static void parseArgs(String[] args) {
        CmdLineArgs argsParser = new CmdLineArgs();
        JCommander.newBuilder()
                .addObject(argsParser)
                .build()
                .parse(args);
        AUTO_ANALYZE = argsParser.autoAnalyze;
        SHOW_GUI = !argsParser.disableViewer;
        EXEC_MIN = argsParser.execMin;
        EXEC_MAX = argsParser.execMax;
    }

    @TrackExecution("mainErrors")
    public static void main(String[] args) {
        parseArgs(args);

        ProcessErrors p = new ProcessErrors();


        // ProcException (6 times)
        // This Execution throw an exception outside procedure block's and it's
        // caught-recorded-throed by @TrackExecution annotation.

        for (int i = 0; i<6; i++) {
            // start recording main Execution's Block
            PerformanceCollector.startRecording("ProcException");

            try {
                // @TrackExecution("ProcException")
                // this method will trow an exception, after running successfully 2 blocks
                p.executionException();
            } catch (Exception ignored) {}

            // stop recording main Execution's Block
            PerformanceCollector.endRecording("ProcException");
        }

        // BlockException
        // This Execution throw an exception in the last procedure block's and it's
        // caught-recorded-throed by @TrackBlock annotation.

        // start recording main Execution's Block
        PerformanceCollector.startRecording("BlockException");

        // @TrackExecution("BlockException")
        // execute 3 blocks and the 3th one will throw an exception
        p.executionExceptionBlock(new NullPointerException("Test exception!"));

        // stop recording main Execution's Block
        PerformanceCollector.endRecording("BlockException");


        // Execute PerformanceAnalyzer
        Performance.executeAnalyzer();
        if (SHOW_GUI) Performance.enableViewer();
    }



    // -------------
    // ProcException
    // -------------

    @TrackExecution("ProcException")
    private void executionException() {
        int exceptionNum = new Random().nextInt(3);

        blockA();
        if (exceptionNum==0)
            throw new NullPointerException("Test exception after blockA!");

        blockB();
        if (exceptionNum==1)
            throw new NullPointerException("Test exception after blockB!");

        blockC();
        if (exceptionNum==2)
          throw new NullPointerException("Test exception after blockC!");
    }


    // --------------
    // BlockException
    // --------------

    @TrackExecution("BlockException")
    private void executionExceptionBlock(Exception newE) {
        blockA();
        blockB();

        try {
            blockC(newE);
        } catch (Exception e) {
            assert e == newE: "Another object was throed.";
        }
    }


    // ------
    // Blocks
    // ------

    @TrackBlock("blockA")
    private void blockA() { block(EXEC_MIN,EXEC_MAX); }

    @TrackBlock("blockB")
    private void blockB() { block(EXEC_MIN,EXEC_MAX); }

    @TrackBlock("blockC")
    private void blockC() { block(EXEC_MIN,EXEC_MAX); }
    @TrackBlock("blockC with Excpetion")
    private void blockC(Exception e) throws Exception {
        block(EXEC_MIN,EXEC_MAX);
        throw e;
    }

    @SuppressWarnings("SameParameterValue")
    private void block(int minMs, int maxMs) {
        try {
            Thread.sleep(new Random().nextInt(maxMs - minMs) + minMs);
        } catch (InterruptedException ignore) {}
    }

}
