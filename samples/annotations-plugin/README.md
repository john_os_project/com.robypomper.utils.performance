# Performance Annotations Plugin
This sample show how use annotations in source code to track his execution via
Performance Library.

The Build file is configured to use the Performance Gradle plugin to apply
Performance Library's annotations with the provided task
``com.robypomper.utils.performance.PerformanceAnnotationsTask``.

## Examples
This example's source code is provided by ``../annotations/src/main``.

**AnnotationsExamples:**

Show how to record a couple of simples execution via annotations:
* First execution is registered using the trackExecution annotation
* The second execution, is split in two methods, first start the
  execution and second one end's it.
        
**AnnotationsNaming:**

Annotations accepts nameType param to determinate the Execution/Block's
name. This example show how to apply different naming rules and their
results

## Build

**Enable Annotations via ``performance-gradle-plugin``**:

* Add performance-gradle-plugin on buildscript:

```gradle
    buildscript {
        repositories {
            // Performance libs are provided by mavenLocal()
            mavenLocal()
        }
        dependencies {
            classpath 'com.robypomper.utils.performance:performance-gradle-plugin:0.2'
        }
    }
```

* Create task ``compileAjcJava``:  
  Task must be of type ``com.robypomper.utils.performance.PerformanceAnnotationsTask`` and
  just set the sourceSet to apply the Performance annotations on.

```gradle
    task compileAjcJava(type: com.robypomper.utils.performance.PerformanceAnnotationsTask) {
        sourceSet = sourceSets.main
    }
```
