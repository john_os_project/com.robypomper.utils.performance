package com.robypomper.utils.performance.performancecollector;

import com.robypomper.utils.performance.Performance;
import com.robypomper.utils.performance.PerformanceCollector;
import com.robypomper.utils.performance.annotations.block.TrackBlock;
import com.robypomper.utils.performance.annotations.execution.TrackExecution;

/**
 * Example source code used in README.md:Performance usage:Executions registration
 * of Performance Project.
 */
class BaseObject {

    private static final String BLOCK_INIT = "Main Init Block";
    private static final String BLOCK_EXEC = "Main Exec Block";

    @TrackExecution("mainBaseObject")
    public static void main (String[] args) {
        String[] names = {"Marco","Fabrizio","Alessandro",""};

        PerformanceCollector.startRecording(BLOCK_INIT);
        BaseObject obj = new BaseObject();
        PerformanceCollector.endRecording(BLOCK_INIT);

        PerformanceCollector.startRecording(BLOCK_EXEC);
        for (String name : names)
            obj.execution(name);
        PerformanceCollector.endRecording(BLOCK_EXEC);

        PerformanceCollector.terminateExecution();
        Performance.executeAnalyzer();      // de-comment to execute PerformanceAnalyzer
        Performance.enableViewer();         // de-comment to show PerformanceViewer
    }


    // ----
    // Init
    // ----

    @TrackExecution("Init")
    private BaseObject() {
        initA();
        initB();
    }

    @TrackBlock()
    private void initA() {
        // do init staff...
    }

    @TrackBlock()
    private void initB() {
        // do init staff...
    }


    // ---------
    // Execution
    // ---------

    private static final String EXECSTUFF = "ExecStuff";
    private static final String EXECSTUFF_ANONYMOUS = "ExecStuff(Anonymous)";

    @TrackExecution()
    private void execution(String name) {
        execBefore();

        PerformanceCollector.startRecording(EXECSTUFF);
        // do exec stuff...
        System.out.println(String.format("Hello %s!", name));
        PerformanceCollector.endRecording(EXECSTUFF);

        execAfter();
    }

    @TrackExecution()
    private void execution2(String name) {
        execBefore();

        if (!name.isEmpty()) {
            PerformanceCollector.startRecording(EXECSTUFF);
            System.out.println(String.format("Hello %s!", name));
            PerformanceCollector.endRecording(EXECSTUFF);

        } else {
            PerformanceCollector.startRecording(EXECSTUFF_ANONYMOUS);
            System.out.println(String.format("Hello my friend!"));
            PerformanceCollector.endRecording(EXECSTUFF_ANONYMOUS);

        }

        execAfter();
    }

    @TrackBlock()
    private void execBefore() {
        // do exec stuff...
    }

    @TrackBlock()
    private void execAfter() {
        // do exec stuff...
    }
}