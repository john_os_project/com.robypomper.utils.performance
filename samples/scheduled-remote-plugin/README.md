# Performance Scheduled Remote Plugin Example
This example, in addition to [ScheduledRemote](../scheduled-remote) sample, it
can execute the PerformanceViewer via Performance Gradle Plugin.

Performance Gradle Plugin provide task ``com.robypomper.utils.performance.PerformanceViewerTask``
that execute PerformanceViewer in a independent process than the Gradle execution.

## Examples
**ProcSimple:**

Record a simple execution with 3 blocks, always the same.

**ProcsNested:**

Record a simple execution with 3 blocks, 2nd block run a sub-execution
also with 3 blocks. The sub-execution can be run random number of times
between 1 and 10. Always 2nd block and sub-execution must have same execution
count.

**ProcError:**

Record an execution with 3 blocks that throw an exception. The exception is
throed randomly 50-50 after 2nd block.
Procedure and first 2 blocks have always same execution count, that's equal
to the sum of exception and 3th block execution counts.

**BlockError:**

Record an execution with 3 blocks that throw an exception inside a block. Each
time the block that throws the exception is choosed randomly. So the sum of all
block's exceptions executions count must be equal to the procedure and blocks
executions count.


## Build

**Enable Annotations via ``performance-gradle-plugin``**:

* Add performance-gradle-plugin on buildscript:

```gradle
    buildscript {
        repositories {
            // Performance libs are provided by mavenLocal()
            mavenLocal()
        }
        dependencies {
            classpath 'com.robypomper.utils.performance:performance-gradle-plugin:0.2'
        }
    }
```

* Create task ``runPerformanceViewer``:  
  Task must be of type ``com.robypomper.utils.performance.PerformanceViewerTask`` and
  just set the runTask and needed properties.

```gradle
    task runPerformanceViewer(type: com.robypomper.utils.performance.PerformanceViewerTask) {
        setRunTask run
        //proceduresPath = '.perf/procedures'
    }
```
