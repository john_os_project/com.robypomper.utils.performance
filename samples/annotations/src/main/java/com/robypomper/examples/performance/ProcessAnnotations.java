package com.robypomper.examples.performance;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import com.robypomper.utils.performance.Performance;
import com.robypomper.utils.performance.PerformanceCollector;
import com.robypomper.utils.performance.annotations.aspect.PerformanceAspect;
import com.robypomper.utils.performance.annotations.block.TrackBlock;
import com.robypomper.utils.performance.annotations.execution.EndExecution;
import com.robypomper.utils.performance.annotations.execution.StartExecution;
import com.robypomper.utils.performance.annotations.execution.TrackExecution;

import java.util.Random;


@SuppressWarnings("unused")
public class ProcessAnnotations {

    private static String strParamVal = "strParamVal";
    private static boolean AUTO_ANALYZE = true;
    private static boolean SHOW_GUI = true;
    private static int EXEC_MIN = 100;
    private static int EXEC_MAX = 300;

    static class CmdLineArgs {
        @Parameter(names = { "--disableViewer" }, description = "if true, don't start PerformanceViewer, default false")
        private boolean disableViewer = false;

        @Parameter(names = { "--realtimeAnalyze" }, arity = 1, description = "if true, execution will analyzed for each cycle, default true")
        private boolean autoAnalyze = true;

        @Parameter(names = {"--execMin"}, description = "Low bound of block execution time")
        private int execMin = 100;

        @Parameter(names = {"--execMax"}, description = "Hi bound of block execution time")
        private int execMax = 300;
    }

    private static void parseArgs(String[] args) {
        CmdLineArgs argsParser = new CmdLineArgs();
        JCommander.newBuilder()
                .addObject(argsParser)
                .build()
                .parse(args);
        AUTO_ANALYZE = argsParser.autoAnalyze;
        SHOW_GUI = !argsParser.disableViewer;
        EXEC_MIN = argsParser.execMin;
        EXEC_MAX = argsParser.execMax;
    }

    @TrackExecution("mainAnnotations")
    public static void main(String[] args) {
        parseArgs(args);

        ProcessAnnotations p = new ProcessAnnotations();


        // AnnotationsExample
        // Show how to record a couple of simples execution via annotations:
        // First execution is registered using the trackExecution annotation
        // The second execution, is split in two methods, first start the
        // execution and second one end's it.

        // start recording main Execution's Block
        PerformanceCollector.startRecording("AnnotationsExample");

        // @TrackExecution("ExecutionExample")
        // start and stop execution tracking within annotated method
        p.execution();

        // @StartExecution("ExecutionExample_StartStop")
        // start execution tracking within annotated method
        p.executionStart();
        // @EndExecution("ExecutionExample_StartStop")
        // stop execution tracking within annotated method
        p.executionEnd();

        // stop recording main Execution's Block
        PerformanceCollector.endRecording("AnnotationsExample");


        // AnnotationsNaming
        // Annotations accepts nameType param to determinate the Execution/Block's
        // name. This example show how to apply different naming rules and their
        // results

        // start recording main Execution's Block
        PerformanceCollector.startRecording("AnnotationsNaming");

        // @TrackExecution()
        // public void executionAnnotated(String val)
        // >"com.f....ProcessAnnotations::executionAnnotated"
        p.executionAnnotated(strParamVal);

        // @TrackExecution("executionAnnotatedWithName-")
        // public void executionAnnotatedWithName(String val)
        // >"executionAnnotatedWithName-"
        p.executionAnnotatedWithName(strParamVal);

        // @TrackExecution(nameType = PerformanceNameType.CUSTOM)
        // public void executionAnnotatedTypeCUSTOM(String val)
        // >""
        p.executionAnnotatedTypeCUSTOM(strParamVal);

        // @TrackExecution(nameType = PerformanceNameType.METHOD_SIGNATURE)
        // public void executionAnnotatedTypeMETHOD_SIGNATURE(String val)
        // >"com.f....ProcessAnnotations::executionAnnotatedTypeMETHOD_PARAMS_VALUES(strParamVal)"
        p.executionAnnotatedTypeMETHOD_PARAMS_VALUES(strParamVal);

        // @TrackExecution(nameType = PerformanceNameType.METHOD_PARAMS_VALUES)
        // public void executionAnnotatedTypeMETHOD_PARAMS_VALUES(String val)
        // >"com.f....ProcessAnnotations::executionAnnotatedTypeMETHOD_SIGN_AND_VALUES(java.lang.String::strParamVal)"
        p.executionAnnotatedTypeMETHOD_SIGN_AND_VALUES(strParamVal);

        // @TrackExecution(nameType = PerformanceNameType.METHOD_SIGN_AND_VALUES)
        // public void executionAnnotatedTypeMETHOD_SIGN_AND_VALUES(String val)
        // >"com.f....ProcessAnnotations::executionAnnotatedTypeMETHOD_SIGNATURE(java.lang.String)"
        p.executionAnnotatedTypeMETHOD_SIGNATURE(strParamVal);

        // stop recording main Execution's Block
        PerformanceCollector.endRecording("AnnotationsNaming");


        // AnnotationsNaming_Blocks
        // As previuous example but on blocks
        // Example disabled because exception and block's annotations are equals

        //// start recording main Execution's Block
        //PerformanceCollector.startRecording("AnnotationsNaming_Blocks");
        //
        //p.blocksAnnotated(strParamVal);
        //p.blocksAnnotatedWithName(strParamVal);
        //p.blocksAnnotatedTypeCUSTOM(strParamVal);
        //p.blocksAnnotatedTypeMETHOD_PARAMS_VALUES(strParamVal);
        //p.blocksAnnotatedTypeMETHOD_SIGN_AND_VALUES(strParamVal);
        //p.blocksAnnotatedTypeMETHOD_SIGNATURE(strParamVal);
        //
        //// stop recording main Execution's Block
        //PerformanceCollector.endRecording("TrackBlocks");


        // Execute PerformanceAnalyzer
        Performance.executeAnalyzer();
        if (SHOW_GUI) Performance.enableViewer();
    }


    // ------------------
    // AnnotationsExample
    // ------------------

    @TrackExecution("ExecutionExample")
    private void execution() {
        blockA(strParamVal);
        blockB(strParamVal);
        blockC(strParamVal);
    }

    @StartExecution("ExecutionExample_StartStop")
    private void executionStart() {
        blockA(strParamVal);
        blockB(strParamVal);
    }

    @EndExecution("ExecutionExample_StartStop")
    private void executionEnd() {
        blockC(strParamVal);
    }


    // -----------------
    // AnnotationsNaming
    // -----------------

    @TrackExecution()
    private void executionAnnotated(String val) {
        blockA(strParamVal);
        blockB(strParamVal);
        blockC(strParamVal);
    }

    @TrackExecution("executionAnnotatedWithName-")
    private void executionAnnotatedWithName(String val) {
        blockA(strParamVal);
        blockB(strParamVal);
        blockC(strParamVal);
    }

    @TrackExecution(nameType = PerformanceAspect.PerformanceNameType.CUSTOM)
    private void executionAnnotatedTypeCUSTOM(String val) {
        blockA(strParamVal);
        blockB(strParamVal);
        blockC(strParamVal);
    }

    @TrackExecution(nameType = PerformanceAspect.PerformanceNameType.METHOD_SIGNATURE)
    private void executionAnnotatedTypeMETHOD_SIGNATURE(String val) {
        blockA(strParamVal);
        blockB(strParamVal);
        blockC(strParamVal);
    }

    @TrackExecution(nameType = PerformanceAspect.PerformanceNameType.METHOD_PARAMS_VALUES)
    private void executionAnnotatedTypeMETHOD_PARAMS_VALUES(String val) {
        blockA(strParamVal);
        blockB(strParamVal);
        blockC(strParamVal);
    }

    @TrackExecution(nameType = PerformanceAspect.PerformanceNameType.METHOD_SIGN_AND_VALUES)
    private void executionAnnotatedTypeMETHOD_SIGN_AND_VALUES(String val) {
        blockA(strParamVal);
        blockB(strParamVal);
        blockC(strParamVal);
    }


//    // ------------------------
//    // AnnotationsNaming_Blocks
//    // ------------------------
//
//    @TrackExecution()
//    private void blocksAnnotated(String val) {
//        blockANoParams(strParamVal);
//        blockBNoParams(strParamVal);
//        blockCNoParams(strParamVal);
//    }
//
//    @TrackExecution("blocksAnnotatedWithName-")
//    private void blocksAnnotatedWithName(String val) {
//        blockAWithName(strParamVal);
//        blockBWithName(strParamVal);
//        blockCWithName(strParamVal);
//    }
//
//    @TrackExecution(nameType = PerformanceNameType.CUSTOM)
//    private void blocksAnnotatedTypeCUSTOM(String val) {
//        blockATypeCUSTOM(strParamVal);
//        blockBTypeCUSTOM(strParamVal);
//        blockCTypeCUSTOM(strParamVal);
//    }
//
//    @TrackExecution(nameType = PerformanceNameType.METHOD_SIGNATURE)
//    private void blocksAnnotatedTypeMETHOD_SIGNATURE(String val) {
//        blockATypeMETHOD_SIGNATURE(strParamVal);
//        blockBTypeMETHOD_SIGNATURE(strParamVal);
//        blockCTypeMETHOD_SIGNATURE(strParamVal);
//    }
//    @TrackExecution(nameType = PerformanceNameType.METHOD_PARAMS_VALUES)
//    private void blocksAnnotatedTypeMETHOD_PARAMS_VALUES(String val) {
//        blockATypeMETHOD_PARAMS_VALUES(strParamVal);
//        blockBTypeMETHOD_PARAMS_VALUES(strParamVal);
//        blockCTypeMETHOD_PARAMS_VALUES(strParamVal);
//    }
//    @TrackExecution(nameType = PerformanceNameType.METHOD_SIGN_AND_VALUES)
//    private void blocksAnnotatedTypeMETHOD_SIGN_AND_VALUES(String val) {
//        blockATypeMETHOD_SIGN_AND_VALUES(strParamVal);
//        blockBTypeMETHOD_SIGN_AND_VALUES(strParamVal);
//        blockCTypeMETHOD_SIGN_AND_VALUES(strParamVal);
//    }


    // ------
    // Blocks
    // ------

    @TrackBlock("blockA")
    private void blockA(String strParam) { block(EXEC_MIN,EXEC_MAX); }
//    @TrackBlock()
//    private void blockANoParams(String strParam) { block(EXEC_MIN,EXEC_MAX); }
//    @TrackBlock("blockAWithName")
//    private void blockAWithName(String strParam) { block(EXEC_MIN,EXEC_MAX); }
//    @TrackBlock(nameType = PerformanceNameType.CUSTOM)
//    private void blockATypeCUSTOM(String strParam) { block(EXEC_MIN,EXEC_MAX); }
//    @TrackBlock(nameType = PerformanceNameType.METHOD_SIGNATURE)
//    private void blockATypeMETHOD_SIGNATURE(String strParam) { block(EXEC_MIN,EXEC_MAX); }
//    @TrackBlock(nameType = PerformanceNameType.METHOD_PARAMS_VALUES)
//    private void blockATypeMETHOD_PARAMS_VALUES(String strParam) { block(EXEC_MIN,EXEC_MAX); }
//    @TrackBlock(nameType = PerformanceNameType.METHOD_SIGN_AND_VALUES)
//    private void blockATypeMETHOD_SIGN_AND_VALUES(String strParam) { block(EXEC_MIN,EXEC_MAX); }

    @TrackBlock("blockB")
    private void blockB(String strParam) { block(EXEC_MIN,EXEC_MAX); }
//    @TrackBlock()
//    private void blockBNoParams(String strParam) { block(EXEC_MIN,EXEC_MAX); }
//    @TrackBlock("blockBWithName")
//    private void blockBWithName(String strParam) { block(EXEC_MIN,EXEC_MAX); }
//    @TrackBlock(nameType = PerformanceNameType.CUSTOM)
//    private void blockBTypeCUSTOM(String strParam) { block(EXEC_MIN,EXEC_MAX); }
//    @TrackBlock(nameType = PerformanceNameType.METHOD_SIGNATURE)
//    private void blockBTypeMETHOD_SIGNATURE(String strParam) { block(EXEC_MIN,EXEC_MAX); }
//    @TrackBlock(nameType = PerformanceNameType.METHOD_PARAMS_VALUES)
//    private void blockBTypeMETHOD_PARAMS_VALUES(String strParam) { block(EXEC_MIN,EXEC_MAX); }
//    @TrackBlock(nameType = PerformanceNameType.METHOD_SIGN_AND_VALUES)
//    private void blockBTypeMETHOD_SIGN_AND_VALUES(String strParam) { block(EXEC_MIN,EXEC_MAX); }

    @TrackBlock("blockC")
    private void blockC(String strParam) { block(EXEC_MIN,EXEC_MAX); }
//    @TrackBlock()
//    private void blockCNoParams(String strParam) { block(EXEC_MIN,EXEC_MAX); }
//    @TrackBlock("blockCWithName")
//    private void blockCWithName(String strParam) { block(EXEC_MIN,EXEC_MAX); }
//    @TrackBlock(nameType = PerformanceNameType.CUSTOM)
//    private void blockCTypeCUSTOM(String strParam) { block(EXEC_MIN,EXEC_MAX); }
//    @TrackBlock(nameType = PerformanceNameType.METHOD_SIGNATURE)
//    private void blockCTypeMETHOD_SIGNATURE(String strParam) { block(EXEC_MIN,EXEC_MAX); }
//    @TrackBlock(nameType = PerformanceNameType.METHOD_PARAMS_VALUES)
//    private void blockCTypeMETHOD_PARAMS_VALUES(String strParam) { block(EXEC_MIN,EXEC_MAX); }
//    @TrackBlock(nameType = PerformanceNameType.METHOD_SIGN_AND_VALUES)
//    private void blockCTypeMETHOD_SIGN_AND_VALUES(String strParam) { block(EXEC_MIN,EXEC_MAX); }

    @SuppressWarnings("SameParameterValue")
    private void block(int minMs, int maxMs) {
        try {
            Thread.sleep(new Random().nextInt(maxMs-minMs) + minMs);
        } catch (InterruptedException ignore) {}
    }

}
