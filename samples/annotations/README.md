# Performance Annotations
This sample show how use annotations in source code to track his execution via
Performance Library.

The Build file is configured to apply Performance Library's annotations with the
custom task ``compileAjcJava``.

## Examples
**AnnotationsExamples:**

Show how to record a couple of simples execution via annotations:
- First execution is registered using the trackExecution annotation
- The second execution, is split in two methods, first start the
execution and second one end's it.
        
**AnnotationsNaming:**

Annotations accepts nameType param to determinate the Execution/Block's
name. This example show how to apply different naming rules and their
results

## Build

**Enable Annotations**:

* Create configurations with following dependencies:

```gradle
    configurations {
        ajc             // Aspects tools required by compileAjcJava task to load ant.iajc task
        aspects         // Aspects and Performance libraries required by the AspectJCompiler as apectPath
        aspectCompile   // Aspects and Performance libraries required by the AspectJCompiler as classpath
    }
    
    dependencies {
        ajc            'org.aspectj:aspectjtools:1.9.+'
    
        aspects        'org.springframework:spring-aspects:5.1.+'
        aspects        'com.robypomper.utils.performance:performance:0.2'
    
        aspectCompile  'javax.persistence:persistence-api:1.0'
        aspectCompile  'org.springframework:spring-tx:5.1.+'
        aspectCompile  'org.springframework:spring-orm:5.1.+'
        aspectCompile  'org.springframework:spring-aop:5.1.+'
        aspectCompile  'org.springframework:spring-context:5.1.+'
    }
```

* Create task ``compileAjcJava``  
   Task ``classes`` dependsOn ``compileAjcJava`` that must be run ofter ``compileJava``<br>
   This task execute the ant's task ``iajc`` imported from ``aspectjtools``
   dependency and configurated with following params:

```gradle
    ant.iajc(
            destDir:                        sourceSets.main.java.outputDir.absolutePath,
            aspectPath:                     configurations.aspects.asPath,
            inpath:                         sourceSets.main.java.outputDir.absolutePath,
            classpath:                      sourceSets.main.compileClasspath.asPath + ';'
                                              + configurations.aspectCompile.asPath,
            maxmem: '1024m',
            fork: 'true',
            Xlint: 'warning', //'info',
            source: rootProject.sourceCompatibility,
            target: rootProject.targetCompatibility,
            //verbose: 'true',
            //showWeaveInfo: 'true'
    )
```
