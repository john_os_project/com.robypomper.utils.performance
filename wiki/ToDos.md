# ToDos

**todo**: bold means for 0.2 release

**General:**

* **remove all '-DEV' from '0.2-DEV' versions**
* collect all samples source code in samples sourceset
* arrange publications as described in README.md
* general design on logs and error handling

**Documentation:**

* aggiungere immagini generate da PerformanceViewer in README.md
* Mettere online la documentazione javadoc
* Documentare tutte le classi linkate (@ javadoc) da README.md
* Sistemare links tra md(s) e @ javadoc
* wiki/ExecutionsAndBlocks.md: descrivere Procedures&Blocks con diagrammi
* wiki/PublicationsContent.md: Report localPublish.gradle configs and jars combination
* wiki/Annotations.md: Different annotations naming types
* annotare versione (since) dei metodi pubblici della libreria (classi Performance e PerformanceCollector)
* rimuovere tutti i br dai file *.md

**PerformanceCollector:**

* **Migliorare registrazione eccezioni**  
  quando viene emessa e registrata un'eccezione, aggiungerla a blockSequencesStats
  un'eccezione viene collegata allo stack interno della procedura cosi sarà
  possibile analizzare lo stack della procedura con e senza errori.
* Migliorare gestione errori nelle chiamate interne:  
  ogni eccezione deve essere cattuata a trasformata in assertion.

**PerformanceAnalyzer:**

* Creare un sourceset dedicato ai Checker e pubblicare una o più librerie dedicate

**PerformanceViewer:**

* **Colorare le exception in rosso**
* **Implementare metodo update, per rimuovere sfarfallio**
* aggiungere supporto per lettura+monitoring file via ssh
* **visualizzare le eccezioni di procedure all'interno della sequence e non sotto
   la procedura (eccezione as block)**
* implementare salvataggio graph su file

**Annotations:**

* aggiungere annotations per metodo main

**GradlePlugin:**

* Gradle Plugin  
  plugin che crea runPerformanceViewer ed applica PerformanceAnnotationsTask a
  tutti i sourcesets del progetto

**Samples:**

* All: riunire il codice comune in unica cartella
* All: passare samples (quando possibile) a plugin gradle e anzichè via file .gradle esterni
* GradleIntegration: esempio in cui viene tracciato ogni task del progetto Gradle 
* Sample Scheduled Remote Plugin:    Scheduled Remote tramite il Gradle Plugin
* Errors: aggiungere gestione eccezioni senza annotations
* SoftwareModels: esempi di software models (condizioni, cicli, proxy, mngm...)
